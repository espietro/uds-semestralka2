#include "structures/heap_monitor.h"
#include "Kraj.h"


Kraj::Kraj(string name)
{	
	nazov_ = name;	
}


Kraj::~Kraj()
{
}

void Kraj::add_okres(Okres * novy)
{
	okresy_.add(novy);		
}

void Kraj::vypis_sum_stat(int rok_od, int rok_do)
{
	char *format = "%26s %5d\n";
	int sum_no_ztp = 0;
	int	sum_no_abs = 0;
	int sum_no_mlad = 0;
	int	sum_no_dlho = 0;
	for (rok_od; rok_od	<= rok_do; rok_od++)
	{
		int no_ztp = 0;
		int	no_abs = 0;
		int no_mlad = 0;
		int	no_dlho = 0;

		for each (Okres* item in okresy_)
		{
			no_ztp += item->get_no_ztp(rok_od);
			no_abs += item->get_no_abs(rok_od);
			no_mlad += item->get_no_mlad(rok_od);
			no_dlho += item->get_no_dlho(rok_od);
		}
		sum_no_ztp += no_ztp;
		sum_no_abs += no_abs;
		sum_no_mlad += no_mlad;
		sum_no_dlho += no_dlho;


		cout << "\n\t\tRok:" << rok_od;
		cout << "\nPocet uchadzacov o zamestnanie z radu:\n";

		printf(format, "zdravotne postihnutych:", no_ztp);
		printf(format, "absolventov:", no_abs);
		printf(format, "mladistvych:", no_mlad);
		printf(format, "dlhodobo nezamestnanych:", no_dlho);
	}
	cout << "\n\t\tSuma za cele obdobie";
	cout << "\nPocet uchadzacov o zamestnanie z radu:\n";

	printf(format, "zdravotne postihnutych:", sum_no_ztp);
	printf(format, "absolventov:", sum_no_abs);
	printf(format, "mladistvych:", sum_no_mlad);
	printf(format, "dlhodobo nezamestnanych:", sum_no_dlho);
}

/**
	Vrati disponibilny pocet uchadzacov o zamestnanie v tomto kraji.(sucet jednotlivych okresov)
*/
int Kraj::get_sum_disp_poc_uchad(int rok)
{
	int sum = 0;
	for each (Okres* item in okresy_)
	{
		sum += item->get_no_disp_uchad(rok);
	}
	return sum;
}

int Kraj::get_sum_ekonom_akt_ob(int rok)
{
	int sum = 0;
	for each (Okres* item in okresy_)
	{
		sum += item->get_no_ekon_aktiv_ob(rok);
	}
	return sum;
}

float Kraj::get_miera_evid_nezam(int rok)
{
	float miera = 0;
	miera = (float)get_sum_disp_poc_uchad(rok) / (float)get_sum_ekonom_akt_ob(rok);
	return miera;
}

float Kraj::get_avg_ekon_aktiv_ob(int rok_od, int rok_do)
{
	float sum = 0;
	float count = 0;
	for (rok_od; rok_od <= rok_do; rok_od++)
	{
		for each (Okres* item in okresy_)
		{
			float pom = item->get_no_ekon_aktiv_ob(rok_od);
			sum += pom;			
		}
		count++;
	}
	return sum/count;
}

float Kraj::get_max_pokles_zeny_kraj(int rok_od, int rok_do)
{	
	float zac = 0;
	float kon = 0;
	for each (Okres* item in okresy_)
	{
		zac += item->get_miera_nezam_zen(rok_od);
		kon += item->get_miera_nezam_zen(rok_do);
	}
	return zac - kon;
}

float Kraj::get_max_pokles_muzi_kraj(int rok_od, int rok_do)
{
	float zac = 0;
	float kon = 0;
	for each (Okres* item in okresy_)
	{
		zac += item->get_miera_nezam_muz(rok_od);
		kon += item->get_miera_nezam_muz(rok_do);
	}	
	return zac - kon;
}

int Kraj::sum_disp_uchadz(int rok)
{
	int sum = 0;
	for each (Okres* item in okresy_)
	{
		sum += item->get_no_disp_uchad(rok);
	}
	return sum;
}

int Kraj::sum_nezam_absol(int rok)
{
	int sum = 0;
	for each (Okres* item in okresy_)
	{
		sum += item->get_no_abs(rok);
	}
	return sum;
}

float Kraj::get_best_pomer_disp_abs(int rok_od, int rok_do)
{
	if (rok_do == rok_od)
	{
		return (float)sum_disp_uchadz(rok_do) / (float)sum_nezam_absol(rok_do);
	}
	else
	{
		float sum = 0;
		float count = 0;
		for (rok_od; rok_od <= rok_do; rok_od++)
		{
			sum += (float)sum_disp_uchadz(rok_od) / (float)sum_nezam_absol(rok_od);
			count++;
		}
		return sum / count;
	}	
}

	
