#include "Stat.h"
#include "structures/heap_monitor.h"

Stat::Stat()
{	
	kraje_.insert("Bratislavsky", new Kraj("Bratislavsky"));
	kraje_.insert("Trnavsky", new Kraj("Trnavsky"));
	kraje_.insert("Trenciansky", new Kraj("Trenciansky"));
	kraje_.insert("Nitriansky", new Kraj("Nitriansky"));
	kraje_.insert("Zilinsky", new Kraj("Zilinsky"));
	kraje_.insert("Banskobystricky", new Kraj("Banskobystricky"));
	kraje_.insert("Presovsky", new Kraj("Presovsky"));
	kraje_.insert("Kosicky", new Kraj("Kosicky"));
}


Stat::~Stat()
{
	for each (TableItem<string, Okres*>* okres in okresy_)
	{
		delete okres->accessData();
	}
	for each (TableItem<string, Kraj*>* kraj in kraje_)
	{
		delete kraj->accessData();
	}
}

bool Stat::loadNeazmest(ifstream & inFile)
{
	if (!inFile.is_open())
	{
		return false;
	}

	string line;
	getline(inFile, line);
	getline(inFile, line);

	for (int i = 0; i < NO_OKRES; i++)
	{
		string token;
		getline(inFile, token, ' ');

		if (token != "Okres")
		{
			return false;
		}
		getline(inFile, token, ';');
		//cout << token << endl;
		Okres* novy = new Okres(token);

		if (!novy->loadNezam(inFile)) {
			return false;
		}
		okresy_.insert(token, novy);
		add_to_kraj(novy, i);
	}
	return true;
}

bool Stat::loadMiera(ifstream & inFile)
{
	if (!inFile.is_open())
	{
		return false;
	}

	string line;

	getline(inFile, line);	//prve dva riadky nepotrebujeme 
	getline(inFile, line);

	for (int i = 0; i < NO_OKRES; i++)
	{
		string token;
		getline(inFile, token, ' ');	//malo by sa nacitat slovo okres

		if (token != "Okres")	//zistime ci sme na riadku so slovom okres
		{
			return false;
		}
		getline(inFile, token, ';');	//zistime nazov aktualneho okresu
		//cout << token << endl;	
		
		Okres* okres;
		if(okresy_.tryFind(token,okres))	//najdeme si okres v systeme
		{
			getline(inFile, token);	//preskocime riadok s percentami

			string line1;
			string line2;			

			getline(inFile, line1);	//nacitame riadok s cislami o ekonom. aktivnom obyvatelstve
			getline(inFile, line2);	//nacitame riadok s cislami o disponib. pocte uchadzacov o zamestnanie

			istringstream riadok1(line1);//ekonim. aktivne obyv
			istringstream riadok2(line2);//dispon. mnozstvo nezam. uchadz.

			if (!okres->loadMiera(riadok1, riadok2))
			{
				return false;
			}
		} 
		else
		{
			return false;
		}
	}
	return true;
}

void Stat::add_to_kraj(Okres* novy, int i)
{
	Kraj* pom;
	if (i < Bratislavsky)
	{
		if (kraje_.tryFind("Bratislavsky", pom)) {
			pom->add_okres(novy);
		}
	}
	else if (i < Trnavsky)
	{
		if (kraje_.tryFind("Trnavsky", pom)) {
			pom->add_okres(novy);
		}
	}
	else if (i < Trenciansky)
	{
		if (kraje_.tryFind("Trenciansky", pom)) {
			pom->add_okres(novy);
		}
	}
	else if (i < Nitriansky)
	{
		if (kraje_.tryFind("Nitriansky", pom)) {
			pom->add_okres(novy);
		}
	}
	else if (i < Zilinsky)
	{
		if (kraje_.tryFind("Zilinsky", pom)) {
			pom->add_okres(novy);
		}
	}
	else if (i < Banskobystricky)
	{
		if (kraje_.tryFind("Banskobystricky", pom)) {
			pom->add_okres(novy);
		}
	}
	else if (i < Presovsky)
	{
		if (kraje_.tryFind("Presovsky", pom)) {
			pom->add_okres(novy);
		}
	}
	else if (i < Kosicky)
	{
		if (kraje_.tryFind("Kosicky", pom)) {
			pom->add_okres(novy);
		}
	}
}

/**
	Vypise vsetky existujuce okresy, abecedne podla nazvu.
*/
void Stat::vypis_okresy()
{
	//int i = 0;
	for each (TableItem<string, Okres*>* okres in okresy_)
	{
		cout << /*i++ << */okres->getKey() << endl;
	}		
}

/**
	Vypise pocet nezamestnanych ludi v danom okrese, pre kazdy rok, v rozmedzi zadanych rokov.
*/
void Stat::vypis_pocty_nezam(string okres_nazov, int rok_od, int rok_do)
{
	Okres* okres;
	if (okresy_.tryFind(okres_nazov, okres)) {
		okres->get_pocty_nezam(rok_od, rok_do);
	}
	else
	{
		cout << "Okres so zadanym nazvom neexistuje.\n";
	}
}

//==============================miera nezamestnanosti

/**
	Usporiada danu strukturu podla kluca od najmensieho po najvacsi
*/
void Stat::usporiadaj_miera_nezam(int rok, UnsortedSequenceTable<float, Okres*>& pom_struct)
{	
	for each (TableItem<string, Okres*>* okres in okresy_)
	{
		pom_struct.insert(okres->accessData()->get_miera_nezam(rok), okres->accessData());
	}
	HeapSort<float, Okres*>* heapSort = new HeapSort<float, Okres*>();
	(*heapSort)(pom_struct);		
	delete heapSort;
}

/**
	Vypise mieru nezamenstnanosti pre kazdy okres, v danom roku, v klesajucej postupnosti.
*/
void Stat::vypis_miera_nezam_zost(int rok)
{
	UnsortedSequenceTable<float, Okres*>* pom_struct = 
		new UnsortedSequenceTable<float, Okres*>();
	
	usporiadaj_miera_nezam(rok, *pom_struct);
	int j = 0;
	for (size_t i = pom_struct->size(); i > 0; i--)
	{
		TableItem<float, Okres*> item = pom_struct->getItemAtIndex(i - 1);
		cout << j++ << ".\t" << item.accessData()->get_nazov();
		cout << ": " << item.getKey() << "%" << endl;		
	}
	delete pom_struct;
}

/**
	Vypise mieru nezamenstnanosti pre kazdy okres, v danom roku, v rastucej postupnosti.
*/
void Stat::vypis_miera_nezam_vzost(int rok)
{
	UnsortedSequenceTable<float, Okres*>* pom_struct =
		new UnsortedSequenceTable<float, Okres*>();

	usporiadaj_miera_nezam(rok, *pom_struct);
	int i = 0;
	for each (TableItem<float, Okres*>* item in *pom_struct) 
	{		
		cout << i++ << ".\t" << item->accessData()->get_nazov();
		cout << ": " << item->getKey() << "%" << endl;
	}
	delete pom_struct;
}

//=========================disponibilny uchadzaci

/**
	Usporiada danu strukturu podla kluca od najmensieho po najvacsi
*/
void Stat::usporiadaj_no_disp_uchadz(int rok, UnsortedSequenceTable<int, Okres*>& pom_struct)
{
	for each (TableItem<string, Okres*>* okres in okresy_)
	{
		pom_struct.insert(okres->accessData()->get_no_disp_uchad(rok), okres->accessData());
	}	
	HeapSort<int, Okres*>* heapSort = new HeapSort<int, Okres*>();
	(*heapSort)(pom_struct);
	delete heapSort;
}

/**
	Vypise pocet disponibilnych uchadzacov o zamesnanie v danom roku, pre kazdy okres, v klesajucej postupnosti.
*/
void Stat::vypis_no_disp_uchadz_zost(int rok)
{
	UnsortedSequenceTable<int, Okres*>* pom_struct =
		new UnsortedSequenceTable<int, Okres*>();

	usporiadaj_no_disp_uchadz(rok, *pom_struct);
	int j = 0;
	for (size_t i = pom_struct->size(); i > 0 ; i--)
	{
		auto item = pom_struct->getItemAtIndex(i - 1);
		cout << j++ << ".\t" << item.accessData()->get_nazov();
		cout << ": " << item.getKey() << endl;
	}
	delete pom_struct;	
}

/**
	Vypise pocet disponibilnych uchadzacov o zamesnanie v danom roku, pre kazdy okres, v rasucej postupnosti.
*/
void Stat::vypis_no_disp_uchadz_vzost(int rok)
{
	UnsortedSequenceTable<int, Okres*>* pom_struct =
		new UnsortedSequenceTable<int, Okres*>();

	usporiadaj_no_disp_uchadz(rok, *pom_struct);
	int i = 0;
	for each (TableItem<int, Okres*>* item in *pom_struct)
	{		
		cout << i++ << ".\t" << item->accessData()->get_nazov();
		cout << ": " << item->getKey() << endl;
	}
	delete pom_struct;
}

void Stat::get_min_max_avg_ekonom_aktiv_ob(int rok_od, int rok_do)
{
	UnsortedSequenceTable<float, Okres*>* avgs = new UnsortedSequenceTable<float, Okres*>();

	for each (TableItem<string,Okres*>* item in okresy_)
	{
		float avg = item->accessData()->get_avg_ekonom_aktiv_obyv(rok_od, rok_do);
		avgs->insert(avg, item->accessData());
	}
	
	HeapSort<float, Okres*>* heapSort = new HeapSort<float, Okres*>();
	(*heapSort)(*avgs);
	/*int k = 0;
	for each (TableItem<float, Okres*>* item in *avgs)
	{
		cout << k++<< ". " << item->accessData()->get_nazov() << ": " << item->getKey() << endl;
	}*/
		
	int i = 0;
	int j = avgs->size() - 1;
	float min = avgs->getItemAtIndex(i).getKey();
	float max = avgs->getItemAtIndex(j).getKey();
		
	cout << endl << "Najvacsi priemerny pocet ekonomicky akti. obyv. maju okres/y:";	
	while (avgs->getItemAtIndex(j).getKey() == max)
	{
		TableItem<float, Okres*>* pom = &avgs->getItemAtIndex(j);
		cout << endl << "\t" << pom->accessData()->get_nazov();
		cout << ": " << pom->getKey() << endl;
		j--;
	}
	cout << endl << "Najmensi priemerny pocet ekonomicky akti. obyv. maju okres/y:";
	while (avgs->getItemAtIndex(i).getKey() == min)
	{
		TableItem<float, Okres*>* pom = &avgs->getItemAtIndex(i);
		cout << endl << "\t" << pom->accessData()->get_nazov();
		cout << ": " << pom->getKey() << endl;
		i++;
	}
	delete heapSort;
	delete avgs;
}

void Stat::get_max_pokles_miery_muzi(int rok_od, int rok_do)
{
	UnsortedSequenceTable<float, Okres*>* difs = new UnsortedSequenceTable<float, Okres*>();
	
	for each (TableItem<string, Okres*>* item in okresy_)
	{
		float dif = item->accessData()->get_pokles_miera_muzi(rok_od, rok_do);		
		difs->insert(dif, item->accessData());
	}
	HeapSort<float, Okres*>* heapSort = new HeapSort<float, Okres*>();
	(*heapSort)(*difs);

	int j = difs->size() - 1;	
	float max = difs->getItemAtIndex(j).getKey();

	cout << endl << "Najvacsi pokles miery evid. nezamest. u muzov maju okres/y:";
	while (difs->getItemAtIndex(j).getKey() == max)
	{
		TableItem<float, Okres*>* pom = &difs->getItemAtIndex(j);
		cout << endl << "\t" << pom->accessData()->get_nazov();
		cout << ": " << pom->getKey() << endl;
		j--;
	}
	delete heapSort;
	delete difs;
}

void Stat::get_max_pokles_miery_zeny(int rok_od, int rok_do)
{
	UnsortedSequenceTable<float, Okres*>* difs = new UnsortedSequenceTable<float, Okres*>();

	for each (TableItem<string, Okres*>* item in okresy_)
	{
		float dif = item->accessData()->get_pokles_miera_zeny(rok_od, rok_do);
		difs->insert(dif, item->accessData());
	}
	HeapSort<float, Okres*>* heapSort = new HeapSort<float, Okres*>();
	(*heapSort)(*difs);

	int j = difs->size() - 1;
	float max = difs->getItemAtIndex(j).getKey();

	cout << endl << "Najvacsi pokles miery evid. nezamest. u zien maju okres/y:";
	while (difs->getItemAtIndex(j).getKey() == max)
	{
		TableItem<float, Okres*>* pom = &difs->getItemAtIndex(j);
		cout << endl << "\t" << pom->accessData()->get_nazov();
		cout << ": " << pom->getKey() << endl;
		j--;
	}
	delete heapSort;
	delete difs;
}

void Stat::get_best_pomer_disp_and_abs(int rok_od, int rok_do)
{
	UnsortedSequenceTable<float, Okres*>* pomery = new UnsortedSequenceTable<float, Okres*>();

	for each (TableItem<string, Okres*>* item in okresy_)
	{
		float pomer = item->accessData()->get_pomer_disp_and_abs(rok_od, rok_do);
		pomery->insert(pomer, item->accessData());
	}
	HeapSort<float, Okres*>* heapSort = new HeapSort<float, Okres*>();
	(*heapSort)(*pomery);

	int j = 0;
	float min = pomery->getItemAtIndex(j).getKey();

	cout << endl << "Najlepsi pomer medzi disp. poc. uchad. o zamest. a poctom nezam. absol. maju okres/y:";
	while (pomery->getItemAtIndex(j).getKey() == min)
	{
		TableItem<float, Okres*>* pom = &pomery->getItemAtIndex(j);
		cout << endl << "\t" << pom->accessData()->get_nazov();
		cout << ": " << pom->getKey() << endl;
		j++;
	}
	delete heapSort;
	delete pomery;
}

void Stat::get_sum_stat_za_kraj(string nazov, int rok_od, int rok_do)
{
	Kraj* pom;
	if (kraje_.tryFind(nazov, pom))
	{
		pom->vypis_sum_stat(rok_od, rok_do);
	}
	else
	{
		cout << "Zadany kraj sa nenasiel." << endl;
	}
}

void Stat::zorad_miera_nezam_kraje(int rok, UnsortedSequenceTable<float, Kraj*>* miery)
{
	for each (TableItem<string, Kraj*>* item in kraje_)
	{
		float miera = item->accessData()->get_miera_evid_nezam(rok) * 100;
		miery->insert(miera, item->accessData());
	}
	HeapSort<float, Kraj*>* heapSort = new HeapSort<float, Kraj*>();
	(*heapSort)(*miery);
	delete heapSort;
}

void Stat::vypis_miera_nezam_kraje_vzost(int rok)
{
	UnsortedSequenceTable<float, Kraj*>* miery = new UnsortedSequenceTable<float, Kraj*>();
	zorad_miera_nezam_kraje(rok, miery);
	int i = 0;
	for each (TableItem<float, Kraj*>* item in *miery)
	{
		cout << endl << i++ << ".\t" << item->accessData()->get_nazov();
		cout << ": " << item->getKey() << "%";
	}
	cout << endl;

	delete miery;
}

void Stat::vypis_miera_nezam_kraje_zost(int rok)
{
	UnsortedSequenceTable<float, Kraj*>* miery = new UnsortedSequenceTable<float, Kraj*>();
	zorad_miera_nezam_kraje(rok, miery);
	int i = 0;

	for (int j = miery->size(); j > 0; j--)
	{
		TableItem<float, Kraj*>* item = &miery->getItemAtIndex(j - 1);
		cout << endl << i++ << ".\t" << item->accessData()->get_nazov();
		cout << ": " << item->getKey() << "%";
	}
	cout << endl;

	delete miery;
}

void Stat::zorad_no_disp_zam_kraje(int rok, UnsortedSequenceTable<int, Kraj*>* disp)
{
	for each (TableItem<string, Kraj*>* item in kraje_)
	{
		int pocet = item->accessData()->get_sum_disp_poc_uchad(rok);
		disp->insert(pocet, item->accessData());
	}
	HeapSort<int, Kraj*>* heapSort = new HeapSort<int, Kraj*>();
	(*heapSort)(*disp);
	delete heapSort;
}

void Stat::vypis_no_disp_zam_kraje_vzost(int rok)
{
	UnsortedSequenceTable<int, Kraj*>* disp = new UnsortedSequenceTable<int, Kraj*>();
	zorad_no_disp_zam_kraje(rok, disp);
	int i = 0;
	for each (TableItem<int, Kraj*>* item in *disp)
	{
		cout << endl << i++ << ".\t" << item->accessData()->get_nazov();
		cout << ": " << item->getKey();
	}
	cout << endl;

	delete disp;
}

void Stat::vypis_no_disp_zam_kraje_zost(int rok)
{
	UnsortedSequenceTable<int, Kraj*>* disp = new UnsortedSequenceTable<int, Kraj*>();
	zorad_no_disp_zam_kraje(rok, disp);
	int i = 0;

	for (int j = disp->size(); j > 0; j--)
	{
		TableItem<int, Kraj*>* item = &disp->getItemAtIndex(j - 1);
		cout << endl << i++ << ".\t" << item->accessData()->get_nazov();
		cout << ": " << item->getKey();
	}
	cout << endl;

	delete disp;
}

void Stat::get_max_min_avg_ekonom_aktiv_ob_kraj(int rok_od, int rok_do)
{
	UnsortedSequenceTable<float, Kraj*>* avgs = new UnsortedSequenceTable<float, Kraj*>();

	for each (TableItem<string,Kraj*>* item in kraje_)
	{
		float pom = item->accessData()->get_avg_ekon_aktiv_ob(rok_od, rok_do);		
		avgs->insert(pom, item->accessData());
	}

	HeapSort<float, Kraj*>* heapSort = new HeapSort<float, Kraj*>();
	(*heapSort)(*avgs);

	int i = 0;
	int j = avgs->size() - 1;
	float min = avgs->getItemAtIndex(i).getKey();
	float max = avgs->getItemAtIndex(j).getKey();

	cout << endl << "Najvacsi priemerny pocet ekonomicky akti. obyv. maju kraj/e:";
	while (avgs->getItemAtIndex(j).getKey() == max)
	{
		TableItem<float, Kraj*>* pom = &avgs->getItemAtIndex(j);
		cout << endl << "\t" << pom->accessData()->get_nazov();
		cout << ": " << pom->getKey() << endl;
		j--;
	}
	cout << endl << "Najmensi priemerny pocet ekonomicky akti. obyv. maju kraj/e:";
	while (avgs->getItemAtIndex(i).getKey() == min)
	{
		TableItem<float, Kraj*>* pom = &avgs->getItemAtIndex(i);
		cout << endl << "\t" << pom->accessData()->get_nazov();
		cout << ": " << pom->getKey() << endl;
		i++;
	}
	delete avgs;
	delete heapSort;
}

void Stat::get_max_pokles_muzi_kraj(int rok_od, int rok_do)
{
	UnsortedSequenceTable<float, Kraj*>* poklesy = new UnsortedSequenceTable<float, Kraj*>();

	for each (TableItem<string, Kraj*>* item in kraje_)
	{
		float pokles = item->accessData()->get_max_pokles_muzi_kraj(rok_od, rok_do);
		poklesy->insert(pokles, item->accessData());		
	}

	HeapSort<float, Kraj*>* heapSort = new HeapSort<float, Kraj*>();
	(*heapSort)(*poklesy);

	int j = poklesy->size() - 1;
	float max = poklesy->getItemAtIndex(j).getKey();

	cout << endl << "Najvacsi pokles miery evid. nezam. pre muzov maju kraj/e:";
	while (poklesy->getItemAtIndex(j).getKey() == max)
	{
		TableItem<float, Kraj*>* pom = &poklesy->getItemAtIndex(j);
		cout << endl << "\t" << pom->accessData()->get_nazov();
		cout << ": " << pom->getKey() << endl;
		j--;
	}

	delete heapSort;
	delete poklesy;
}

void Stat::get_max_pokles_zeny_kraj(int rok_od, int rok_do)
{
	UnsortedSequenceTable<float, Kraj*>* poklesy = new UnsortedSequenceTable<float, Kraj*>();

	for each (TableItem<string, Kraj*>* item in kraje_)
	{
		float pokles = item->accessData()->get_max_pokles_zeny_kraj(rok_od, rok_do);
		poklesy->insert(pokles, item->accessData());		
	}
		
	HeapSort<float, Kraj*>* heapSort = new HeapSort<float, Kraj*>();
	(*heapSort)(*poklesy);
	
	int j = poklesy->size() - 1;
	float max = poklesy->getItemAtIndex(j).getKey();

	cout << endl << "Najvacsi pokles miery evid. nezam. pre zeny maju kraj/e:";
	while (poklesy->getItemAtIndex(j).getKey() == max)
	{
		TableItem<float, Kraj*>* pom = &poklesy->getItemAtIndex(j);
		cout << endl << "\t" << pom->accessData()->get_nazov();
		cout << ": " << pom->getKey() << endl;
		j--;
	}

	delete heapSort;
	delete poklesy;
}

void Stat::get_best_pomer_disp_abs_kraj(int rok_od, int rok_do)
{
	UnsortedSequenceTable<float, Kraj*>* pomery = new UnsortedSequenceTable<float, Kraj*>();

	for each (TableItem<string,Kraj*>* item in kraje_)
	{
		float pomer = item->accessData()->get_best_pomer_disp_abs(rok_od, rok_do);
		pomery->insert(pomer, item->accessData());
		//cout << item->getKey() << "\t" << pomer << endl;
	}

	HeapSort<float, Kraj*>* heapSort = new HeapSort<float, Kraj*>();
	(*heapSort)(*pomery);

	int j = 0;
	float min = pomery->getItemAtIndex(j).getKey();

	cout << endl << "Najlepsi pomer medzi disp. poc. uchad. o zamest. a poctom nezam. absol. maju kraj/e:";
	while (pomery->getItemAtIndex(j).getKey() == min)
	{
		TableItem<float, Kraj*>* pom = &pomery->getItemAtIndex(j);
		cout << endl << "\t" << pom->accessData()->get_nazov();
		cout << ": " << pom->getKey() << endl;
		j++;
	}
}







