#pragma once
#include <fstream>
#include <iostream>
#include <string>
#include "structures\heap_monitor.h"

using namespace std;
class RocnikNezam
{
public:

	RocnikNezam(/*int rok, */int no_ztp, int no_abs, int no_mlad, int no_dlho)
	{
		//rok_ = rok;
		ztp_uchadz_ = no_ztp;
		abs_uchadz_ = no_abs;
		mlad_uchadz_ = no_mlad;
		dlho_uchadz_ = no_dlho;
	}

	~RocnikNezam()
	{
	}	

	int get_ztp_no() { return ztp_uchadz_; }
	int get_abs_no() { return abs_uchadz_; }
	int get_mlad_no() { return mlad_uchadz_; }
	int get_dlho_no() { return dlho_uchadz_; }
	

private://Applic = Applicants
	//int rok_;
	int ztp_uchadz_; //Po�et uch�dza�ov o zamestnanie so ZP
	int abs_uchadz_;//Po�et uch�dza�ov o zamestnanie - absolventi
	int mlad_uchadz_;//Po�et uch�dza�ov o zamestnanie - mladistv�
	int dlho_uchadz_;//Po�et dlhodobo evidovan�ch uch�dza�ov o zamestnanie
};

class RocnikMiera
{
public:
	RocnikMiera(int aktiv_muz, int aktiv_zen, int disp_muz, int disp_zen)
	{		
		aktiv_muz_ = aktiv_muz;
		aktiv_zen_ = aktiv_zen;
		aktiv_spolu_ = aktiv_muz + aktiv_zen;

		disp_muz_ = disp_muz;
		disp_zen_ = disp_zen;
		disp_spolu_ = disp_muz + disp_zen;

		miera_muz_ = (float)disp_muz * 100 / (float)aktiv_muz;
		miera_zen_ = (float)disp_zen * 100 / (float)aktiv_zen;;
		miera_spolu_ = (float)disp_spolu_ * 100 / (float)aktiv_spolu_;;
	}

	~RocnikMiera() 
	{
	}	

	/**miera evidovanej nezam. muzi [%]*/
	float get_miera_muz() { return miera_muz_; }		
	/**miera evidovanej nezam.zeny[%]*/
	float get_miera_zen() { return miera_zen_; }        
	/**miera evidovanej nezam. spolu [%]*/
	float get_miera_spolu() { return miera_spolu_; }    
	/**ekonimicky aktivne obyvatelstvo muzi*/
	int get_aktiv_muz() { return aktiv_muz_; }          
	/**ekonimicky aktivne obyvatelstvo zeny*/
	int get_aktiv_zen() { return aktiv_zen_; }          
	/**ekonimicky aktivne obyvatelstvo spolu*/
	int get_aktiv_spolu() { return aktiv_spolu_; }      
	/**disponibilny pocet uchadzacov o zamestnanie muzi*/
	int get_disp_muz() { return disp_muz_; }            
	/**disponibilny pocet uchadzacov o zamestnanie zeny*/
	int get_disp_zen() { return disp_zen_; }            
	/**disponibilny pocet uchadzacov o zamestnanie spolu*/
	int get_disp_spolu() { return disp_spolu_; }        

private:
	float miera_muz_; //miera evidovanej nezam. muzi [%]
	float miera_zen_; //miera evidovanej nezam. zeny [%]
	float miera_spolu_;	//miera evidovanej nezam. spolu [%]

	int aktiv_muz_;	//ekonimicky aktivne obyvatelstvo muzi
	int aktiv_zen_;	//ekonimicky aktivne obyvatelstvo zeny
	int aktiv_spolu_;	//ekonimicky aktivne obyvatelstvo spolu

	int disp_muz_;	//disponibilny pocet uchadzacov o zamestnanie muzi
	int disp_zen_;	//disponibilny pocet uchadzacov o zamestnanie zeny
	int disp_spolu_;	//disponibilny pocet uchadzacov o zamestnanie spolu


};