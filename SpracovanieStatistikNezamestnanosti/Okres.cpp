#include "structures/heap_monitor.h"
#include "Okres.h"


Okres::Okres(string name)
{
	nazov_ = name;
}


Okres::~Okres()
{
	for (size_t i = 2001; i <= 2016; i++)
	{
		delete nezamestnani_[i];
		delete miera_nezamest_[i];
	}	

}

bool Okres::loadNezam(ifstream& inFile)
{	
	string token;
	for (int i = 0; i < 16; i++)
	{
		getline(inFile, token, ';');	//pocet ZTP
		int ztp = stoi(token);
		getline(inFile, token, ';');	//pocet absolventov
		int absol = stoi(token);
		getline(inFile, token, ';');	//pocet mladistvych
		int mlad = stoi(token);


		int dlho;					//pocet dlhodobo nezamestnanych
		if (i == 15) {
			getline(inFile, token, '\n');
		}
		else
		{
			getline(inFile, token, ';');			
		}
		dlho = stoi(token);


		int rok = 2001 + i;
		RocnikNezam* rocnik = new RocnikNezam(ztp, absol, mlad, dlho);
		nezamestnani_.insert(rok, rocnik);
	}
	return true;
}

bool Okres::loadMiera(istringstream& aktiv_obyv, istringstream& disp_mnoz)
{
	string token1;
	string token2;
	for (int i = 0; i < 2; i++)
	{	//prvy krat zobrie len ';' druhy krat zoberie popis riadku
		getline(aktiv_obyv, token1, ';');
		getline(disp_mnoz, token2, ';');
	}

	for (int i = 0; i < 16; i++)
	{
		for (int j = 0; j < 2; j++)
		{	//prvy krat zobrie stlpec 'spolu' druhy krat zoberie stlpec 'muzi'
			getline(aktiv_obyv, token1, ';');
			getline(disp_mnoz, token2, ';');
		}
		
		int aktiv_muz = stoi(token1);	//ulozim si pocet ekonomicko aktivnych muzov
		int disp_muz = stoi(token2);	//a aj disp. pocet disp. nezam. muzov
			

		int aktiv_zen;	//kvoli koncu riadku musim davat pozor ci uz nie som na konci
		int disp_zen;	//a podla toho nacitat dalsie data
		if (i == 15) {
			getline(aktiv_obyv, token1, '\n');
			getline(disp_mnoz, token2, '\n');
		}
		else
		{
			getline(aktiv_obyv, token1, ';');
			getline(disp_mnoz, token2, ';');
		}
		aktiv_zen = stoi(token1);
		disp_zen = stoi(token2);


		int rok = 2001 + i;
		RocnikMiera* rocnik = new RocnikMiera(aktiv_muz, aktiv_zen, disp_muz, disp_zen);
		miera_nezamest_.insert(rok, rocnik);
	}	
	return true;
}

void Okres::get_pocty_nezam(int rok_od, int rok_do)
{
	char *format = "%26s %5d\n";
	int sum_ztp = 0;
	int sum_abs = 0;
	int sum_mlad = 0;
	int sum_dlho = 0;


	for (rok_od; rok_od <= rok_do; rok_od++)
	{
		RocnikNezam* roc;

		if (nezamestnani_.tryFind(rok_od, roc)) {
			cout << "\n\t\tRok:" << rok_od;
			cout << "\nPocet uchadzacov o zamestnanie z radu:\n";
			
			printf(format, "zdravotne postihnutych:", roc->get_ztp_no());
			printf(format, "absolventov:", roc->get_abs_no());
			printf(format, "mladistvych:", roc->get_mlad_no());
			printf(format, "dlhodobo nezamestnanych:", roc->get_dlho_no());

			sum_ztp += roc->get_ztp_no();
			sum_abs += roc->get_abs_no();
			sum_mlad += roc->get_mlad_no();
			sum_dlho += roc->get_dlho_no();
		}
		else
		{
			cout << "V tychto rokoch neboli zadane ziadne zaznamy.\n";
		}		
	}
	cout << "\n\t\tSuma za cele obdobie";
	cout << "\nPocet uchadzacov o zamestnanie z radu:\n";

	printf(format, "zdravotne postihnutych:", sum_ztp);
	printf(format, "absolventov:", sum_abs);
	printf(format, "mladistvych:", sum_mlad);
	printf(format, "dlhodobo nezamestnanych:", sum_dlho);
}

float Okres::get_miera_nezam(int rok)
{
	RocnikMiera* pom;
	if (miera_nezamest_.tryFind(rok, pom)) {
		return pom->get_miera_spolu();
	}
	else
	{
		throw std::exception();
	}
}

int Okres::get_no_disp_uchad(int rok)
{
	RocnikMiera* pom;
	if (miera_nezamest_.tryFind(rok, pom)) {
		return pom->get_disp_spolu();
	}
	else
	{
		throw std::exception();
	}
}

/**
	Vypocita a vrati priemerny pocet ekonomicky aktivneho obyvatelstva v danom intervale rokov.
*/
float Okres::get_avg_ekonom_aktiv_obyv(int rok_od, int rok_do)
{
	RocnikMiera* pom;
	float sum = 0;
	int count = 0;
	float avg;
	
	for (rok_od; rok_od <= rok_do; rok_od++)
	{
		if (miera_nezamest_.tryFind(rok_od, pom))
		{
			sum += (float)pom->get_aktiv_spolu();
			count++;
		}
	}
	avg = sum / (float)count;

	return avg;
}

float Okres::get_pokles_miera_zeny(int rok_od, int rok_do)
{
	RocnikMiera* zaciatok;
	RocnikMiera* koniec;
	float pokles;

	if (miera_nezamest_.tryFind(rok_od, zaciatok) && miera_nezamest_.tryFind(rok_do,koniec))
	{
		pokles = zaciatok->get_miera_zen() - koniec->get_miera_zen();
	}

	return pokles;
}

float Okres::get_pokles_miera_muzi(int rok_od, int rok_do)
{
	RocnikMiera* zaciatok;
	RocnikMiera* koniec;
	float pokles;

	if (miera_nezamest_.tryFind(rok_od, zaciatok) && miera_nezamest_.tryFind(rok_do, koniec))
	{
		pokles = zaciatok->get_miera_muz() - koniec->get_miera_muz();
	}

	return pokles;
}

float Okres::get_pomer_disp_and_abs(int rok_od, int rok_do)
{
	float pomer = 0;
	RocnikMiera* disp;//disponibilny pocet uchadzacov o zamestnanie;
	RocnikNezam* abs;//pocet uchadzacov z radu absolventov;

	if (rok_do == rok_od && miera_nezamest_.tryFind(rok_do, disp) && nezamestnani_.tryFind(rok_do,abs))
	{
		pomer = (float)disp->get_disp_spolu() / (float)abs->get_abs_no();
	} 
	else
	{
		float count = 0;
		for (rok_od; rok_od <= rok_do; rok_od++) 
		{
			if (miera_nezamest_.tryFind(rok_od, disp) && nezamestnani_.tryFind(rok_od, abs))
			{
				pomer += (float)disp->get_disp_spolu() / (float)abs->get_abs_no();
				count++;
			}
		}
		pomer = pomer / count;
	}


	return pomer;
}

int Okres::get_no_ztp(int rok)
{
	RocnikNezam* pom;
	if (nezamestnani_.tryFind(rok, pom))
	{
		return pom->get_ztp_no();
	}
	throw std::exception();
}

int Okres::get_no_abs(int rok)
{
	RocnikNezam* pom;
	if (nezamestnani_.tryFind(rok, pom))
	{
		return pom->get_abs_no();
	}
	throw std::exception();
}

int Okres::get_no_mlad(int rok)
{
	RocnikNezam* pom;
	if (nezamestnani_.tryFind(rok, pom))
	{
		return pom->get_mlad_no();
	}
	throw std::exception();
}

int Okres::get_no_dlho(int rok)
{
	RocnikNezam* pom;
	if (nezamestnani_.tryFind(rok, pom))
	{
		return pom->get_dlho_no();
	}
	throw std::exception();
}

/**
	Vrati disponibilny pocet uchadzacov o zamestnanie v tomto kraji pre dany rok.
*/
//int Okres::get_no_disp_uchadz(int rok)
//{
//	RocnikMiera* pom;
//	if (miera_nezamest_.tryFind(rok, pom))
//	{
//		return pom->get_disp_spolu();
//	}
//	throw std::exception();
//}

int Okres::get_no_ekon_aktiv_ob(int rok)
{
	RocnikMiera* pom;
	if (miera_nezamest_.tryFind(rok, pom))
	{
		return pom->get_aktiv_spolu();
	}
	throw std::exception();
}

float Okres::get_miera_nezam_muz(int rok)
{
	RocnikMiera* pom;
	if (miera_nezamest_.tryFind(rok, pom))
	{
		return pom->get_miera_muz();
	}
	throw std::exception();
}

float Okres::get_miera_nezam_zen(int rok)
{
	RocnikMiera* pom;
	if (miera_nezamest_.tryFind(rok, pom))
	{
		return pom->get_miera_zen();
	}
	throw std::exception();
}




		


