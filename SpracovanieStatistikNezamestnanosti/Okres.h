#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "structures/heap_monitor.h"
#include "structures\table\sorted_sequence_table.h"
#include "structures\table\sorting\heap_sort.h"
#include "Rocniky.h"	
#include "structures\array\array.h"


using namespace std;
using namespace structures;
/**
	Trieda reprezentuje okres
*/
class Okres
{
public:
	Okres(string);
	~Okres();
	bool loadNezam(ifstream&);
	bool loadMiera(istringstream&, istringstream&);
	string get_nazov() { return nazov_; }
	void get_pocty_nezam(int rok_od, int rok_do);
	float get_miera_nezam(int rok);
	int get_no_disp_uchad(int rok);
	//3a
	float get_avg_ekonom_aktiv_obyv(int rok_od, int rok_do);
	//3b
	float get_pokles_miera_zeny(int rok_od, int rok_do);
	float get_pokles_miera_muzi(int rok_od, int rok_do);
	//3c
	float get_pomer_disp_and_abs(int rok_od, int rok_do);
	//4
	int get_no_ztp(int rok);
	int get_no_abs(int rok);
	int get_no_mlad(int rok);
	int get_no_dlho(int rok);
	//5
	//int get_no_disp_uchadz(int rok);
	//5,6
	int get_no_ekon_aktiv_ob(int rok);
	
	float get_miera_nezam_muz(int rok);
	float get_miera_nezam_zen(int rok);
private:
	string nazov_;	//nazov okresu	
	SortedSequenceTable <int, RocnikNezam*> nezamestnani_; //klucom je rok
	SortedSequenceTable <int, RocnikMiera*> miera_nezamest_;

};

