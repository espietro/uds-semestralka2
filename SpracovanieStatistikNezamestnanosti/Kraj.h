#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "structures/table/sorted_sequence_table.h"
#include "structures/heap_monitor.h"
#include "Okres.h"

using namespace structures;
/**
	Trieda reprezentuje Kraj
*/
class Kraj
{
public:
	Kraj(string);
	~Kraj();
	string get_nazov() { return nazov_; }
	void add_okres(Okres*);
	//4
	void vypis_sum_stat(int rok_od, int rok_do);
	//5a, 5b
	int get_sum_disp_poc_uchad(int rok);
	//5a
	int get_sum_ekonom_akt_ob(int rok);
	float get_miera_evid_nezam(int rok);
	//6a
	float get_avg_ekon_aktiv_ob(int,int);
	//6b
	float get_max_pokles_zeny_kraj(int, int);
	float get_max_pokles_muzi_kraj(int, int);

	int sum_disp_uchadz(int rok);
	int sum_nezam_absol(int rok);
	float get_best_pomer_disp_abs(int rok_od, int rok_do);
private:	
	string nazov_;
	ArrayList<Okres*> okresy_;	//zoznam smernikov na okresy
};


