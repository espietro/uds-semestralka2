#pragma once

#include "list.h"
#include "../structure_iterator.h"
#include "../array/array.h"

namespace structures
{

	/// <summary> Zoznam implementovany polom. </summary>
	/// <typeparam name = "T"> Typ dat ukladanych v zozname. </typeparam>
	template<typename T>
	class ArrayList : public List<T>
	{
	public:
		/// <summary> Konstruktor. </summary>
		ArrayList();

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> ArrayList, z ktoreho sa prevezmu vlastnosti. </param>
		ArrayList(const ArrayList<T>& other);

		/// <summary> Destruktor. </summary>
		~ArrayList();
		
		/// <summary> Operacia klonovania. Vytvori a vrati duplikat zoznamu. </summary>
		/// <returns> Ukazovatel na klon struktury. </returns>
		Structure* clone() const override;

		/// <summary> Vrati pocet prvkov v zozname. </summary>
		/// <returns> Pocet prvkov v zozname. </returns>
		size_t size() const override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Zoznam, z ktoreho ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tento zoznam nachadza po priradeni. </returns>
		List<T>& operator=(const List<T>& other) override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Zoznam, z ktoreho ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tento zoznam nachadza po priradeni. </returns>
		ArrayList<T>& operator=(const ArrayList<T>& other);

		/// <summary> Vrati adresou prvok na indexe. </summary>
		/// <param name = "index"> Index prvku. </param>
		/// <returns> Adresa prvku na danom indexe. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do zoznamu. </exception>  
		T& operator[](const int index) override;

		/// <summary> Vrati hodnotou prvok na indexe. </summary>
		/// <param name = "index"> Index prvku. </param>
		/// <returns> Hodnota prvku na danom indexe. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do zoznamu. </exception>  
		const T operator[](const int index) const override;

		/// <summary> Prida prvok do zoznamu. </summary>
		/// <param name = "data"> Pridavany prvok. </param>
		void add(const T& data) override;

		/// <summary> Vlozi prvok do zoznamu na dany index. </summary>
		/// <param name = "data"> Pridavany prvok. </param>
		/// <param name = "index"> Index prvku. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do zoznamu. </exception>  
		/// <remarks> Ak je ako index zadana hodnota poctu prvkov (teda prvy neplatny index), metoda insert sa sprava ako metoda add. </remarks>
		void insert(const T& data, const int index) override;

		/// <summary> Odstrani prvy vyskyt prvku zo zoznamu. </summary>
		/// <param name = "data"> Odstranovany prvok. </param>
		/// <returns> true, ak sa podarilo prvok zo zoznamu odobrat, false inak. </returns>
		bool tryRemove(const T& data) override;

		/// <summary> Odstrani zo zoznamu prvok na danom indexe. </summary>
		/// <param name = "index"> Index prvku. </param>
		/// <returns> Odstraneny prvok. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do zoznamu. </exception>  
		T removeAt(const int index) override;

		/// <summary> Ak je v poli najviac 1 / 4 aktivnych prvkov tak zmensime jeho velkost na 1 / 2. </summary>
		void shrinkToFit();

		/// <summary> Vrati index prveho vyskytu prvku v zozname. </summary>
		/// <param name = "data"> Prvok, ktoreho index sa hlada. </param>
		/// <returns> Index prveho vyskytu prvku v zozname, ak sa prvok v zozname nenachadza, vrati -1. </returns>
		int getIndexOf(const T& data) override;

		/// <summary> Vymaze zoznam. </summary>
		void clear() override;
	
		/// <summary> Vrati skutocny iterator na zaciatok struktury </summary>
		/// <returns> Iterator na zaciatok struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<T>* getBeginIterator() const override;

		/// <summary> Vrati skutocny iterator na koniec struktury </summary>
		/// <returns> Iterator na koniec struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<T>* getEndIterator() const override;
	private:
		/// <summary> Pole s datami. </summary>
		Array<T>* array_; //typ je smernik na pole
		/// <summary> Pocet prvkov v zozname. </summary>
		size_t size_;
	private:
		/// <summary> Rozsiri kapacitu zoznamu. </summary>
		void enlarge();

		/// <summary> Iterator pre ArrayList. </summary>
		class ArrayListIterator : public Iterator<T>	//privatna vnorena trieda
		{
		public:
			/// <summary> Konstruktor. </summary>
			/// <param name = "arrayList"> Zoznam, cez ktory iteruje. </param>
			/// <param name = "position"> Pozicia, na ktorej zacina. </param>
			ArrayListIterator(const ArrayList<T>* arrayList, int position);

			/// <summary> Destruktor. </summary>
			~ArrayListIterator();

			/// <summary> Operator priradenia. Priradi do seba hodnotu druheho iteratora. </summary>
			/// <param name = "other"> Druhy iterator. </param>
			/// <returns> Vrati seba po priradeni. </returns>
			Iterator<T>& operator= (const Iterator<T>& other) override;

			/// <summary> Porovna sa s druhym iteratorom na nerovnost. </summary>
			/// <param name = "other"> Druhy iterator. </param>
			/// <returns> True, ak sa iteratory nerovnaju, false inak. </returns>
			bool operator!=(const Iterator<T>& other) override;

			/// <summary> Vrati data, na ktore aktualne ukazuje iterator. </summary>
			/// <returns> Data, na ktore aktualne ukazuje iterator. </returns>
			const T operator*() override;

			/// <summary> Posunie iterator na dalsi prvok v strukture. </summary>
			/// <returns> Iterator na dalsi prvok v strukture. </returns>
			/// <remarks> Zvycajne vrati seba. Ak vrati iny iterator, povodny bude automaticky zruseny. </remarks>
			Iterator<T>& operator++() override;
		private:
			/// <summary> Zoznam, cez ktory iteruje. </summary>
			const ArrayList<T>* arrayList_; //iterator ma len prechadzat nema ho modifikovat preto const
			/// <summary> Aktualna pozicia v zozname. </summary>
			int position_;
		};
	};

	/**
		Konstruktor.
	*/
	template<typename T>
	inline ArrayList<T>::ArrayList():
		List<T>(),		//vyvolaj tento konstruktor listu-> ak ho nenapiseme vyvola sa implicitne(bezparametricky), cize rovnaky
		array_(new Array<T>(4)),
		size_(0)	//prazdny arrayList- size je pocet aktivnych prvkov
	{
	}

	/**
		Kopirovaci konstruktor.
		other - ArrayList, z ktoreho sa prevezmu vlastnosti.
	*/
	template<typename T>
	inline ArrayList<T>::ArrayList(const ArrayList<T>& other):
		List<T>(other),
		array_(new Array<T>(*other.array_)), //volame kopirovaci konstruktor pola
		size_(other.size_)
	{
	}

	/**
		 Destruktor.
	*/
	template<typename T>
	inline ArrayList<T>::~ArrayList()//TODO 03: ArrayList
	{
		delete array_;
		array_ = nullptr;	//davame to sem pretoze napr.:ak by sme mali 2 smerniky na nas ArrayList a na jednom z nich by som zavolal destruktor
							//a nevykonal by som tento riadok, zabudol by som ze ten druhy smernik ukazuje na neplatne data ale ak tento riadok vykonam array bude ukazovat nikam
		size_ = 0;
	}

	/**
		Operacia klonovania. Vytvori a vrati duplikat zoznamu.
		return - Ukazovatel na klon struktury.
	*/
	template<typename T>
	inline Structure * ArrayList<T>::clone() const
	{
		return new ArrayList<T>(*this);
	}

	/**
		Vrati pocet prvkov v zozname. 
		return - Pocet prvkov v zozname.
	*/
	template<typename T>
	inline size_t ArrayList<T>::size() const
	{
		return size_;
	}

	/**
		Operator priradenia.
		other - Zoznam, z ktoreho ma prebrat vlastnosti.
		return - Adresa, na ktorej sa tento zoznam nachadza po priradeni.
	*/
	template<typename T>
	inline List<T>& ArrayList<T>::operator=(const List<T>& other)	//tento operator je operator z listu je virtualny a abstraktny
	{
		if (this != &other)
		{
			*this = dynamic_cast<const ArrayList<T>&>(other);
		}
		return *this;
	}
	/*
	priklad:
	List<int> *list;
	ArrayList<int> *arrayList = new ArrayList<int>();
	list = new ArrayList<int>();

	*list = *arrayList; - vola sa operator priradenia z arraylistu
	*arrayList = *list; - vola sa operator priradenia z listu, kt je tu hore, ktory sa naprv snazi pretypovat a potom vola operator= v arrayListe takze ak
							to nie je mozne,napr chceme priradit do arraylistu linkedlist vyvola vynimku
	*/

	/**
		 Operator priradenia. 
		other - Zoznam, z ktoreho ma prebrat vlastnosti.
		return - Adresa, na ktorej sa tento zoznam nachadza po priradeni.
	*/
	template<typename T>
	inline ArrayList<T>& ArrayList<T>::operator=(const ArrayList<T>& other)	//TODO 03: ArrayList
	{
		//nemozem zavolat operator priradenia pola, namiesto neho musim zavolat copy konstruktor pola, je to kvoli operatoru priradenia v Poli kt sa spolieha na operator priradenia Vectora
		if (this != &other) //ak by som priradzoval sam do seba tak by som najprv vymazal svoje data apotom ich priradil to je zle
		{
			//potrebujeme mat rovnako velke polia
			delete array_; //inak by vznikol memory leak
			array_ = new Array<T>(*other.array_);	//lebo konstruktor vyzaduje objekt nie smernik
			size_ = other.size_;
		}
		return *this;
	}

	/**
		Vrati adresou prvok na indexe. </summary>
		index - Index prvku. </param>
		return - Adresa prvku na danom indexe. </returns>
		exception cref="std::out_of_range"> Vyhodena, ak index nepatri do zoznamu.
	*/
	template<typename T>
	inline T & ArrayList<T>::operator[](const int index)	//mozeme aj zapisovat na tuto adresu
	{
		DSRoutines::rangeCheckExcept(0, size_, "Neplatny index.");
		/*priklad: 
		T pom = (*array_)[index];
		return pom; //toto nemozeme lebo by sme vratili lokalnu premennu a bola by out of scope*/
		return (*array_)[index];
	}
															/////tieto dva operatory [] su rovnake, o to co vratia sa postara navratovy typ funkcie
	/**
		Vrati hodnotou prvok na indexe.
		index - Index prvku. 
		returns> Hodnota prvku na danom indexe.
		exception std::out_of_range - Vyhodena, ak index nepatri do zoznamu.
	*/
	template<typename T>
	inline const T ArrayList<T>::operator[](const int index) const	//len citame prvok lebo je tu const
	{
		DSRoutines::rangeCheckExcept(0, size_, "Neplatny index.");
		T pom = (*array_)[index];
		return pom;	//mozeme lebo premenna sa skopiruje passByValue
		//return (*array_)[index]; //toto je zavolanie toho operatora vyssie
	}

	/**
		Prida prvok do zoznamu.
		data - Pridavany prvok.
	*/
	template<typename T>
	inline void ArrayList<T>::add(const T & data)
	{
		//na prvu volnu poziciu uloz data (na indexe size_)
		if (size_ >= array_->size())	//ak v poli NIE je volne miesto tak zavolame zvacsovaciu metodu enlarge 
		{
			enlarge();
		}
		(*array_)[static_cast<int>(size_)] = data;	//do pola v ArrayListe na index size_ pridaj novy prvok 
		size_++;	//toto mozeme dat o riadok vyssie ale takto sa mi to zda prehladnejsie
		
	}

	/**
		Vlozi prvok do zoznamu na dany index.
		data - Pridavany prvok.
		index - Index prvku. 
		exception std::out_of_range - Vyhodena, ak index nepatri do zoznamu. 
		remarks - Ak je ako index zadana hodnota poctu prvkov (teda prvy neplatny index), metoda insert sa sprava ako metoda add.
	*/
	template<typename T>
	inline void ArrayList<T>::insert(const T & data, const int index)	//bolo na domacu ulohu
	{
		DSRoutines::rangeCheckExcept(index, size_ + 1, "Index mimo ArrayList");	//ak je index>size_ vyhodim vynimku, ak je index==size_ to je ako add()
		if (index == size_)	//prvy neplatny index 
		{
			add(data);
		} 
		else
		{
			if (size_ == array_->size())
			{
				enlarge();	//musime zvacsit pole lebo to stare je uz plne
			}
			Array<T>::copy(*array_, index, *array_, index + 1, static_cast<int>(size_) - index);
			(*array_)[index] = data;
			size_++;
		}
	}

	/**
		Odstrani prvy vyskyt prvku zo zoznamu.
		data - Odstranovany prvok.
		return - true, ak sa podarilo prvok zo zoznamu odobrat, false inak.
	*/
	template<typename T>
	inline bool ArrayList<T>::tryRemove(const T & data)		//ak sa operacia podari (-zmazanie dat cez adresu parametra-) dostaneme true, ak nie tak sa nic nestane
	{
		int index = getIndexOf(data);
		if (index > -1)
		{
			removeAt(index);
			return true;
		}
		return false;
	}

	/**
		Odstrani zo zoznamu prvok na danom indexe.
		index - Index prvku. 
		return - Odstraneny prvok. 
		exception std::out_of_range - Vyhodena, ak index nepatri do zoznamu.
	*/
	template<typename T>
	inline T ArrayList<T>::removeAt(const int index)	//funguje inverzne k metode insert
	{
		T pom = (*this)[index];
		Array<T>::copy(*array_, index + 1, *array_, index, static_cast<int>(size_) - index - 1);	//musim posunut vsetky prvky indexom po odstranenom prvku aby zaplnili medzeru po odstranenom prvku
		--size_;
		shrinkToFit();
		return pom;
	}

	/**
		Ak je v poli najviac 1 / 4 aktivnych prvkov tak zmensime jeho velkost na 1 / 2.
	*/
	template<typename T>
	inline void ArrayList<T>::shrinkToFit()
	{	
		if ((int)(array_->size()) >= 8 && ((int)(array_->size() / 4) >= size()))
		{
			Array<T>* pom = new Array<T>((int)(array_->size() / 2));
			Array<T>::copy(*array_, 0, *pom, 0, size());
			delete array_;
			array_ = pom;
		}
	}

	/**
		Vrati index prveho vyskytu prvku v zozname.
		data - Prvok, ktoreho index sa hlada.
		return - Index prveho vyskytu prvku v zozname, ak sa prvok v zozname nenachadza, vrati -1.
	*/
	template<typename T>
	inline int ArrayList<T>::getIndexOf(const T & data)	//TODO 03: ArrayList
	{
		for (size_t i = 0; i < size_; i++)
		{
			if (data == (*array_)[i])	//(*this)[i] takto zavolame svoj operator[] ArrayList-u a nie operator Array-u
			{
				return i;
			}
		}
		return -1;
		
	}

	/**
		Vymaze zoznam.
	*/
	template<typename T>
	inline void ArrayList<T>::clear()	
	{	//ak mam v arrayListe smerniky musim ich najprv uvolnit pred zavolanim clear lebo dostaneme memory leak
		size_ = 0;	//tie stare data zostavaju ale nas to netrapi lebo v buducnosti ich len prepiseme	
		shrinkToFit();
	}

	/**
		Vrati skutocny iterator na zaciatok struktury
		return - Iterator na zaciatok struktury.
		remarks - Zabezpecuje polymorfizmus.
	*/
	template<typename T>
	inline Iterator<T>* ArrayList<T>::getBeginIterator() const	//naozajstny begin a end su implementovane na pozadi
	{
		return new ArrayListIterator(this, 0);	//delete je rieseny niekde na pozadi preto sa o tento delete nestarame my
	}

	/**
		Vrati skutocny iterator na koniec struktury 
		return - Iterator na koniec struktury. 
		remarks - Zabezpecuje polymorfizmus.
	*/
	template<typename T>
	inline Iterator<T>* ArrayList<T>::getEndIterator() const //musi ukazovat az za posledny prvok nie na posledny 
	{
		return new ArrayListIterator(this, size_);
	}

	/**
		Rozsiri kapacitu zoznamu.
	*/
	template<typename T>
	inline void ArrayList<T>::enlarge()
	{
		Array<T>* pom = new Array<T>(array_->size() > 0 ? 2 * array_->size() : 4);	//vytvorim nove vacsie pole //podmienka osetruje stare pole o velkosti 0
		Array<T>::copy(*array_, 0, *pom, 0, static_cast<int>(size_));	//skopirujem data z aktualneho do noveho pola
		delete array_;								//zmazem stare pole		//kedze je to nase pole (ako objekt), nemazeme ho pomocou(delete [] ...)
		array_ = pom;								//nove pole priradim tam kde bolo stare;
	}

	/**
		 Konstruktor.
			arrayList - Zoznam, cez ktory iteruje.
			position - Pozicia, na ktorej zacina.
	*/
	template<typename T>
	inline ArrayList<T>::ArrayListIterator::ArrayListIterator(const ArrayList<T>* arrayList, int position) :
		arrayList_(arrayList),
		position_(position)
	{
	}

	/**
		Destruktor.
	*/
	template<typename T>
	inline ArrayList<T>::ArrayListIterator::~ArrayListIterator()
	{
		arrayList_ = nullptr;
		position_ = -1;
	}

	/**
		 Operator priradenia. Priradi do seba hodnotu druheho iteratora. 
			other - Druhy iterator.
			return - Vrati seba po priradeni.
	*/
	template<typename T>
	inline Iterator<T>& ArrayList<T>::ArrayListIterator::operator=(const Iterator<T>& other)
	{
		position_ = dynamic_cast<const ArrayListIterator&>(other).position_;
		arrayList_ = dynamic_cast<const ArrayListIterator&>(other).arrayList_;
		return *this;
	}

	/**
		Porovna sa s druhym iteratorom na nerovnost. 
			other - Druhy iterator.
			return - True, ak sa iteratory nerovnaju, false inak.
	*/
	template<typename T>
	inline bool ArrayList<T>::ArrayListIterator::operator!=(const Iterator<T>& other)
	{
		return position_ != dynamic_cast<const ArrayListIterator&>(other).position_ ||	//dynamic castom mozem pretypovavat len smernikove premenne alebo adresu a ak mam prvok & tak ho musim pretypovat na &
			arrayList_ != dynamic_cast<const ArrayListIterator&>(other).arrayList_;	

	}

	/**
		Vrati data, na ktore aktualne ukazuje iterator. 
			return - Data, na ktore aktualne ukazuje iterator.
	*/
	template<typename T>
	inline const T ArrayList<T>::ArrayListIterator::operator*()
	{
		return (*arrayList_)[position_];
	}

	/**
		Posunie iterator na dalsi prvok v strukture.
			return - Iterator na dalsi prvok v strukture. 
			remarks - Zvycajne vrati seba. Ak vrati iny iterator, povodny bude automaticky zruseny.
	*/
	template<typename T>
	inline Iterator<T>& ArrayList<T>::ArrayListIterator::operator++()	//toto je opertator pred, ak by sme chceli pretazit operator++ za museli by sme to napisat ako - operator++(int) -
	{
		++position_;	//to co napiseme sem nema vplyv na to aky operator momentalne pretazujeme ci pre alebo post
		return *this;
	}

}