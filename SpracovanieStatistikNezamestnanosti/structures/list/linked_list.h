#pragma once

#include "list.h"
#include "../structure_iterator.h"
#include "../ds_routines.h"

namespace structures
{

	/// <summary> Prvok jednostranne zretazeneho zoznamu. </summary>
	/// <typeparam name = "T"> Typ dat ukladanych v prvku. </typeparam>
	template<typename T>
	class LinkedListItem : public DataItem<T>	//v triede dataItem su len data a operacie ktore ich sprostredkuvaju
	{
	public:
		/// <summary> Konstruktor. </summary>
		/// <param name = "data"> Data, ktore uchovava. </param>
		LinkedListItem(T data);

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> Prvok jednstranne zretazeneho zoznamu, z ktoreho sa prevezmu vlastnosti.. </param>
		LinkedListItem(const LinkedListItem<T>& other);

		/// <summary> Destruktor. </summary>
		~LinkedListItem();

		/// <summary> Getter nasledujuceho prvku zretazeneho zoznamu. </summary>
		/// <returns> Nasledujuci prvok zretazeneho zoznamu. </returns>
		LinkedListItem<T>* getNext();

		/// <summary> Getter predchadzajuceho prvku zretazeneho zoznamu. </summary>
		/// <returns> Predchadzajuci prvok zretazeneho zoznamu. </returns>
		LinkedListItem<T>* getPrev();

		/// <summary> Setter nasledujuceho prvku zretazeneho zoznamu. </summary>
		/// <param name= "next"> Novy nasledujuci prvok zretazeneho zoznamu. </param>
		void setNext(LinkedListItem<T>* next);

		/// <summary> Setter predchadzajuceho prvku zretazeneho zoznamu. </summary>
		/// <param name= "next"> Novy predchadzajuci prvok zretazeneho zoznamu. </param>
		void setPrev(LinkedListItem<T>* prev);
	private:
		/// <summary> Nasledujuci prvok zretazeneho zoznamu. </summary>
		LinkedListItem<T>* next_;	//Nasledujuci prvok zretazeneho zoznamu.
									//ukazujeme na dalsi prvok linkedListItem 
									//pre obojstranne zretazeny len pridame ptr na previous a jeden geter jeden setter
		/// <summary> Predchadzajuci prvok zretazeneho zoznamu. </summary>
		LinkedListItem<T>* prev_;	//Predchadzajuci prvok zretazeneho zoznamu.
		
	};

	/// <summary> Jednostranne zretazeny zoznam. </summary>
	/// <typeparam name = "T"> Typ dat ukladanych v zozname. </typeparam>
	template<typename T>
	class LinkedList : public List<T>
	{
	public:
		/// <summary> Konstruktor. </summary>
		LinkedList();

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> LinkedList, z ktoreho sa prevezmu vlastnosti. </param>
		LinkedList(const LinkedList<T>& other);

		/// <summary> Destruktor. </summary>
		~LinkedList();

		/// <summary> Operacia klonovania. Vytvori a vrati duplikat zoznamu. </summary>
		/// <returns> Ukazovatel na klon struktury. </returns>
		Structure* clone() const override;

		/// <summary> Vrati pocet prvkov v zozname. </summary>
		/// <returns> Pocet prvkov v zozname. </returns>
		size_t size() const override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Zoznam, z ktoreho ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tento zoznam nachadza po priradeni. </returns>
		List<T>& operator=(const List<T>& other) override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Zoznam, z ktoreho ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tento zoznam nachadza po priradeni. </returns>
		LinkedList<T>& operator=(const LinkedList<T>& other);

		/// <summary> Vrati adresou prvok na indexe. </summary>
		/// <param name = "index"> Index prvku. </param>
		/// <returns> Adresa prvku na danom indexe. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do zoznamu. </exception>  
		T& operator[](const int index) override;

		/// <summary> Vrati hodnotou prvok na indexe. </summary>
		/// <param name = "index"> Index prvku. </param>
		/// <returns> Hodnota prvku na danom indexe. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do zoznamu. </exception>  
		const T operator[](const int index) const override;

		/// <summary> Prida prvok do zoznamu. </summary>
		/// <param name = "data"> Pridavany prvok. </param>
		void add(const T& data) override;

		/// <summary> Vlozi prvok do zoznamu na dany index. </summary>
		/// <param name = "data"> Pridavany prvok. </param>
		/// <param name = "index"> Index prvku. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do zoznamu. </exception>  
		/// <remarks> Ak je ako index zadana hodnota poctu prvkov (teda prvy neplatny index), metoda insert sa sprava ako metoda add. </remarks>
		void insert(const T& data, const int index) override;

		/// <summary> Odstrani prvy vyskyt prvku zo zoznamu. </summary>
		/// <param name = "data"> Odstranovany prvok. </param>
		/// <returns> true, ak sa podarilo prvok zo zoznamu odobrat, false inak. </returns>
		bool tryRemove(const T& data) override;

		/// <summary> Odstrani zo zoznamu prvok na danom indexe. </summary>
		/// <param name = "index"> Index prvku. </param>
		/// <returns> Odstraneny prvok. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do zoznamu. </exception>  
		T removeAt(const int index) override;

		/// <summary> Vrati index prveho vyskytu prvku v zozname. </summary>
		/// <param name = "data"> Prvok, ktoreho index sa hlada. </param>
		/// <returns> Index prveho vyskytu prvku v zozname, ak sa prvok v zozname nenachadza, vrati -1. </returns>
		int getIndexOf(const T& data) override;

		/// <summary> Vymaze zoznam. </summary>
		void clear() override;
	
		/// <summary> Vrati skutocny iterator na zaciatok struktury </summary>
		/// <returns> Iterator na zaciatok struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<T>* getBeginIterator() const override;

		/// <summary> Vrati skutocny iterator na koniec struktury </summary>
		/// <returns> Iterator na koniec struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<T>* getEndIterator() const override;
	private:
		/// <summary> Pocet prvkov v zozname. </summary>
		size_t size_;
		/// <summary> Prvy prvok zoznamu. </summary>
		LinkedListItem<T>* first_;
		/// <summary> Posledny prvok zoznamu. </summary>
		LinkedListItem<T>* last_;
	private:
		/// <summary> Vrati prvok zoznamu na danom indexe. </summary>
		/// <param name = "index"> Pozadovany index. </summary>
		/// <returns> Prvok zoznamu na danom indexe. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do zoznamu. </exception>  
		LinkedListItem<T>* getItemAtIndex(int index) const;
	private:
		/// <summary> Iterator pre LinkedList. </summary>
		class LinkedListIterator : public Iterator<T>
		{
		public:
			/// <summary> Konstruktor. </summary>
			/// <param name = "position"> Pozicia v zretazenom zozname, na ktorej zacina. </param>
			LinkedListIterator(LinkedListItem<T>* position);

			/// <summary> Destruktor. </summary>
			~LinkedListIterator();

			/// <summary> Operator priradenia. Priradi do seba hodnotu druheho iteratora. </summary>
			/// <param name = "other"> Druhy iterator. </param>
			/// <returns> Vrati seba po priradeni. </returns>
			Iterator<T>& operator= (const Iterator<T>& other) override;

			/// <summary> Porovna sa s druhym iteratorom na nerovnost. </summary>
			/// <param name = "other"> Druhy iterator. </param>
			/// <returns> True, ak sa iteratory nerovnaju, false inak. </returns>
			bool operator!=(const Iterator<T>& other) override;

			/// <summary> Vrati data, na ktore aktualne ukazuje iterator. </summary>
			/// <returns> Data, na ktore aktualne ukazuje iterator. </returns>
			const T operator*() override;

			/// <summary> Posunie iterator na dalsi prvok v strukture. </summary>
			/// <returns> Iterator na dalsi prvok v strukture. </returns>
			/// <remarks> Zvycajne vrati seba. Ak vrati iny iterator, povodny bude automaticky zruseny. </remarks>
			Iterator<T>& operator++() override;
		private:
			/// <summary> Aktualna pozicia v zozname. </summary>
			LinkedListItem<T>* position_;
		};
	};
	//===========================================================LinkedListItem====================
	/**
		Konstruktor.
		data - Data, ktore uchovava.
	*/
	template<typename T>
	inline LinkedListItem<T>::LinkedListItem(T data):
		DataItem<T>(data),
		next_(nullptr),
		prev_(nullptr)
	{
	}

	/**
		Kopirovaci konstruktor. 
		other - Prvok jednstranne zretazeneho zoznamu, z ktoreho sa prevezmu vlastnosti.
	*/
	template<typename T>
	inline LinkedListItem<T>::LinkedListItem(const LinkedListItem<T>& other):
		DataItem<T>(other),
		next_(other.next_),
		prev_(other.prev_)
	{
	}

	/**
		Destruktor.
	*/
	template<typename T>
	inline LinkedListItem<T>::~LinkedListItem()
	{
		next_ = nullptr;
		prev_ = nullptr;
	}
	
	/**
		Getter nasledujuceho prvku zretazeneho zoznamu. 
		returns - Nasledujuci prvok zretazeneho zoznamu.
	*/
	template<typename T>
	inline LinkedListItem<T> * LinkedListItem<T>::getNext()
	{
		return next_;
	}

	/**
		Getter predchadzajuceho prvku zretazeneho zoznamu. 
		return - Predchadzajuci prvok zretazeneho zoznamu.
	*/
	template<typename T>
	inline LinkedListItem<T>* LinkedListItem<T>::getPrev()
	{
		return prev_;
	}

	/**
		Setter nasledujuceho prvku zretazeneho zoznamu. 
		next - Novy nasledujuci prvok zretazeneho zoznamu.
	*/
	template<typename T>
	inline void LinkedListItem<T>::setNext(LinkedListItem<T> * next)
	{
		next_ = next;
	}

	/**
		Setter predchadzajuceho prvku zretazeneho zoznamu. 
		next - Novy predchadzajuci prvok zretazeneho zoznamu.
	*/
	template<typename T>
	inline void LinkedListItem<T>::setPrev(LinkedListItem<T>* prev)
	{
		prev_ = prev;
	}
	//===========================================================LinkedList====================
	/**
		Konstruktor. 
	*/
	template<typename T>
	inline LinkedList<T>::LinkedList():
		List(),
		size_(0),
		first_(nullptr),
		last_(nullptr)
	{
	}

	/**
		Kopirovaci konstruktor.
		other - LinkedList, z ktoreho sa prevezmu vlastnosti.
	*/
	template<typename T>
	inline LinkedList<T>::LinkedList(const LinkedList<T>& other):
		LinkedList()	//toto vytvori prazdny linkedList, takze uz len potrebujeme priradit data do seba z other
	{
		*this = other;
	}

	/**
		 Destruktor. 
	*/
	template<typename T>
	inline LinkedList<T>::~LinkedList()
	{
		clear();
	}

	/**
		Operacia klonovania. Vytvori a vrati duplikat zoznamu. 
		returns - Ukazovatel na klon struktury. 
	*/
	template<typename T>
	inline Structure * LinkedList<T>::clone() const
	{
		return new LinkedList<T>(*this);
	}

	/**
		Vrati pocet prvkov v zozname. 
		returns - Pocet prvkov v zozname.
	*/
	template<typename T>
	inline size_t LinkedList<T>::size() const
	{
		return size_;
	}

	/**
		Operator priradenia. 
		other - Zoznam, z ktoreho ma prebrat vlastnosti.
		return - Adresa, na ktorej sa tento zoznam nachadza po priradeni. 
	*/
	template<typename T>
	inline List<T>& LinkedList<T>::operator=(const List<T>& other)
	{
		if (this != &other)
		{
			*this = dynamic_cast<const LinkedList<T>&>(other);
		}
		return *this;
	}

	/**
		Operator priradenia. 
		other - Zoznam, z ktoreho ma prebrat vlastnosti. 
		return - Adresa, na ktorej sa tento zoznam nachadza po priradeni.
	*/
	template<typename T>
	inline LinkedList<T>& LinkedList<T>::operator=(const LinkedList<T>& other)	//ten nas list moze mat vlastny obsah takze musime nas zoznam zmazat
																				//potom musime prekopirovat cely obsah other do seba
	{
		if (this != &other)	
		{
			clear();
			for each (T data in other)
			{
				add(data);
			}
			/*for (T data : other)
			{
				add(data);
			}*/
		}
		return *this;
	}

	/**
		Vrati adresou prvok na indexe. 
		index - Index prvku.
		return - Adresa prvku na danom indexe. 
		exception std::out_of_range - Vyhodena, ak index nepatri do zoznamu. 
	*/
	template<typename T>
	inline T & LinkedList<T>::operator[](const int index)
	{
		DSRoutines::rangeCheckExcept(index, size_, "Nespravny index LinkedList");
		return getItemAtIndex(index)->accessData();	//tu je problem s VS so sablonami preto neukazuje po ->ziadne autocomplete
	}

	/**
		Vrati hodnotou prvok na indexe. 
		index - Index prvku. 
		return - Hodnota prvku na danom indexe. 
		exception std::out_of_range - Vyhodena, ak index nepatri do zoznamu.
	*/
	template<typename T>
	inline const T LinkedList<T>::operator[](const int index) const
	{
		DSRoutines::rangeCheckExcept(index, size_, "Nespravny index LinkedList");
		return getItemAtIndex(index)->accessData();	//tu je problem s VS so sablonami preto neukazuje po ->ziadne autocomplete
	}

	/**
		Prida prvok do zoznamu. 
		data - Pridavany prvok.
	*/
	template<typename T>
	inline void LinkedList<T>::add(const T & data)
	{
		LinkedListItem<T>* newItem = new LinkedListItem<T>(data); //najskor vytvorim nove data
		if (first_ == nullptr)	//ak je zoznam prazdny
		{
			first_ = newItem;	//ptr first zacne ukazovat na nove data -> a na konci metody ja last nastavime na tie iste data
		}
		else	//ak zoznam nie je prazdny
		{

			last_->setNext(newItem);	//next na poslednom prvku nastavim na nove data
			newItem->setPrev(last_);	//previous na novom prvku nastavim na last
		}		
		last_ = newItem;	//nakoniec nastavim ptr last na nove data
		size_++;
	}

	/**
		Vlozi prvok do zoznamu na dany index. 
		data - Pridavany prvok.
		index - Index prvku. 
		exception std::out_of_range - Vyhodena, ak index nepatri do zoznamu.
		remarks - Ak je ako index zadana hodnota poctu prvkov (teda prvy neplatny index), metoda insert sa sprava ako metoda add. 
	*/
	template<typename T>
	inline void LinkedList<T>::insert(const T & data, const int index)	//ak chcem vkladat na index i musim sa dostat na index i-1
	{	
		DSRoutines::rangeCheckExcept(index, size_ + 1, "Nespravny index LinkedList");
		
		
		if (index == size_)		//ak vkladam na koniec
		{
			add(data);	//je to ako add, tam zvisime size_ o 1
		}
		else	//ak zoznam nie je prazdny
		{			
			LinkedListItem<T>* newItem = new LinkedListItem<T>(data);	//najskor potrebujeme vytvorit obal na data -> kam vlozime data
			if (index == 0) //a zaroven vkladam na zaciatok
			{
				newItem->setNext(first_);	//nastavime noveData.next na povodny prvy prvok 
				first_->setPrev(newItem);	//nastavime povodny first.prev na novy prvok
				first_ = newItem;	//vkladame na zaciatok takze novy prvok je teraz prvym prvkom
			}
			else	//alebo vkladam niekam do stredu
			{
				LinkedListItem<T>* prev = getItemAtIndex(index - 1);	//prvok predchadzajuci indexu kam chceme novy prvok vlozit
				newItem->setNext(prev->getNext());	//new.next_ noveho prvku nastavim na prev.next
				newItem->setPrev(prev);		//new.prev_ nastavim na predosly prvok prev
				prev->setNext(newItem);		//prev.next_ nastavim na novy prvok new
				newItem->getNext()->setPrev(newItem);	//next.prev_ nastavim na novy prvok new
			}
			size_++;
		}
	}

	/**
		Odstrani prvy vyskyt prvku zo zoznamu. 
		data - Odstranovany prvok. 
		return - true, ak sa podarilo prvok zo zoznamu odobrat, false inak.
	*/
	template<typename T>
	inline bool LinkedList<T>::tryRemove(const T & data)
	{
		int index = getIndexOf(data);	//pozriem sa ci sa prvok nachadza v zozname
		if (index != -1)
		{
			removeAt(index);	//ak ano tak ho odstranim na indexe
			return true;
		}
		return false;
	}

	/**
		Odstrani zo zoznamu prvok na danom indexe.
		index - Index prvku. 
		return - Odstraneny prvok. 
		exception std::out_of_range - Vyhodena, ak index nepatri do zoznamu. 
		{
		Zma�e prvok zoznamu na danom indexe a vr�ti jeho d�ta. 
		Vyu�ite met�du getItemAtIndex, ktorou prejdete pred mazan� prvok. 
		Uprav� re�az prvkov na tomto mieste (�vyrad� mazan� prvok zo zre�azenia). 
		Mazan� prvok zoznamu dealokuje! 
		Ak ma�ete zo za�iatku resp. z konca zoznamu, nezabudnite aktualizova� atrib�ty first_ resp. last_. 
		Zn�i hodnotu atrib�tu size_.
		}
	*/
	template<typename T>
	inline T LinkedList<T>::removeAt(const int index)
	{
		DSRoutines::rangeCheckExcept(index, size_, "Nespravny index LinkedList");
		LinkedListItem<T>* pom;	//pomocna premenna
		T vraciam; //premenna v ktorej bude navratova hodnota

		if (index == 0)		//ak mazem prvy prvok
		{
			if (first_ == last_)	//ak v zozname nie je dalsi prvok
			{
				vraciam = first_->accessData();		//ulozim si data ktore vratim
				delete first_;		//a zmazem prvok
				first_ = nullptr;
				last_ = nullptr;
			} 
			else	//ak v zozname, po prvom prvku, existuje aspon jeden nasledujuci prvok
			{
				pom = first_;	//zapamatam si adresu odstranovaneho prvku
				first_ = first_->getNext();		//prvy prvok nastavim aby ukazoval az na druhy prvok, teda druhy bude novy prvy
				vraciam = pom->accessData();	//ulozim si data ktore vratim
				delete pom;		//zmazem prvok
			}
		}
		else	//ak mazem prvok iny ako ten prvy
		{				
			pom = getItemAtIndex(index - 1);		//dostanem sa ku prvku o jeden pred prvkom, ktory chcem zmazat
			LinkedListItem<T>* mazem = pom->getNext();	//prvok ktory chcem zmazat

			pom->setNext(mazem->getNext());		//nastav na predchadzajucom prvku ukazatel na dalsi prvok tak, aby ukazoval az za prvok, ktory chcem zmazat
			mazem->getNext()->setPrev(pom);		//nastav na nasledujucom prvku ukazatel na predosly tak, aby ukazoval pred prvok ktory chcem zmazat
			vraciam = mazem->accessData();

			if (index == size_ - 1) //ak mazeme posledny prvok
			{
				last_ = pom;	//ukazatel last presunieme na predposledny prvok
			}
			delete mazem;	//dealokuj
		}
		size_--;	//znizim pocet prvkov o 1
		return vraciam;
	}

	/**
		Vrati index prveho vyskytu prvku v zozname. 
		data - Prvok, ktoreho index sa hlada. 
		return - Index prveho vyskytu prvku v zozname, ak sa prvok v zozname nenachadza, vrati -1.
	*/
	template<typename T>
	inline int LinkedList<T>::getIndexOf(const T & data)
	{
		LinkedListItem<T>* cur = first_;	//current ukazuje na aktualny prvok
		for (int i = 0; i < size_; i++)
		{
			if (cur->accessData() == data)
			{
				return i;
			}
			cur = cur->getNext();
		}
		return -1;
	}

	/**
		Vymaze zoznam.
	*/
	template<typename T>
	inline void LinkedList<T>::clear()
	{
		LinkedListItem<T>* cur = first_;
		while (cur != nullptr) {
			cur = cur->getNext();
			delete first_;
			first_ = cur;
		}
		last_ = nullptr;
		size_ = 0;
	}

	/**
		Vrati skutocny iterator na zaciatok struktury
		return - Iterator na zaciatok struktury.
		remark - Zabezpecuje polymorfizmus. 
	*/
	template<typename T>
	inline Iterator<T>* LinkedList<T>::getBeginIterator() const
	{
		return new LinkedListIterator(first_);
	}

	/**
		Vrati skutocny iterator na koniec struktury
		return - Iterator na koniec struktury.
		remark - Zabezpecuje polymorfizmus. 
	*/
	template<typename T>
	inline Iterator<T>* LinkedList<T>::getEndIterator() const
	{
		return new LinkedListIterator(nullptr);	//ukazuje za posledny prvok
	}

	/**
		Vrati prvok zoznamu na danom indexe. 
		index - Pozadovany index.
		return - Prvok zoznamu na danom indexe.
		exception std::out_of_range - Vyhodena, ak index nepatri do zoznamu.
	*/
	template<typename T>
	inline LinkedListItem<T>* LinkedList<T>::getItemAtIndex(int index) const
	{
		LinkedListItem<T>* cur;

		if (index < size_/2)	//ak sa prvok nachadza v prvej polovici zoznamu pojdeme k nemu od zaciatku
		{
			cur = first_;
			for (int i = 0; i < index; i++)
			{
				cur = cur->getNext();
			}
		} 
		else
		{
			cur = last_;
			for (int i = size_ - 1; i > index; i--)
			{
				cur = cur->getPrev();
			}
		}				
		return cur;
	}

	/**
		Konstruktor. 
		position - Pozicia v zretazenom zozname, na ktorej zacina.
	*/
	template<typename T>
	inline LinkedList<T>::LinkedListIterator::LinkedListIterator( LinkedListItem<T> * position):
		position_(position)
	{
	}

	/**
		Destruktor.
	*/
	template<typename T>
	inline LinkedList<T>::LinkedListIterator::~LinkedListIterator()
	{
		position_ = nullptr;
	}

	/**
		Operator priradenia. Priradi do seba hodnotu druheho iteratora. 
			other - Druhy iterator. 
			return - Vrati seba po priradeni. 
	*/
	template<typename T>
	inline Iterator<T>& LinkedList<T>::LinkedListIterator::operator=(const Iterator<T>& other)
	{
		position_ = dynamic_cast<const LinkedListIterator&>(other).position_;
		return *this;
	}

	/**
		Porovna sa s druhym iteratorom na nerovnost. 
			other - Druhy iterator.
			return - True, ak sa iteratory nerovnaju, false inak.	
	*/
	template<typename T>
	inline bool LinkedList<T>::LinkedListIterator::operator!=(const Iterator<T>& other)
	{
		return position_ != dynamic_cast<const LinkedListIterator&>(other).position_;

	}

	/**
		Vrati data, na ktore aktualne ukazuje iterator. 
		return - Data, na ktore aktualne ukazuje iterator. 
	*/
	template<typename T>
	inline const T LinkedList<T>::LinkedListIterator::operator*()
	{
		return position_->accessData();
	}

	/**
		Posunie iterator na dalsi prvok v strukture.
			return - Iterator na dalsi prvok v strukture. 
			remarks - Zvycajne vrati seba. Ak vrati iny iterator, povodny bude automaticky zruseny.
	*/
	template<typename T>
	inline Iterator<T>& LinkedList<T>::LinkedListIterator::operator++()
	{
		position_ = position_->getNext();
		return *this;
	}
}