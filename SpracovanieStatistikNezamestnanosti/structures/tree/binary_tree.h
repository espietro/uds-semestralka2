#pragma once

#include "k_way_tree.h"
#include "tree.h"
#include <queue>

namespace structures
{
	/// <summary> Vrchol binarneho stromu. </summary>
	/// <typeparam name = "T"> Typ dat ukladanych vo vrchole stromu. </typeparam>
	template <typename T>
	class BinaryTreeNode : public KWayTreeNode<T,2>
	{
	public:
		/// <summary> Konstruktor. </summary>
		/// <param name = "data"> Data, ktore uchovava. </param>
		BinaryTreeNode(T data);

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> Vrchol binarneho stromu, z ktoreho sa prevezmu vlastnosti. </param>
		/// <remarks> Vytvori sa ako plytka kopia vrcholu other. </remarks>
		BinaryTreeNode(const BinaryTreeNode<T>& other);

		/// <summary> Vytvori plytku kopiu vrchola (teda skopiruje iba data a smerniky na synov). </summary>
		/// <returns> Vystvorena plytka kopia vrcholu. </returns>
		TreeNode<T>* shallowCopy() override;

		/// <summary> Getter otca. </summary>
		/// <returns> Otec. </returns>
		BinaryTreeNode<T>* getParent() const;

		/// <summary> Getter laveho syna. </summary>
		/// <returns> Lavy syn. </returns>
		BinaryTreeNode<T>* getLeftSon() const;

		/// <summary> Getter praveho syna. </summary>
		/// <returns> Pravy syn. </returns>
		BinaryTreeNode<T>* getRightSon() const;

		/// <summary> Setter laveho syna. </summary>
		/// <param name = "leftSon"> Novy lavy syn. </returns>
		/// <returns> Nahradeny lavy syn vrcholu. </returns>
		/// <remarks> Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. </remarks>
		void setLeftSon(BinaryTreeNode<T>* leftSon);

		/// <summary> Setter praveho syna. </summary>
		/// <param name = "rightSon"> Novy pravy syn. </returns>
		/// <returns> Nahradeny pravy vrcholu. </returns>
		/// <remarks> Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. </remarks>
		void setRightSon(BinaryTreeNode<T>* rightSon);

		/// <summary> Zameni laveho syna. </summary>
		/// <param name = "leftSon"> Novy lavy syn. </returns>
		/// <returns> Nahradeny lavy syn vrcholu. </returns>
		/// <remarks> Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. </remarks>
		BinaryTreeNode<T>* changeLeftSon(BinaryTreeNode<T>* leftSon);

		/// <summary> Zameni praveho syna. </summary>
		/// <param name = "rightSon"> Novy pravy syn. </returns>
		/// <returns> Nahradeny pravy vrcholu. </returns>
		/// <remarks> Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. </remarks>
		BinaryTreeNode<T>* changeRightSon(BinaryTreeNode<T>* rightSon);

		/// <summary> Zisti, ci je lavym synom svojho otca. </summary>
		/// <returns> true, ak je lavym synom svojho otca, false ak nema otca alebo nie je jeho lavym synom. </returns>
		bool isLeftSon() const;

		/// <summary> Zisti, ci je pravym synom svojho otca. </summary>
		/// <returns> true, ak je pravym synom svojho otca, false ak nema otca alebo nie je jeho pravym synom. </returns>
		bool isRightSon() const;

		/// <summary> Zisti, ci ma laveho syna. </summary>
		/// <returns> true, ma laveho syna, false inak. </returns>
		bool hasLeftSon() const;

		/// <summary> Zisti, ci ma praveho syna. </summary>
		/// <returns> true, ma praveho syna, false inak. </returns>
		bool hasRightSon() const;

	private:
		static const int LEFT_SON = 0;
		static const int RIGHT_SON = 1;
	};

	/// <summary> Binary strom. </summary>
	/// <typeparam name = "T"> Typ dat ukladanych v strome. </typeparam>
	template <typename T>
	class BinaryTree : public KWayTree<T,2>
	{
	public:
		/// <summary> Konstruktor. </summary>
		BinaryTree();

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> Binarny strom, z ktoreho sa prevezmu vlastnosti. </param>
		BinaryTree(const BinaryTree<T>& other);

		/// <summary> Operacia klonovania. Vytvori a vrati duplikat udajovej struktury. </summary>
		/// <returns> Ukazovatel na klon struktury. </returns>
		Structure* clone() const override;

		/// <summary> Vytvori a vrati instanciu vrcholu binarneho stromu. </summary>
		/// <returns> Vytvorena instancia vrcholu binarneho stromu. </returns>
		TreeNode<T>* createTreeNodeInstance() override;

		/// <summary> Vrati skutocny iterator na zaciatok struktury </summary>
		/// <returns> Iterator na zaciatok struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<T>* getBeginIterator() const override;

		/// <summary> Vrati skutocny iterator na koniec struktury </summary>
		/// <returns> Iterator na koniec struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<T>* getEndIterator() const override;
	public:
		class InOrderTreeIterator : public TreeIterator
		{
		public:
			/// <summary> Konstruktor. </summary>
			/// <param name = "startNode"> Vrchol podstromu, cez ktory ma iterovat. </param>
			InOrderTreeIterator(TreeNode<T>* const startNode);
		private:
			/// <summary> Zostavi do path_ prehliadku binarneho stromu v poradi inorder. </summary>
			/// <param name = "path"> Cesta stromom, ktoru zostavuje. </param>
			/// <param name = "current"> Aktualny spracovavany vrchol stromu. </param>
			void populatePath(TreeNode<T>* const current);
		};
	};

	/**
		Konstruktor. 
		data - Data, ktore uchovava. 
	*/
	template<typename T>
	inline BinaryTreeNode<T>::BinaryTreeNode(T data):
		KWayTreeNode<T, 2>(data)
	{
	}

	/**
		Kopirovaci konstruktor. 
		other - Vrchol binarneho stromu, z ktoreho sa prevezmu vlastnosti. 
		remarks - Vytvori sa ako plytka kopia vrcholu other. 
	*/
	template<typename T>
	inline BinaryTreeNode<T>::BinaryTreeNode(const BinaryTreeNode<T>& other):
		KWayTreeNode<T, 2>(other)
	{
	}

	/**
		Vytvori plytku kopiu vrchola (teda skopiruje iba data a smerniky na synov). 
		returns - Vystvorena plytka kopia vrcholu. 
	*/
	template<typename T>
	inline TreeNode<T>* BinaryTreeNode<T>::shallowCopy()
	{
		return new BinaryTreeNode<T>(*this);		
	}

	/**
		Getter otca. 
		returns - Otec. 
	*/
	template<typename T>
	inline BinaryTreeNode<T>* BinaryTreeNode<T>::getParent() const
	{
		return dynamic_cast<BinaryTreeNode<T>*>(parent_);
	}

	/**
		Getter laveho syna. 
		returns - Lavy syn. 
	*/
	template<typename T>
	inline BinaryTreeNode<T>* BinaryTreeNode<T>::getLeftSon() const
	{
		return dynamic_cast<BinaryTreeNode<T>*>((*children_)[LEFT_SON]);
	}

	/**
		Getter praveho syna. 
		returns - Pravy syn. 
	*/
	template<typename T>
	inline BinaryTreeNode<T>* BinaryTreeNode<T>::getRightSon() const
	{
		return dynamic_cast<BinaryTreeNode<T>*>((*children_)[RIGHT_SON]);
	}

	/**
		Setter laveho syna. Vymeni stareho syna za noveho laveho syna. 
		leftSon - Novy lavy syn. Otec bude prepisany na tento vrchol.
		oldLeftSon - Nahradeny lavy syn vrcholu. 
		remarks - Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. 
	*/
	template<typename T>
	inline void BinaryTreeNode<T>::setLeftSon(BinaryTreeNode<T>* leftSon)
	{
		changeLeftSon(leftSon);
	}

	/**
		Setter praveho syna. Vymeni stareho syna za noveho praveho syna.
		rightSon - Novy pravy syn. Otec bude prepisany na tento vrchol.
		oldRightSon - Nahradeny pravy vrcholu. 
		remarks - Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. 
	*/
	template<typename T>
	inline void BinaryTreeNode<T>::setRightSon(BinaryTreeNode<T>* rightSon)
	{
		changeRightSon(rightSon);
	}

	/**
		Zameni laveho syna. Novemu synovi prepise otca.
		leftSon - Novy lavy syn. 
		returns - Nahradeny lavy syn vrcholu. 
		remarks - Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. 
	*/
	template<typename T>
	inline BinaryTreeNode<T>* BinaryTreeNode<T>::changeLeftSon(BinaryTreeNode<T>* leftSon)
	{	//najskor vybrieme stareho syna tohto vrcholu
		BinaryTreeNode<T>* oldLeftSon = dynamic_cast<BinaryTreeNode<T>*>((*children_)[LEFT_SON]);
		(*children_)[LEFT_SON] = leftSon;	//pointer na stareho syna prepiseme novym synom
		if (leftSon != nullptr)
		{	//ak novym synom je vrchol a nie null, tak jeho otca nasmerujem na seba
			leftSon->setParent(this);
		}	
		return oldLeftSon;
	}

	/**
		Zameni praveho syna. Novemu synovi prepise otca.
		rightSon - Novy pravy syn. 
		returns - Nahradeny pravy vrcholu. 
		remarks - Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. 
	*/
	template<typename T>
	inline BinaryTreeNode<T>* BinaryTreeNode<T>::changeRightSon(BinaryTreeNode<T>* rightSon)
	{
		BinaryTreeNode<T>* oldRightSon = dynamic_cast<BinaryTreeNode<T>*>((*children_)[RIGHT_SON]);
		(*children_)[RIGHT_SON] = rightSon;
		if (rightSon != nullptr)
		{
			rightSon->setParent(this);
		}		
		return oldRightSon;
	}

	/**
		Zisti, ci je lavym synom svojho otca. 
		returns - true, ak je lavym synom svojho otca, false ak nema otca alebo nie je jeho lavym synom. 
	*/
	template<typename T>
	inline bool BinaryTreeNode<T>::isLeftSon() const
	{		
		return parent_ != nullptr && 
			dynamic_cast<BinaryTreeNode<T>*>(parent_)->getLeftSon() == this;
	}

	/**
		Zisti, ci je pravym synom svojho otca. 
		returns - true, ak je pravym synom svojho otca, false ak nema otca alebo nie je jeho pravym synom. 
	*/
	template<typename T>
	inline bool BinaryTreeNode<T>::isRightSon() const
	{		
		return parent_ != nullptr && 
			dynamic_cast<BinaryTreeNode<T>*>(parent_)->getRightSon() == this;
	}

	/**
		Zisti, ci ma laveho syna. 
		returns - true, ma laveho syna, false inak. 
	*/
	template<typename T>
	inline bool BinaryTreeNode<T>::hasLeftSon() const
	{
		return (*children_)[LEFT_SON] != nullptr;
	}

	/**
		Zisti, ci ma praveho syna. 
		returns - true, ma praveho syna, false inak. 
	*/
	template<typename T>
	inline bool BinaryTreeNode<T>::hasRightSon() const
	{
		return (*children_)[RIGHT_SON] != nullptr;
	}

	/**
		Konstruktor. 
	*/
	template<typename T>
	inline BinaryTree<T>::BinaryTree():
		KWayTree<T, 2>()
	{
	}

	/**
		Kopirovaci konstruktor. 
		other - Binarny strom, z ktoreho sa prevezmu vlastnosti. 
	*/
	template<typename T>
	inline BinaryTree<T>::BinaryTree(const BinaryTree<T>& other):
		KWayTree<T, 2>(other)
	{
	}

	/**
		Operacia klonovania. Vytvori a vrati duplikat udajovej struktury. 
		returns - Ukazovatel na klon struktury. 
	*/
	template<typename T>
	inline Structure * BinaryTree<T>::clone() const
	{
		return new BinaryTree<T>(*this);
	}

	/**
		Vytvori a vrati instanciu vrcholu binarneho stromu. 
		returns - Vytvorena instancia vrcholu binarneho stromu. 
	*/
	template<typename T>
	inline TreeNode<T>* BinaryTree<T>::createTreeNodeInstance()
	{
		T data = T();
		return new BinaryTreeNode<T>(data);
	}

	/**
		Vrati skutocny iterator na zaciatok struktury 
		returns - Iterator na zaciatok struktury. 
		remarks - Zabezpecuje polymorfizmus. 
	*/
	template<typename T>
	inline Iterator<T>* BinaryTree<T>::getBeginIterator() const
	{
		return new InOrderTreeIterator(root_);
	}

	/**
		Vrati skutocny iterator na koniec struktury 
		returns - Iterator na koniec struktury. 
		remarks - Zabezpecuje polymorfizmus. 
	*/
	template<typename T>
	inline Iterator<T>* BinaryTree<T>::getEndIterator() const
	{
		return new InOrderTreeIterator(nullptr);
	}

	/**
		Konstruktor. 
		startNode - Vrchol podstromu, cez ktory ma iterovat. 
	*/
	template<typename T>
	inline BinaryTree<T>::InOrderTreeIterator::InOrderTreeIterator(TreeNode<T>* const startNode):
		TreeIterator()
	{
		populatePath(startNode);
	}

	/**
		Zostavi do path_ prehliadku binarneho stromu v poradi inorder. 
		path - Cesta stromom, ktoru zostavuje. 
		current - Aktualny spracovavany vrchol stromu. 
	*/
	template<typename T>
	inline void BinaryTree<T>::InOrderTreeIterator::populatePath(TreeNode<T>* const current)
	{
		if (current != nullptr)
		{
			TreeNode<T>* leftSon = current->getSon(0);
			TreeNode<T>* rightSon = current->getSon(1);
			if (leftSon != nullptr)
			{	
				populatePath(leftSon);
			}
			path_->push(current);
			if (rightSon != nullptr)
			{
				populatePath(rightSon);
			}

		}
	}

}

