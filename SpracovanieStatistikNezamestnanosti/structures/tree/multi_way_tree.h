#pragma once

#include "tree.h"
#include "../list/list.h"
#include "../list/array_list.h"

namespace structures
{
	/// <summary> Vrchol viaccestneho stromu. </summary>
	/// <typeparam name = "T"> Typ dat ukladanych vo vrchole stromu. </typeparam>
	template <typename T>
	class MultiWayTreeNode : public TreeNode<T>
	{
	public:
		/// <summary> Konstruktor. </summary>
		/// <param name = "data"> Data, ktore uchovava. </param>
		MultiWayTreeNode(T data);

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> Vrchol viaccestneho stromu, z ktoreho sa prevezmu vlastnosti. </param>
		/// <remarks> Vytvori sa ako plytka kopia vrcholu other. </remarks>
		MultiWayTreeNode(const MultiWayTreeNode<T>& other);

		/// <summary> Destruktor. </summary>
		~MultiWayTreeNode();

		/// <summary> Vytvori plytku kopiu vrchola (teda skopiruje iba data a smerniky na synov). </summary>
		/// <returns> Vystvorena plytka kopia vrcholu. </returns>
		TreeNode<T>* shallowCopy() override;

		/// <summary> Test, ci je list. </summary>
		/// <returns> true, ak je list, false inak. </returns>
		bool isLeaf() override;

		/// <summary> Spristupni syna vrcholu podla jeho pozicie v tomto vrchole. </summary>
		/// <param name = "order"> Poradie syna. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak neexistuje syn s takym poradim. </exception>
		/// <returns> Syn vrcholu. </returns>
		TreeNode<T>* getSon(int order) const override;

		/// <summary> Vlozi noveho syna vrcholu na dane miesto. </summary>
		/// <param name = "order"> Poradie syna. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak neexistuje syn s takym poradim. </exception>
		/// <remarks> Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. </remarks>
		void insertSon(TreeNode<T>* son, int order) override;

		/// <summary> Nahradi syna vrcholu na danom mieste. </summary>
		/// <param name = "order"> Poradie syna. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak je poradie neplatne. </exception>
		/// <returns> Nahradeny syn vrcholu. </returns>
		/// <remarks> Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. </remarks>
		TreeNode<T>* replaceSon(TreeNode<T>* son, int order) override;

		/// <summary> Odstrani syna na danom mieste. </summary>
		/// <param name = "order"> Poradie syna. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak neexistuje syn s takym poradim. </exception>
		/// <returns> Odstraneny syn vrcholu. </returns>
		/// <remarks> Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. </remarks>
		TreeNode<T>* removeSon(int order) override;

		/// <summary> Vrati pocet synov vrcholu. </summary>
		/// <returns> Vzdy K. </returns>
		int degree() override;
	protected:
		/// <summary> Synova vrchola. </summary>
		List<MultiWayTreeNode<T>*>* children_;
	};

	/// <summary> Viaccestny strom. </summary>
	/// <typeparam name = "T"> Typ dat ukladanych v strome. </typepram>
	template <typename T>
	class MultiWayTree : public Tree<T>
	{
	public:
		/// <summary> Konstruktor. </summary>
		MultiWayTree();

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> Viaccestny strom, z ktoreho sa prevezmu vlastnosti. </param>
		MultiWayTree(const MultiWayTree<T>& other);

		/// <summary> Operacia klonovania. Vytvori a vrati duplikat udajovej struktury. </summary>
		/// <returns> Ukazovatel na klon struktury. </returns>
		Structure* clone() const override;

		/// <summary> Vytvori a vrati instanciu vrcholu k-cestneho stromu. </summary>
		/// <returns> Vytvorena instancia vrcholu k-cestneho stromu. </returns>
		TreeNode<T>* createTreeNodeInstance() override;
	};


	/**
		Konstruktor. 
		data - Data, ktore uchovava.
	*/
	template<typename T>
	inline MultiWayTreeNode<T>::MultiWayTreeNode(T data) :
		TreeNode<T>(data),
		children_(new ArrayList<MultiWayTreeNode<T>*>())
	{
	}

	/**
		Kopirovaci konstruktor. 
		other - Vrchol viaccestneho stromu, z ktoreho sa prevezmu vlastnosti.
		remarks - Vytvori sa ako plytka kopia vrcholu other.
	*/
	template<typename T>
	inline MultiWayTreeNode<T>::MultiWayTreeNode(const MultiWayTreeNode<T>& other) :
		TreeNode<T>(other),
		children_(dynamic_cast<ArrayList<MultiWayTreeNode<T>*>*>(other.children_->clone()))
	{
	}

	/**
		Destruktor.
	*/
	template<typename T>
	inline MultiWayTreeNode<T>::~MultiWayTreeNode()
	{
		for (int i = children_->size() - 1; i >= 0; i--)
		{
			delete (*children_)[i];
		}
		delete children_;
		children_ = nullptr;
	}

	/**
		Vytvori plytku kopiu vrchola (teda skopiruje iba data a smerniky na synov).
		return - Vystvorena plytka kopia vrcholu.
	*/
	template<typename T>
	inline TreeNode<T>* MultiWayTreeNode<T>::shallowCopy()
	{
		MultiWayTreeNode<T>* ret = new MultiWayTreeNode<T>(*this);
		return ret;
	}

	/**
		Test, ci je list. 
		return - true, ak je list, false inak. 
	*/
	template<typename T>
	inline bool MultiWayTreeNode<T>::isLeaf()
	{
		if (children_->isEmpty())
		{
			return true;
		}
		return false;
	}

	/**
		Spristupni syna vrcholu podla jeho pozicie v tomto vrchole.
		order - Poradie syna. 
		exception std::out_of_range - Vyhodena, ak neexistuje syn s takym poradim.
		return - Syn vrcholu.
	*/
	template<typename T>
	inline TreeNode<T>* MultiWayTreeNode<T>::getSon(int order) const
	{		
		try
		{
			return (*children_)[order];
		}
		catch (std::out_of_range ex) {	throw std::out_of_range("Syn neexistuje!");		}
	}

	/**
		 Vlozi noveho syna vrcholu na dane miesto. 
		order - Poradie syna. 
		exception std::out_of_range - Vyhodena, ak neexistuje syn s takym poradim. 
		remarks - Vsetky smerniky zucastnenych vrcholov budu spravne nastavene.
	*/
	template<typename T>
	inline void MultiWayTreeNode<T>::insertSon(TreeNode<T>* son, int order)
	{
		try
		{
			children_->insert(dynamic_cast<MultiWayTreeNode<T>*>(son), order);
			son->setParent(this);
		} 
		catch (std::out_of_range ex) {	throw std::out_of_range("Syn neexistuje!");		}
	}

	/**
		Nahradi syna vrcholu na danom mieste.
		order - Poradie syna.
		exception std::out_of_range - Vyhodena, ak je poradie neplatne.
		return - Nahradeny syn vrcholu. 
		remarks - Vsetky smerniky zucastnenych vrcholov budu spravne nastavene.
	*/
	template<typename T>
	inline TreeNode<T>* MultiWayTreeNode<T>::replaceSon(TreeNode<T>* son, int order)
	{
		try
		{
			MultiWayTreeNode<T>* oldSon = (*children_)[order];	//pokusim sa pristupit k danemu synovi	
			oldSon->setParent(nullptr);
			children_->insert(dynamic_cast<MultiWayTreeNode<T>*>(son),order);
			son->setParent(this);
			return oldSon;
		}
		catch (std::out_of_range ex) { throw std::out_of_range("Syn neexistuje!"); }
	}

	/**
		Odstrani syna na danom mieste. 
		order - Poradie syna. 
		exception std::out_of_range - Vyhodena, ak neexistuje syn s takym poradim. 
		return - Odstraneny syn vrcholu.
		remarks - Vsetky smerniky zucastnenych vrcholov budu spravne nastavene.
	*/
	template<typename T>
	inline TreeNode<T>* MultiWayTreeNode<T>::removeSon(int order)
	{
		try
		{
			MultiWayTreeNode<T>* pom = (*children_)[order];
			pom->setParent(nullptr);
			children_->removeAt(order);	
			return pom;	
		} 
		catch (std::out_of_range ex) { throw std::out_of_range("Syn neexistuje!"); }
	}

	/**
		Vrati pocet synov vrcholu.
		return - Vzdy K.
	*/
	template<typename T>
	inline int MultiWayTreeNode<T>::degree()
	{
		return children_->size();
	}

	/**
		Konstruktor.
	*/
	template<typename T>
	inline MultiWayTree<T>::MultiWayTree() :
		Tree<T>()
	{
	}

	/**
		Kopirovaci konstruktor.
		other - Viaccestny strom, z ktoreho sa prevezmu vlastnosti.
	*/
	template<typename T>
	inline MultiWayTree<T>::MultiWayTree(const MultiWayTree<T>& other) :
		Tree<T>(other)
	{
	}

	/**
		Operacia klonovania. Vytvori a vrati duplikat udajovej struktury.
		return - Ukazovatel na klon struktury.
	*/
	template<typename T>
	inline Structure * MultiWayTree<T>::clone() const
	{
		return new MultiWayTree<T>(*this);
	}

	/**
		 Vytvori a vrati instanciu vrcholu k-cestneho stromu.
		return - Vytvorena instancia vrcholu k-cestneho stromu. 
	*/
	template<typename T>
	inline TreeNode<T>* MultiWayTree<T>::createTreeNodeInstance()
	{
		T data = T();
		return new MultiWayTreeNode<T>(data);
	}

}

