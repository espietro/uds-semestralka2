#pragma once

#include "../structure.h"
#include "../structure_iterator.h"
#include "../queue/explicit_queue.h"

namespace structures
{
	/// <summary> Vrchol stromu. | interface(pre data stromu) a potomok triedy DataItem </summary>
	/// <typeparam name = "T"> Typ dat ukladanych vo vrchole stromu. </typeparam>
	template <typename T>
	class TreeNode : public DataItem<T>
	{
	public:
		/// <summary> Vytvori plytku kopiu vrchola (teda skopiruje iba data a smerniky na synov). </summary>
		/// <returns> Vystvorena plytka kopia vrcholu. </returns>
		virtual TreeNode<T>* shallowCopy() = 0;

		/// <summary> Vytvori a vrati hlboku kopiu vrchola (teda skopiruje data a skopiruje celu stromovu strukturu). </summary>
		/// <returns> Hlboka kopia vrchola. </returns>
		virtual TreeNode<T>* deepCopy();

		/// <summary> Test, ci je koren. </summary>
		/// <returns> true, ak je koren, false inak. </returns>
		bool isRoot();
    
		/// <summary> Test, ci je list. </summary>
		/// <returns> true, ak je list, false inak. </returns>
		virtual bool isLeaf() = 0;
  
		/// <summary> Spristupni rodica vrcholu. </summary>
		/// <returns> Rodic vrcholu. </returns>
		TreeNode<T>* getParent() const;

		/// <summary> Nastavi rodica na nullptr. </summary>
		void resetParent();
    
		/// <summary> Zmeni rodica vrcholu. </summary>
		/// <param name = "parent"> Novy rodic vrcholu. </param>
		void setParent(TreeNode<T>* parent);

		/// <summary> Spristupni brata vrcholu podla jeho pozicie u spolocneho rodica. </summary>
		/// <param name = "brothersOrder"> Poradie brata u rodica. </param>
		/// <exception cref="std::logic_error"> Vyhodena, ak je korenom. </exception>  
		/// <returns> Brat vrcholu. </returns>
		virtual TreeNode<T>* getBrother(int brothersOrder);
    
		/// <summary> Spristupni syna vrcholu podla jeho pozicie v tomto vrchole. </summary>
		/// <param name = "order"> Poradie syna. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak neexistuje syn s takym poradim. </exception>
		/// <returns> Syn vrcholu. </returns>
		virtual TreeNode<T>* getSon(int order) const = 0;
    
		/// <summary> Vlozi noveho syna vrcholu na dane miesto. </summary>
		/// <param name = "order"> Poradie syna. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak neexistuje syn s takym poradim. </exception>
		/// <remarks> 
		/// Spravanie sa lisi podla toho, ci sa jedna o vrchol s pevnym alebo variabilnym poctom synov. 
		/// Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. 
		/// </remarks>
		virtual void insertSon(TreeNode<T>* son, int order) = 0;
    
		/// <summary> Nahradi syna vrcholu na danom mieste. </summary>
		/// <param name = "order"> Poradie syna. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak je poradie neplatne. </exception>
		/// <returns> Nahradeny syn vrcholu. </returns>
		/// <remarks> Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. </remarks>
		virtual TreeNode<T>* replaceSon(TreeNode<T>* son, int order) = 0;

		/// <summary> Odstrani syna na danom mieste. </summary>
		/// <param name = "order"> Poradie syna. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak neexistuje syn s takym poradim. </exception>
		/// <returns> Odstraneny syn vrcholu. </returns>
		/// <remarks> 
		/// Spravanie sa lisi podla toho, ci sa jedna o vrchol s pevnym alebo variabilnym poctom synov. 
		/// Vsetky smerniky zucastnenych vrcholov budu spravne nastavene. 
		/// </remarks>
		virtual TreeNode<T>* removeSon(int order) = 0;
    
		/// <summary> Vrati stupen vrcholu. </summary>
		/// <returns> Stupen vrcholu. </returns>
		virtual int degree() = 0;   

		/// <summary> Vrati pocet vrcholov v podstrome. </summary>
		/// <returns> Pocet vrcholov v podstrome. </returns>
		virtual size_t sizeOfSubtree();
	protected:
		/// <summary> Konstruktor. </summary>
		/// <param name = "data"> Data, ktore uchovava. </param>
		TreeNode(T data);

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> Vrchol stromu, z ktoreho sa prevezmu vlastnosti. </param>
		TreeNode(const TreeNode<T>& other);
    
		/// <summary> Odkaz na rodica. </summary>
		TreeNode<T>* parent_;  
	};

	/// <summary> Strom. </summary>
	/// <typeparam name = "T"> Typ dat ukladanych v strome. </typeparam>
	template <typename T> 
	class Tree : public Structure, public Iterable<T>
	{
	public:
		/// <summary> Destruktor. </summary>
		~Tree();
  
		/// <summary> Operacia klonovania. Vytvori a vrati duplikat udajovej struktury. </summary>
		/// <returns> Ukazovatel na klon struktury. </returns>
		virtual Structure* clone() const = 0;
    
		/// <summary> Zisti, ci je struktura prazdna. </summary>
		/// <returns> true, ak je struktura prazdna, false inak. </returns>
		bool isEmpty() const override; 

		/// <summary> Vrati pocet vrcholov v strome. </summary>
		/// <returns> Pocet vrcholov v strome. </returns>
		virtual size_t size() const;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Struktura (strom), z ktorej ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tento strom nachadza po priradeni. </returns>
		/// <exception cref="std::logic_error"> Vyhodena, vstupny parameter nie je strom. </exception>  
		/// <remarks> Vyuziva typovy operator priradenia. </remarks>
		Structure& operator=(const Structure& other) override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Strom, z ktoreho ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tento strom nachadza po priradeni. </returns>
		virtual Tree<T>& operator=(const Tree<T>& other);
		
		/// <summary> Vymaze strom. </summary>
		virtual void clear();
    
		/// <summary> Spristupni koren stromu. </summary>
		/// <returns> Koren stromu. </returns>
		TreeNode<T>* getRoot();
    
		/// <summary> Nahradi koren stromu. </summary>
		/// <param name = "newRoot">  Novy koren stromu. </param>
		/// <returns> Povodny koren stromu. </returns>
		virtual TreeNode<T>* replaceRoot(TreeNode<T>* newRoot);

		/// <summary> Vytvori a vrati instanciu vrcholu stromu. </summary>
		/// <returns> Vytvorena instancia vrcholu stromu. </returns>
		/// <remarks> 
		/// Kazdy strom spravuje vrcholy ineho typu, mali by vsak byt jednotne v celom strome. 
		/// Tato tovarenska metoda by mala byt pretazena v kazdom potomkovi stromu tak, aby vracala vrcholy, ktore ten strom ocakava.
		/// Instance vrcholov by mali byt vytvarane vyhradne pomocou tejto metody.
		/// </remarks>
		virtual TreeNode<T>* createTreeNodeInstance() = 0;

		/// <summary> Vrati skutocny iterator na zaciatok struktury </summary>
		/// <returns> Iterator na zaciatok struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<T>* getBeginIterator() const override;

		/// <summary> Vrati skutocny iterator na koniec struktury </summary>
		/// <returns> Iterator na koniec struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<T>* getEndIterator() const override;
	protected:
		/// <summary> Konstruktor. </summary>
		Tree();

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> Stromu, z ktoreho sa prevezmu vlastnosti. </param>
		Tree(const Tree<T>& other);
    
		/// <summary> Koren stromu. </summary>
		TreeNode<T>* root_;

	protected:
		/// <summary> Iterator pre Tree. </summary>
		class TreeIterator : public Iterator<T>
		{
		public:
			/// <summary> Konstruktor. </summary>
			TreeIterator();

			/// <summary> Destruktor. </summary>
			~TreeIterator();

			/// <summary> Operator priradenia. Priradi do seba hodnotu druheho iteratora. </summary>
			/// <param name = "other"> Druhy iterator. </param>
			/// <returns> Vrati seba po priradeni. </returns>
			Iterator<T>& operator= (const Iterator<T>& other) override;

			/// <summary> Porovna sa s druhym iteratorom na nerovnost. </summary>
			/// <param name = "other"> Druhy iterator. </param>
			/// <returns> True, ak sa iteratory nerovnaju, false inak. </returns>
			bool operator!=(const Iterator<T>& other) override;

			/// <summary> Vrati data, na ktore aktualne ukazuje iterator. </summary>
			/// <returns> Data, na ktore aktualne ukazuje iterator. </returns>
			const T operator*() override;

			/// <summary> Posunie iterator na dalsi prvok v strukture. </summary>
			/// <returns> Iterator na dalsi prvok v strukture. </returns>
			/// <remarks> Zvycajne vrati seba. Ak vrati iny iterator, povodny bude automaticky zruseny. </remarks>
			Iterator<T>& operator++() override;
		protected:
			/// <summary> Prehliadka stromu. </summary>
			Queue<TreeNode<T>*>* path_;
		};

	public:
		class PreOrderTreeIterator : public TreeIterator
		{
		public:
			/// <summary> Konstruktor. </summary>
			/// <param name = "startNode"> Vrchol podstromu, cez ktory ma iterovat. </param>
			PreOrderTreeIterator(TreeNode<T>* const startNode);
		private:
			/// <summary> Zostavi do path_ prehliadku stromu v poradi preorder. </summary>
			/// <param name = "current"> Aktualny spracovavany vrchol stromu. </param>
			void populatePath(TreeNode<T>* const current);
		};

		class PostOrderTreeIterator: public TreeIterator
		{
		public:
			/// <summary> Konstruktor. </summary>
			/// <param name = "startNode"> Vrchol podstromu, cez ktory ma iterovat. </param>
			PostOrderTreeIterator(TreeNode<T>* const startNode);
		private:
			/// <summary> Zostavi do path_ prehliadku stromu v poradi postorder. </summary>
			/// <param name = "path"> Cesta stromom, ktoru zostavuje. </param>
			/// <param name = "current"> Aktualny spracovavany vrchol stromu. </param>
			void populatePath(TreeNode<T>* const current);
		};

		class LevelOrderTreeIterator : public TreeIterator
		{
		public:
			/// <summary> Konstruktor. </summary>
			/// <param name = "startNode"> Vrchol podstromu, cez ktory ma iterovat. </param>
			LevelOrderTreeIterator(TreeNode<T>* const startNode);
		private:
			/// <summary> Zostavi do path_ prehliadku stromu v poradi levelorder. </summary>
			/// <param name = "current"> Aktualny spracovavany vrchol stromu. </param>
			void populatePath(TreeNode<T>* const current);
		};
	};

	/**
		Vytvori a vrati hlboku kopiu vrchola (teda skopiruje data a skopiruje celu stromovu strukturu). 
		return - Hlboka kopia vrchola.
	*/
	template<typename T>
	inline TreeNode<T>* TreeNode<T>::deepCopy() //predpokladame ze shalowCopy mame uz implementovanu
	{
		TreeNode<T>* pom = shallowCopy();
		for (int i = 0; i < degree(); i++)
		{
			TreeNode<T> *son = getSon(i);
			if (son != nullptr)
			{
				pom->replaceSon(son->deepCopy(), i);
			}
		}
		return pom;
	}

	/**
		Test, ci je koren. 
		return - true, ak je koren, false inak.
	*/
	template<typename T>
	inline bool TreeNode<T>::isRoot()
	{
		return parent_ == nullptr;
	}

	/**
		Spristupni rodica vrcholu. 
		return - Rodic vrcholu(pointer).
	*/
	template<typename T>
	inline TreeNode<T>* TreeNode<T>::getParent() const
	{
		return parent_;
	}

	/**
		Nastavi rodica na nullptr.
	*/
	template<typename T>
	inline void TreeNode<T>::resetParent()
	{
		parent_ = nullptr;
	}

	/**
		Zmeni rodica vrcholu. 
		parent - Novy rodic vrcholu(Ukazovatel nan).
	*/
	template<typename T>
	inline void TreeNode<T>::setParent(TreeNode<T>* parent)
	{		
		parent_ = parent;
	}

	/**
		Spristupni brata vrcholu podla jeho pozicie u spolocneho rodica. 
		brothersOrder - Poradie brata u rodica. 
		exception std::logic_error - Vyhodena, ak je korenom. 
		return - Brat vrcholu.
	*/
	template<typename T>
	inline TreeNode<T>* TreeNode<T>::getBrother(int brothersOrder)
	{
		if (parent_ != nullptr)
		{
			return parent_->getSon(brothersOrder);
		}
		else
		{
			throw std::logic_error("Vrchol je korenom.");
		}
	}

	/**
		Vrati pocet vrcholov v podstrome. 
		return - Pocet vrcholov v podstrome. (pocet vsetkych vrcholov stromu, ktoreho korenom je aktualny vrchol, vratane aktualneho vrcholu)
	*/
	template<typename T>
	inline size_t TreeNode<T>::sizeOfSubtree()	//vrati pocet vsetkych vrcholov stromu, ktoreho korenom je aktualny vrchol, vratane aktualneho vrcholu
	{
		size_t count = 1; //zacinam od seba
		for (int i = 0; i < degree(); i++)
		{
			TreeNode<T>* son = getSon(i);
			if (son != nullptr)
			{
				count += son->sizeOfSubtree();
			}
		}
		return count;
	}

	/**
		Konstruktor. 
		data - Data, ktore uchovava.
	*/
	template<typename T>
	inline TreeNode<T>::TreeNode(T data):
		DataItem<T>(data),
		parent_(nullptr)
	{
	}

	/**
		Kopirovaci konstruktor. 
		other - Vrchol stromu, z ktoreho sa prevezmu vlastnosti.
	*/
	template<typename T>
	inline TreeNode<T>::TreeNode(const TreeNode<T>& other):
		DataItem<T>(other),
		parent_(other.parent_)
	{
	}
  
	/**
		Destruktor.
	*/
	template<typename T>
	inline Tree<T>::~Tree() //ked rusim vrchol tak sa automaticky zrusi cely podstrom
	{
		clear();
	}

	/**
		Zisti, ci je struktura prazdna. 
		return - true, ak je struktura prazdna, false inak.
	*/
	template<typename T>
	inline bool Tree<T>::isEmpty() const	
	{
		return root_ == nullptr;
	}

	/**
		Vrati pocet vrcholov v strome. 
		return - Pocet vrcholov v strome.
	*/
	template<typename T>
	inline size_t Tree<T>::size() const
	{
		return root_ != nullptr ? root_->sizeOfSubtree() : 0;
	}

	/**
		Vrati skutocny iterator na zaciatok struktury 
		return - Iterator na zaciatok struktury. 
		remarks - Zabezpecuje polymorfizmus.
	*/
	template<typename T>
	inline Iterator<T>* Tree<T>::getBeginIterator() const
	{
		return new PreOrderTreeIterator(root_);
	}

	/**
		Vrati skutocny iterator na koniec struktury 
		return - Iterator na koniec struktury. 
		remarks - Zabezpecuje polymorfizmus.
	*/
	template<typename T>
	inline Iterator<T>* Tree<T>::getEndIterator() const
	{
		return new PreOrderTreeIterator(nullptr);
	}

	/**
		Konstruktor.(protected)
	*/
	template<typename T>
	inline Tree<T>::Tree():
		Structure(),
		Iterable<T>(),
		root_(nullptr)    
	{
	}

	/**
		Kopirovaci konstruktor. (protected)
		other - Stromu, z ktoreho sa prevezmu vlastnosti.
	*/
	template<typename T>
	inline Tree<T>::Tree(const Tree<T>& other):
		Tree<T>()
	{
		*this = other;
	}

	/**
		Operator priradenia. 
		other - Struktura (strom), z ktorej ma prebrat vlastnosti. 
		return - Adresa, na ktorej sa tento strom nachadza po priradeni. 
		exception std::logic_error - Vyhodena, vstupny parameter nie je strom. 
		remarks - Vyuziva typovy operator priradenia.
	*/
	template<typename T>
	inline Structure & Tree<T>::operator=(const Structure & other)
	{
		if (this != &other)
		{
			*this = dynamic_cast<const Tree<T>&>(other);
		}
		return *this;
	}

	/**
		Operator priradenia. 
		other - Strom, z ktoreho ma prebrat vlastnosti. 
		return - Adresa, na ktorej sa tento strom nachadza po priradeni.
	*/
	template<typename T>
	inline Tree<T>& Tree<T>::operator=(const Tree<T>& other)
	{
		if (this != &other)
		{
			clear();
			if (other.root_ != nullptr)	//nemozem priradzovat ak ten druhy je null
			{
				root_ = other.root_->deepCopy();
			}			
		}
		return *this;
	}

	/**
		Vymaze strom.
	*/
	template<typename T>
	inline void Tree<T>::clear()
	{
		delete root_;
		root_ = nullptr;
	}

	/**
		Spristupni koren stromu. 
		return - Koren stromu.
	*/
	template<typename T>
	inline TreeNode<T>* Tree<T>::getRoot()
	{
		return root_;
	}

	/**
		Nahradi koren stromu. 
		newRoot -  Novy koren stromu. 
		return - Povodny koren stromu.
	*/
	template<typename T>             
	inline TreeNode<T>* Tree<T>::replaceRoot(TreeNode<T>* newRoot)
	{
		TreeNode<T>* old = root_;
		if (newRoot != nullptr)
		{
			newRoot->setParent(nullptr);
		}		
		root_ = newRoot;		
		return old;
	}

	/**
		Konstruktor.
	*/
	template<typename T>
	inline Tree<T>::TreeIterator::TreeIterator():
		Iterator<T>(),
		path_(new ExplicitQueue<TreeNode<T>*>())	//nemozem tam dat len Queue lebo to je abstraktna trieda
	{
	}

	/**
		Destruktor.
	*/
	template<typename T>
	inline Tree<T>::TreeIterator::~TreeIterator()
	{
		delete path_;
		path_ = nullptr;
	}

	/**
		Operator priradenia. Priradi do seba hodnotu druheho iteratora. 
		other - Druhy iterator. 
		return - Vrati seba po priradeni.
	*/
	template<typename T>
	inline Iterator<T>& Tree<T>::TreeIterator::operator=(const Iterator<T>& other)
	{
		*path_ = *dynamic_cast<const TreeIterator&>(other).path_;
		return *this;
	}

	/**
		Porovna sa s druhym iteratorom na nerovnost. 
		other - Druhy iterator. 
		return - True, ak sa iteratory nerovnaju, false inak.
	*/
	template<typename T>
	inline bool Tree<T>::TreeIterator::operator!=(const Iterator<T>& other)
	{
		if (!path_->isEmpty() && !dynamic_cast<const TreeIterator&>(other).path_->isEmpty())
		{
			return path_->peek() != dynamic_cast<const TreeIterator&>(other).path_->peek();
		}		
		else 
		{
			return !path_->isEmpty() || !dynamic_cast<const TreeIterator&>(other).path_->isEmpty();	
		}
	}

	/**
		Vrati data, na ktore aktualne ukazuje iterator. 
		return - Data, na ktore aktualne ukazuje iterator.
	*/
	template<typename T>
	inline const T Tree<T>::TreeIterator::operator*()
	{
		return path_->peek()->accessData();	//pretoze v strome ukladame smerniky na objekty treeNode
	}

	/**
		Posunie iterator na dalsi prvok v strukture. 
		return - Iterator na dalsi prvok v strukture. 
		remarks - Zvycajne vrati seba. Ak vrati iny iterator, povodny bude automaticky zruseny.
	*/
	template<typename T>
	inline Iterator<T>& Tree<T>::TreeIterator::operator++()
	{
		path_->pop();
		return *this;
	}

	//konstruktory pre 3 iteratory =============================

	/**
		Konstruktor. 
		startNode - Vrchol podstromu, cez ktory ma iterovat.
		//pouziva sa pri deepCopy
	*/
	template<typename T>
	inline Tree<T>::PreOrderTreeIterator::PreOrderTreeIterator(TreeNode<T>* const startNode):
		TreeIterator()
	{
		populatePath(startNode);
	}

	/**
		Zostavi do path_ prehliadku stromu v poradi preorder. 
		current - Aktualny spracovavany vrchol stromu.
	*/
	template<typename T>
	inline void Tree<T>::PreOrderTreeIterator::populatePath(TreeNode<T>* const current)
	{
		if (current != nullptr)
		{
			path_->push(current);	//najskor zaradim do frontu seba az potom svojich synov
			for (int i = 0; i < current->degree(); i++)
			{
				TreeNode<T>* son = current->getSon(i);
				if (son != nullptr)
				{
					populatePath(son);
				}
			}
		}
	}

	/**
		Konstruktor. 
		startNode - Vrchol podstromu, cez ktory ma iterovat.
	*/
	template<typename T>
	inline Tree<T>::PostOrderTreeIterator::PostOrderTreeIterator(TreeNode<T>* const startNode) :
		TreeIterator()
	{
		populatePath(startNode);
	}

	/**
		Zostavi do path_ prehliadku stromu v poradi postorder. 
		path - Cesta stromom, ktoru zostavuje. 
		current - Aktualny spracovavany vrchol stromu.
	*/
	template<typename T>
	inline void Tree<T>::PostOrderTreeIterator::populatePath(TreeNode<T>* const current)
	{
		if (current != nullptr)
		{
			for (int i = 0; i < current->degree(); i++)
			{
				TreeNode<T>* son = current->getSon(i);
				if (son != nullptr)
				{
					populatePath(son);
				}
			}
			path_->push(current);
		}
	}

	/**
		Konstruktor. 
		startNode - Vrchol podstromu, cez ktory ma iterovat.
	*/
	template<typename T>	//jeden front a druhu pomocny -> v prednaskach
	inline Tree<T>::LevelOrderTreeIterator::LevelOrderTreeIterator(TreeNode<T>* const startNode) :
		TreeIterator()
	{
		populatePath(startNode);
	}

	/**
		Zostavi do path_ prehliadku stromu v poradi levelorder. 
		current - Aktualny spracovavany vrchol stromu.
	*/
	template<typename T>
	inline void Tree<T>::LevelOrderTreeIterator::populatePath(TreeNode<T>* const current)
	{
		if (current != nullptr)
		{
			Queue<TreeNode<T>*> pom;
			pom.push(current);
			while (!pom.isEmpty())
			{
				TreeNode<T>* aktualny = pom.pop();
				path_->push(aktualny);
				for (int i = 0; i < aktualny->degree(); i++)
				{
					TreeNode<T>* son = aktualny->getSon(i);
					if (son != nullptr)
					{
						pom.push(son);
					}
				}
			}
		}
	}

}

