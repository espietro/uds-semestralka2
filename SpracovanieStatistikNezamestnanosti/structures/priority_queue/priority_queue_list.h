#pragma once

#include "priority_queue.h"
#include "../list/list.h"
#include <stdexcept>

namespace structures
{
	/// <summary> Spolocny predok pre vsetky prioritne fronty, ktorych implementujucim typom je zoznam. | Atributy - *list_</summary>
	/// <typeparam name = "T"> Typ dat ukladanych v prioritnom fronte. </typeparam>
	template<typename T>
	class PriorityQueueList : public PriorityQueue<T>
	{
	public:
		/// <summary> Destruktor. </summary>
		~PriorityQueueList();

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Prioritny front, z ktoreho ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tento prioritny front nachadza po priradeni. </returns>
		PriorityQueue<T>& operator=(const PriorityQueue<T>& other) override;

		/// <summary> Operator priradenia pre prioritny front implementovany zoznamom. </summary>
		/// <param name = "other"> Prioritny front implementovany zoznamom, z ktoreho ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tento prioritny front nachadza po priradeni. </returns>
		virtual PriorityQueueList<T>& operator=(const PriorityQueueList<T>& other);

		/// <summary> Vrati pocet prvkov v prioritnom fronte implementovanom zoznamom. </summary>
		/// <returns> Pocet prvkov v prioritnom fronte implementovanom zoznamom. </returns>
		size_t size() const override;

		/// <summary> Vymaze obsah prioritneho frontu implementovaneho zoznamom. </summary>
		void clear() override;

		/// <summary> Odstrani prvok s najvacsou prioritou z prioritneho frontu implementovaneho zoznamom. </summary>
		/// <returns> Odstraneny prvok. </returns>
		/// <exception cref="std::logic_error"> Vyhodena, ak je prioritny front implementovany zoznamom prazdny. </exception>  
		T pop() override;

		/// <summary> Vrati adresou prvok s najvacsou prioritou. </summary>
		/// <returns> Adresa, na ktorej sa nachadza prvok s najvacsou prioritou. </returns>
		/// <exception cref="std::logic_error"> Vyhodena, ak je prioritny front implementovany zoznamom prazdny. </exception>  
		T& peek() override;

		/// <summary> Vrati kopiu prvku s najvacsou prioritou. </summary>
		/// <returns> Kopia prvku s najvacsou prioritou. </returns>
		/// <exception cref="std::logic_error"> Vyhodena, ak je prioritny front implementovany zoznamom prazdny. </exception>  
		const T peek() const override;

		/// <summary> Vrati prioritu prvku s najvacsou prioritou. </summary>
		/// <returns> Priorita prvku s najvacsou prioritou. </returns>
		/// <exception cref="std::logic_error"> Vyhodena, ak je prioritny front implementovany zoznamom prazdny. </exception>  
		int peekPriority() const override;

	protected:
		/// <summary> Konstruktor. </summary>
		/// <param name = "list"> Zoznam, do ktoreho sa budu ukladat prvky prioritneho frontu. </param>
		/// <remarks> 
		/// Potomkovia ako list vlozia instanciu implicitne alebo explicitne implementovaneho zoznamu. 
		/// Prioritny front implementovany zoznamom dealokuje tento objekt.
		/// </remarks>
		PriorityQueueList(List<PriorityQueueItem<T>*>* list);

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> Prioritny front implementovany zoznamom, z ktoreho sa prevezmu vlastnosti. </param>
		PriorityQueueList(const PriorityQueueList<T>& other);

		/// <summary> Vrati index v zozname, na ktorom sa nachadza prvok s najvacsou prioritou. </summary>
		/// <returns> Index prvku s najvacsou prioritou. </returns>
		/// <exception cref="std::logic_error"> Vyhodena, ak je zoznam prazdny. </exception>  
		virtual int indexOfPeek() const;

	protected:
		/// <summary> Smernik na zoznam, do ktoreho sa ukladaju prvky prioritneho frontu. </summary>
		List<PriorityQueueItem<T>*>* list_;	//este neviem kam budeme vkladat preto neimplementujeme operaciu push
	};										//operacia pop, pouzivame removeAt a indexOfPeek  // potomkovia potom len musia prekryt indexOfPeek a push

	/**
		Konstruktor. 
		list - Zoznam, do ktoreho sa budu ukladat prvky prioritneho frontu. 
		remarks - Potomkovia ako list vlozia instanciu implicitne alebo explicitne implementovaneho zoznamu. 
					Prioritny front implementovany zoznamom dealokuje tento objekt.
	*/
	template<typename T>
	inline PriorityQueueList<T>::PriorityQueueList(List<PriorityQueueItem<T>*>* list) :	//je protected
		PriorityQueue<T>(),
		list_(list)
	{
	}

	/**
		Kopirovaci konstruktor. 
		other - Prioritny front implementovany zoznamom, z ktoreho sa prevezmu vlastnosti. 
	*/
	template<typename T>
	inline PriorityQueueList<T>::PriorityQueueList(const PriorityQueueList<T>& other) :		//je tiez protected, nieco k tomu je v prednaskach
		PriorityQueueList<T>(dynamic_cast<List<PriorityQueueItem<T>*>*>(other.list_->clone()))
	{
		list_->clear();
		*this = other;
	}

	/**
		Destruktor.
	*/
	template<typename T>
	inline PriorityQueueList<T>::~PriorityQueueList()
	{
		list_->clear();
		delete list_;
		list_ = nullptr;

	}

	/**
		Operator priradenia. 
		other - Prioritny front, z ktoreho ma prebrat vlastnosti. 
		return - Adresa, na ktorej sa tento prioritny front nachadza po priradeni.
	*/
	template<typename T>
	inline PriorityQueue<T>& PriorityQueueList<T>::operator=(const PriorityQueue<T>& other)
	{
		if (this != &other)
		{
			*this = dynamic_cast<const PriorityQueueList<T>&>(other);
		}
		return *this;
	}

	/**
		Operator priradenia pre prioritny front implementovany zoznamom. 
		other - Prioritny front implementovany zoznamom, z ktoreho ma prebrat vlastnosti. 
		return - Adresa, na ktorej sa tento prioritny front nachadza po priradeni.
	*/
	template<typename T>
	inline PriorityQueueList<T>& PriorityQueueList<T>::operator=(const PriorityQueueList<T>& other)
	{
		if (this != &other)
		{
			clear();	//vypraznime vsetky moje prvky ..teda musime ich aj dealokovat
			for each (PriorityQueueItem<T>* otherItem in *other.list_)
			{
				list_->add(new PriorityQueueItem<T>(*otherItem));
			}
		}
		return *this;
		//*list_ = *other.list_; toto nemozem spravit lebo by sme priradili len smerniky a obe polia by ukazovali na rovnake data
	}

	/**
		 Vrati pocet prvkov v prioritnom fronte implementovanom zoznamom. 
		return - Pocet prvkov v prioritnom fronte implementovanom zoznamom. 
	*/
	template<typename T>
	inline size_t PriorityQueueList<T>::size() const
	{
		return list_->size();
	}

	/**
		 Vymaze obsah prioritneho frontu implementovaneho zoznamom. 
	*/
	template<typename T>
	inline void PriorityQueueList<T>::clear()
	{
		for each (PriorityQueueItem<T>* item in *list_)
		{
			delete item;	//pretoze mame pole smernikov na PriorityQueueItem
		}
		list_->clear();
	}

	/**
		Vrati index v zozname, na ktorom sa nachadza prvok s najvacsou prioritou. 
		returns - Index prvku s najvacsou prioritou. 
		exception std::logic_error - Vyhodena, ak je zoznam prazdny. 
	*/
	template<typename T>
	inline int PriorityQueueList<T>::indexOfPeek() const
	{
		if (list_->size() <= 0)
		{
			throw std::logic_error("Prioritny front je prazdny");
		}
		int min = (*list_)[0]->getPriority();	//hladame minimum z priorit
		int minIndex = 0;
		int i = 0;
		for each (PriorityQueueItem<T>* item in *list_)
		{
			if (item->getPriority() < min)
			{
				min = item->getPriority();
				minIndex = i;
			}
			i++;
		}
		return minIndex;
	}

	/**
		Odstrani prvok s najvacsou prioritou z prioritneho frontu implementovaneho zoznamom. 
		return - Odstraneny prvok. 
		exception std::logic_error - Vyhodena, ak je prioritny front implementovany zoznamom prazdny.
	*/
	template<typename T>
	inline T PriorityQueueList<T>::pop()
	{
		PriorityQueueItem<T>* pom = list_->removeAt(indexOfPeek());	//vrati smernik 
		T data = pom->accessData();	//ak by som zavolal destruktor data by som stratil a ak by som ho nezavolal a vratil kopiu dat -> memory leak
		delete pom;
		return data;
	}

	/**
		Vrati adresou prvok s najvacsou prioritou. 
		return - Adresa, na ktorej sa nachadza prvok s najvacsou prioritou. 
		exception std::logic_error - Vyhodena, ak je prioritny front implementovany zoznamom prazdny.
	*/
	template<typename T>
	inline T & PriorityQueueList<T>::peek()
	{
		return (*list_)[indexOfPeek()]->accessData();
	}

	/**
		Vrati kopiu prvku s najvacsou prioritou. 
		return - Kopia prvku s najvacsou prioritou. 
		exception std::logic_error - Vyhodena, ak je prioritny front implementovany zoznamom prazdny.
	*/
	template<typename T>
	inline const T PriorityQueueList<T>::peek() const
	{	
		return (*list_)[indexOfPeek()]->accessData();
	}

	/**
		Vrati prioritu prvku s najvacsou prioritou. 
		return - Priorita prvku s najvacsou prioritou. 
		exception std::logic_error - Vyhodena, ak je prioritny front implementovany zoznamom prazdny.
	*/
	template<typename T>
	inline int PriorityQueueList<T>::peekPriority() const
	{
		return (*list_)[indexOfPeek()]->getPriority();
	}
}