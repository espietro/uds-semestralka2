#include "memory_routines.h"

namespace structures {
	//nastavit byte na same 1ky
	byte & MemoryRoutines::byteSet(byte & B)	//TODO 01: MemoryRoutines
	{
		B = 255;
		return B;
	}
	//vynulovat cely byte 
	byte & MemoryRoutines::byteReset(byte & B)	//TODO 01: MemoryRoutines
	{
		B = 0;
		return B;
	}
	byte & MemoryRoutines::byteXOR(byte & B)	//TODO 01: MemoryRoutines
	{
		B = ~B;
		return B;
	}
	byte & MemoryRoutines::byteSHL(byte & B)	//TODO 01: MemoryRoutines
	{
		B = B << 1;
		return B;
	}
	byte & MemoryRoutines::byteSHR(byte & B)	//TODO 01: MemoryRoutines
	{
		B = B >> 1;
		return B;
	}
	//bity zacinaju z prava od 0-teho indexu
	bool MemoryRoutines::byteNthBitGet(byte & B, int n)	//TODO 01: MemoryRoutines
	{
		byte temp = B;
		temp = temp >> n;
		temp = temp & 1;
		if (temp == 1)
		{
			return true;
		}
		return false;
	}
	
	byte & MemoryRoutines::byteNthBitTo0(byte & B, int n)	//TODO 01: MemoryRoutines
	{
		byte temp = 1;
		temp = ~(temp << n);
		B = B & temp;
		return B;
	}
	
	byte & MemoryRoutines::byteNthBitTo1(byte & B, int n)	//TODO 01: MemoryRoutines
	{
		byte temp = 1;
		temp = temp << n;
		B = B | temp;
		return B;
	}

	byte & MemoryRoutines::byteNthBitToggle(byte & B, int n)
	{
		if (byteNthBitGet(B,n))
		{
			byteNthBitTo0(B, n);
		}
		else
		{
			byteNthBitTo1(B, n);
		}
		return B;
	}

	MemoryRoutines::MemoryRoutines()
	{
	}

}
