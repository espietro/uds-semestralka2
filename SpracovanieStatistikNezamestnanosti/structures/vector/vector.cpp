#include "vector.h"
#include "../ds_routines.h"
#include <cstdlib>
#include <cstring>

namespace structures {
	
	/** 
		Konstruktor vytvori vektor o velkosti size bytov.
		size - Pocet bytov vo vytvorenom vektore.
	*/
	Vector::Vector(size_t size) ://size - pocet prvkov ktore moze Vector ponat
		memory_(calloc(size, 1)),//alokujeme pocet bytov pamate, pocet=size
		size_(size)				//pocet bytov vo vektore
	{
	}

	/**
		Kopirovaci konstruktor.
		other - Zdrojovy vektor.
	*/
	Vector::Vector(const Vector& other) : 
		Vector(other.size_)//defaultne sa alokuje pamat
	{
		memcpy(memory_, other.memory_, size_);
	}

	/**
		Destruktor.
	*/
	Vector::~Vector()	//TODO 01: Vector
	{
		free(memory_);
		memory_ = nullptr;
		size_ = 0;
		
	}

	/**
		Operacia klonovania. Vytvori a vrati duplikat vektora. </summary>
		return - Ukazovatel na vytvoreny klon vektora.
	*/
	Structure* Vector::clone() const
	{
		return new Vector(*this); //volam copy konstruktor ??preco je tam new- lebo navratova hodnota metody je pointer na nieco
	}

	/**
		Vrati pocet bytov vo vektore. 
		return - Pocet bytov vo vektore.
	*/
	size_t Vector::size() const
	{
		return size_;
	}

	/**
		Operator priradenia.
		other - Struktura (vektor), z ktorej ma prebrat vlastnosti.
		return - Adresa, na ktorej sa tento vektor nachadza po priradeni.
		exception std::logic_error - Vyhodena, ak vstupny parameter nie je Vector.
		remarks - Vyuziva typovy operator priradenia.
	*/
	Structure & Vector::operator=(const Structure & other)
	{
		if (this != &other)
		{
			*this = dynamic_cast<const Vector&>(other);
		}
		return *this;
	}

	/**
		Operator priradenia. 
		other - Vektor, z ktoreho sa maju prebrat vlastnosti. 
		return - Adresa, na ktorej sa tento vektor nachadza po priradeni.
	*/
	Vector& Vector::operator=(const Vector& other)	//operator priradenia ked do vektoru V vkladame iny vektor other //TODO 01: Vector
	{
		if (this != &other)	//podari sa priradit iba ak nepriradzujeme vektor sam do seba
		{
			size_ = other.size_;
			memory_ = realloc(memory_, size_);
			memcpy(memory_, other.memory_, size_);
		}
		return *this;//kedze vraciame adresu, musime ptr this dereferencovat
	}

	/**
		Operator porovnania.
		other - Vektor, s ktorym sa ma porovnat.
		return - true, ak su vektory rovnake, false inak.
	*/
	bool Vector::operator==(const Vector& other) const		//TODO 01: Vector
	{
		return size_ == other.size_ && memcmp(memory_, other.memory_, size_);
	}

	/**
		Vrati adresou byte na indexe.
		index - Index byte-u.
		return - Adresa byte-u na danom indexe.
		exception std::out_of_range - Vyhodena, ak index nepatri do vektoru.
	*/
	byte& Vector::operator[](const int index)	//TODO 01: Vector
	{
		DSRoutines::rangeCheckExcept(index, size_, "Invalid index."); //overime ci index nie je momo hranic
		return *(reinterpret_cast<byte*>(memory_) + index);
		/*spristupnenie prvku, Vrati adresou byte na indexe.
		??preco len byte, co ak tam budeme skladovat double 
		->pretoze Vector pracuje len s Bytami ak potrebujeme vacsiu jednotku pouzijeme pole, 
		ktore pouziva ako kompoziciu Vector, resp. pole je postavene na Vectore*/
	}

	/**
		Vrati hodnotou byte na indexe. 
		index - Index byte-u. 
		returns - Hodnota byte-u na danom indexe. 
		exception std::out_of_range - Vyhodena, ak index nepatri do vektoru.
	*/
	byte Vector::operator[](const int index) const		//TODO 01: Vector
	{
		DSRoutines::rangeCheckExcept(index, size_, "Invalid index.");
		return *(reinterpret_cast<byte*>(memory_) + index);
	}

	/**
		Precita count bytov od daneho indexu do cielovej adresy dest.
		index - Index byte-u vo vektore, od ktoreho sa zacne citat. 
		count - Pocet bytov, ktore budu skopirovane z vektora od pozicie index do pamate dest. 
		dest - Pamat, do ktorej sa bude zapisovat. 
		return - dest. 
	*/
	byte& Vector::readBytes(const int index, const int count, byte& dest)	//TODO 01: Vector
	{
		DSRoutines::rangeCheckExcept(index, size_, "Invalid index.");
		DSRoutines::rangeCheckExcept(index + count, size_ + 1, "Invalid index.");
		memcpy(&dest, getBytePointer(index), count);
		return dest;
		//podmienky
		//start < size
		//start + length <= size + 1
		//throw std::exception("Vector::readBytes: Not implemented yet.");
	}

	/**
		Skopiruje length bytov z vektora src od pozicie srcStartIndex do vektora dest od pozicie destStartIndex.
		src - Zdrojovy vektor.
		srcStartIndex - Index byte-u vo vektore src, od ktoreho sa zacne citat.
		dest - Cielovy vektor. 
		destStartIndex - Index byte-u vo vektore dest, od ktoreho sa zacne zapisovat. 
		length - Pocet bytov, ktore budu skopirovane z vektora src od pozicie srcStartIndex do vektora dest od pozicie destStartIndex. 
		exception std::out_of_range - Vyhodena, ak zaciatocne alebo koncove indexy nepatria do prislusnych vektorov.
	*/
	void Vector::copy(const Vector& src, const int srcStartIndex, Vector& dest, const int destStartIndex, const int length)	//TODO 01: Vector
	{
		DSRoutines::rangeCheckExcept(srcStartIndex, src.size_, "Invalid index.");
		DSRoutines::rangeCheckExcept(destStartIndex, dest.size_, "Invalid index.");
		DSRoutines::rangeCheckExcept(srcStartIndex + length, src.size_ + 1, "Invalid index.");
		DSRoutines::rangeCheckExcept(srcStartIndex + length, dest.size_ + 1, "Invalid index.");
		if (src.memory_ == dest.memory_)
		{
			memmove(dest.getBytePointer(destStartIndex), src.getBytePointer(srcStartIndex), length);
		}
		else
		{
			memcpy(dest.getBytePointer(destStartIndex), src.getBytePointer(srcStartIndex), length);
		}
		//podmienky
		//start < size
		//start + length <= size + 1
		//throw std::exception("Vector::copy: Not implemented yet.");
	}

	/**
		Vrati ukazovatel na byte na danom indexe.
		index - Index byte-u.
		return - Ukazovatel na byte na danom indexe.
		exception std::out_of_range - Vyhodena, ak index nepatri do vektoru.
	*/
	byte* Vector::getBytePointer(const int index) const		//TODO 01: Vector
	{
		DSRoutines::rangeCheckExcept(index, size_, "Invalid index");
		return reinterpret_cast<byte*>(memory_) + index;
	}

}