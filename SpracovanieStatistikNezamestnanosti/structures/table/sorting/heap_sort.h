#pragma once

#include "sort.h"
#include "../unsorted_sequence_table.h"

namespace structures
{

	/// <summary> Triedenie Heap sort. </summary>
	/// <typeparam name = "K"> Kluc prvkov v tabulke. </typeparam>
	/// <typeparam name = "T"> Typ dat ukladanych v tabulke. </typeparam>
	template <typename K, typename T>
	class HeapSort : public Sort<K, T>
	{
	public:
		/// <summary> Utriedi tabulku triedenim Heap sort. </summary>
		/// <param name = "table"> NonortedSequenceTable, ktoru ma utriedit. </param>
		void sort(UnsortedSequenceTable<K, T>& table) override;
	};

	template<typename K, typename T>
	inline void HeapSort<K, T>::sort(UnsortedSequenceTable<K, T>& table)
	{
		for (size_t i = 1; i < table.size(); i++)	//ideme od 1. prvku pretoze halda na 0. indexe nema prvok
		{
			int cur = i;
			bool swap;
			do
			{
				swap = false;
				int dad = (cur - 1) / 2;
				if (cur > 0 && table.getItemAtIndex(cur).getKey() > table.getItemAtIndex(dad).getKey())
				{					
					table.swap(cur, dad);
					/*cout << "cur:" << table.getItemAtIndex(cur).getKey() << " dad:" << table.getItemAtIndex(dad).getKey() << endl;*/
					cur = dad;
					swap = true;
				}
			} while (swap);
			//notify();
		}

		for (int i = table.size()-1; i > 0; i--)
		{
			table.swap(0, i);
			int cur = 0;
			bool swap = false;
			int left, right, max;
			do
			{
				swap = false;
				left = cur * 2 + 1;
				right = cur * 2 + 2;

				if (left < i && right < i)
				{
					max = table.getItemAtIndex(left).getKey() > table.getItemAtIndex(right).getKey() ? left : right;
				}
				else
				{
					max = left < i ? left : right;
				}
				if (max < i && table.getItemAtIndex(max).getKey() > table.getItemAtIndex(cur).getKey())
				{
					table.swap(max, cur);
					//notify();
					cur = max;
					swap = true;
				}
			} while (swap);
			//notify();
		}
	}

}