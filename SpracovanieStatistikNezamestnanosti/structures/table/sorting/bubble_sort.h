#pragma once

#include "sort.h"
#include "../unsorted_sequence_table.h"

namespace structures
{

	/// <summary> Triedenie Bubble sort. </summary>
	/// <typeparam name = "K"> Kluc prvkov v tabulke. </typeparam>
	/// <typeparam name = "T"> Typ dat ukladanych v tabulke. </typeparam>
	template <typename K, typename T>
	class BubbleSort : public Sort<K, T>
	{
	public:
		/// <summary> Utriedi tabulku triedenim Bubble sort. </summary>
		/// <param name = "table"> NonortedSequenceTable, ktoru ma utriedit. </param>
		void sort(UnsortedSequenceTable<K, T>& table) override;
	};

	template<typename K, typename T>
	inline void BubbleSort<K, T>::sort(UnsortedSequenceTable<K, T>& table)
	{
		bool swap = false;
		int n = table.size() - 1;
		do {
			swap = false;
			for (int i = 0; i < n; i++)
			{
				if (table.getItemAtIndex(i).getKey() > table.getItemAtIndex(i + 1).getKey()) //vzdy triedim na zaklade kluca //teraz to utriedi od najmensieho po najvacsie
				{
					table.swap(i, i + 1);
					swap = true;
					//notify();
				}
			}
			notify();	//pozrie ci ma niekto sleduje a ak ani tak mu to oznami		
			n--;//vlastne utriezdujem uz len neutriedenu cast tabulky
		} while (swap);
		
		
	}

}