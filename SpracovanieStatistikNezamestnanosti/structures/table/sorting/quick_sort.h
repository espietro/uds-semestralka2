#pragma once

#include "sort.h"
#include "../unsorted_sequence_table.h"

namespace structures
{

	/// <summary> Triedenie Quick sort. </summary>
	/// <typeparam name = "K"> Kluc prvkov v tabulke. </typepram>
	/// <typeparam name = "T"> Typ dat ukladanych v tabulke. </typepram>
	template <typename K, typename T>
	class QuickSort : public Sort<K, T>
	{
	public:
		/// <summary> Utriedi tabulku triedenim Quick sort. </summary>
		/// <param name = "table"> NonortedSequenceTable, ktoru ma utriedit. </param>
		void sort(UnsortedSequenceTable<K, T>& table) override;
	private:
		void quick(UnsortedSequenceTable<K, T>& table, const int beginIndex, const int endIndex);
	};

	template<typename K, typename T>
	inline void QuickSort<K, T>::sort(UnsortedSequenceTable<K, T>& table)
	{
		if (table.size() > 1)	//podmienka je tu preto ze keby sme od size_t 0 odcitali 1ku tak by sme dostali maximalne cislo(podtecenie)
		{
			quick(table, 0, table.size() - 1);
		}
	}

	template<typename K, typename T>
	inline void QuickSort<K, T>::quick(UnsortedSequenceTable<K, T>& table, const int beginIndex, const int endIndex)
	{
		K pivot = table.getItemAtIndex(beginIndex + (endIndex - beginIndex) / 2).getKey();	//vzorec aby nedoslo k preteceniu pri velkych cislach ak by sme ich nemali ako scitat
		int leftIndex = beginIndex;
		int rightIndex = endIndex;
		do
		{
			//z lavej strany hladame prvok ktory je vacsi ako pivot
			while (table.getItemAtIndex(leftIndex).getKey() < pivot)	//da sa napisat aj takto zle citatelne - while (table.getItemAtIndex(leftIndex++).getKey() < pivot);
			{
				leftIndex++;
			}
			//z pravej strany hladame prvok ktory je mensi ako pivot
			while (table.getItemAtIndex(rightIndex).getKey() > pivot)
			{
				rightIndex--;
			}

			if (leftIndex <= rightIndex)
			{	//tak ich vymenime
				table.swap(leftIndex, rightIndex);
				notify();
				leftIndex++;
				rightIndex--;
			}
		} while (leftIndex <= rightIndex);	//pokial toto plati tak pokracujem //zlozitost O(n+1) alebo O(n) (ale nie O(n^2) aj ked su tu vnorene cykly!)
		
		if (beginIndex < rightIndex)	//ma zmysel triedit len ak (rightIndex - beginIndex > 1)
		{
			quick(table, beginIndex, rightIndex);//utriedenie lavej casti tabulky
		}
		if (leftIndex < endIndex)
		{
			quick(table, leftIndex, endIndex);//utriedenie pravej casti tabulky
		}
		

	}

}