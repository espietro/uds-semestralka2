#pragma once

#include "table.h"
#include "../list/list.h"
#include <stdexcept>

namespace structures 
{

	/// <summary> Sekvencna tabulka. </summary>
	/// <typeparam name = "K"> Kluc prvkov v tabulke. </typeparam>
	/// <typeparam name = "T"> Typ dat ukladanych v tabulke. </typeparam>
	template <typename K, typename T>
	class SequenceTable : public Table<K, T>
	{
	public:
		/// <summary> Destruktor. </sumary>
		~SequenceTable();

		/// <summary> Operacia klonovania. Vytvori a vrati duplikat udajovej struktury. </summary>
		/// <returns> Ukazovatel na klon struktury. </returns>
		virtual Structure* clone() const = 0;

		/// <summary> Vrati pocet prvkov v tabulke. </summary>
		/// <returns> Pocet prvkov v tabulke. </returns>
		size_t size() const override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Tabulka, z ktorej ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tato tabulka nachadza po priradeni. </returns>
		Table<K, T>& operator=(const Table<K, T>& other) override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Sekvencna tabulka, z ktorej ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tato tabulka nachadza po priradeni. </returns>
		SequenceTable<K, T>& operator=(const SequenceTable<K, T>& other);

		/// <summary> Vrati adresou data s danym klucom. </summary>
		/// <param name = "key"> Kluc dat. </param>
		/// <returns> Adresa dat s danym klucom. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak kluc nepatri do tabulky. </exception>  
		T& operator[](const K key) override;

		/// <summary> Vrati hodnotou data s daynm klucom. </summary>
		/// <param name = "key"> Kluc dat. </param>
		/// <returns> Hodnota dat s danym klucom. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak kluc nepatri do zoznamu. </exception>  
		const T operator[](const K key) const override;

		/// <summary> Vlozi data s danym klucom do tabulky. </summary>
		/// <param name = "key"> Kluc vkladanych dat. </param>
		/// <param name = "data"> Vkladane data. </param>
		/// <exception cref="std::logic_error"> Vyhodena, ak tabulka uz obsahuje data s takymto klucom. </exception>  
		void insert(const K& key, const T& data) override;

		/// <summary> Odstrani z tabulky prvok s danym klucom. </summary>
		/// <param name = "key"> Kluc prvku. </param>
		/// <returns> Odstranene data. </returns>
		/// <exception cref="std::logic_error"> Vyhodena, ak tabulka neobsahuje data s takymto klucom. </exception>  
		T remove(const K& key) override;

		/// <summary> Bezpecne ziska data s danym klucom. </summary>
		/// <param name = "key"> Kluc dat. </param>
		/// <param name = "data"> Najdene data (vystupny parameter). </param>
		/// <returns> true, ak sa podarilo najst a naplnit data s danym klucom, false inak. </returns>
		bool tryFind(const K& key, T& data) override;

		/// <summary> Zisti, ci tabulka obsahuje data s danym klucom. </summary>
		/// <param name = "key"> Kluc dat. </param>
		/// <returns> true, tabulka obsahuje dany kluc, false inak. </returns>
		bool containsKey(const K& key) override;

		/// <summary> Vymaze tabulku. </summary>
		void clear() override;

		/// <summary> Vrati skutocny iterator na zaciatok struktury </summary>
		/// <returns> Iterator na zaciatok struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<TableItem<K, T>*>* getBeginIterator() const override;

		/// <summary> Vrati skutocny iterator na koniec struktury </summary>
		/// <returns> Iterator na koniec struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<TableItem<K, T>*>* getEndIterator() const override;
	protected:
		/// <summary> Konstruktor. </summary>
		/// <param name = "list"> Zoznam, do ktoreho sa budu ukladat prvky tabulky. </param>
		/// <remarks> 
		/// Potomkovia ako list vlozia instanciu implicitne alebo explicitne implementovaneho zoznamu. 
		/// List dealokuje tento objekt.
		/// </remarks>
		SequenceTable(List<TableItem<K, T>*>* list);

		/// <summary> Najde prvok tabulky s danym klucom. </summary>
		/// <param name = "key"> Hladany kluc. </param>
		/// <returns> Prvok tabulky s danym klucom, ak neexistuje, vrati nullptr. </returns>
		virtual TableItem<K, T>* findTableItem(const K& key) const;
	protected:
		List<TableItem<K, T>*>* list_;
	};

	/**
		Destruktor.
	*/
	template<typename K, typename T>
	inline SequenceTable<K, T>::~SequenceTable()
	{
		clear();
		delete list_;
		list_ = nullptr;
	}

	/**
		Vrati pocet prvkov v tabulke. 
		returns - Pocet prvkov v tabulke.
	*/
	template<typename K, typename T>
	inline size_t SequenceTable<K, T>::size() const
	{
		return list_->size();
	}

	/**
		Operator priradenia. 
		other - Tabulka, z ktorej ma prebrat vlastnosti. 
		returns - Adresa, na ktorej sa tato tabulka nachadza po priradeni.
	*/
	template<typename K, typename T>
	inline Table<K, T>& SequenceTable<K, T>::operator=(const Table<K, T>& other)
	{
		if (this != &other)
		{
			*this = dynamic_cast<const SequenceTable<K, T>&>(other);
		}
		return *this;
	}

	/**
		Operator priradenia. 
		other - Sekvencna tabulka, z ktorej ma prebrat vlastnosti. 
		returns - Adresa, na ktorej sa tato tabulka nachadza po priradeni.
	*/
	template<typename K, typename T>
	inline SequenceTable<K, T>& SequenceTable<K, T>::operator=(const SequenceTable<K, T>& other)	//vzdy musim skontrolovat ci nepriradzujem sam do seba a musim z inej struktury priradit vlastnosti do seba(this)
	{
		if (this != &other)
		{
			//list_ = other.list_; toto by sme len skopirovali smernik na list druhej struktury
			//*list_ = *other.list_; //takto sa zavola operator priradenia listu ale skopirovali by sa mi len pointre na objekty typu TableItem
			//takze potrebujeme to spravit v cykle pre kazdy TableItem
			clear();	//najskor sam seba vymazem
			for each (TableItem<K,T>* otherItem in *other.list_)
			{
				list_->add(new TableItem<K, T>(*otherItem)); //kopy konstruktor potrebuje objekt, nie pointer preto dereferncujem
			}												//nerobime insert lebo add ma O(1) insert by sme dostali O(n^2)
		}
		return *this;
	}

	/**
		Vrati adresou data s danym klucom. 
		key - Kluc dat. 
		returns - Adresa dat s danym klucom. 
		exception std::out_of_range - Vyhodena, ak kluc nepatri do tabulky.
	*/
	template<typename K, typename T>
	inline T & SequenceTable<K, T>::operator[](const K key)
	{
		TableItem<K, T>* item = findTableItem(key);
		if (item != nullptr)
		{
			return item->accessData();
		}
		else
		{
			throw std::out_of_range("Kluc neexistuje.");
		}
	}

	/**
		Vrati hodnotou data s daynm klucom. 
		key - Kluc dat. 
		returns - Hodnota dat s danym klucom. 
		exception std::out_of_range - Vyhodena, ak kluc nepatri do zoznamu.
	*/
	template<typename K, typename T>
	inline const T SequenceTable<K, T>::operator[](const K key) const
	{
		TableItem<K, T>* item = findTableItem(key);
		if (item != nullptr)
		{
			return item->accessData();
		}
		else
		{
			throw std::out_of_range("Kluc neexistuje.");
		}
	}

	/**
		Vlozi data s danym klucom do tabulky. 
		key - Kluc vkladanych dat. 
		data - Vkladane data. 
		//neplati//exception std::logic_error - Vyhodena, ak tabulka uz obsahuje data s takymto klucom.
	*/
	template<typename K, typename T>
	inline void SequenceTable<K, T>::insert(const K & key, const T & data)
	{
		TableItem<K, T>* item = new TableItem<K, T>(key, data);
		list_->add(item);
		/*if (!containsKey(key))
		{
			TableItem<K, T>* item = new TableItem<K, T>(key, data);
			list_->add(item);
		}
		else 
		{
			throw std::logic_error("Kluc uz existuje.");
		}	*/	
	}

	/**
		Odstrani z tabulky prvok s danym klucom. 
		key - Kluc prvku. 
		returns - Odstranene data. 
		exception std::logic_error - Vyhodena, ak tabulka neobsahuje data s takymto klucom.
	*/
	template<typename K, typename T>
	inline T SequenceTable<K, T>::remove(const K & key)
	{
		TableItem<K, T>* item = findTableItem(key);
		if (item != nullptr)
		{
			T data = item->accessData();
			list_->tryRemove(item);
			delete item;
			return data;
			//
		}
		throw std::logic_error("Kluc neexistuje.");
	}

	/**
		Bezpecne ziska data s danym klucom. 
		key - Kluc dat. 
		data - Najdene data (vystupny parameter). 
		return - true, ak sa podarilo najst a naplnit data s danym klucom, false inak. 
	*/
	template<typename K, typename T>
	inline bool SequenceTable<K, T>::tryFind(const K & key, T & data)
	{
		TableItem<K, T>* item = findTableItem(key);
		if (item != nullptr)
		{
			data = item->accessData();
			return true;
		}
		return false;
	}

	/**
		Zisti, ci tabulka obsahuje data s danym klucom. 
		key - Kluc dat. 
		return - true, tabulka obsahuje dany kluc, false inak.
	*/
	template<typename K, typename T>
	inline bool SequenceTable<K, T>::containsKey(const K & key)
	{
		TableItem<K, T>* item = findTableItem(key);
		return item != nullptr;
		
	}

	/**
		Vymaze tabulku.
	*/
	template<typename K, typename T>
	inline void SequenceTable<K, T>::clear()
	{
		for each (TableItem<K,T>* item in *list_)
		{
			delete item;
		}
		list_->clear();
	}

	/**
		Vrati skutocny iterator na zaciatok struktury 
		return - Iterator na zaciatok struktury. 
		remark - Zabezpecuje polymorfizmus.
	*/
	template<typename K, typename T>
	inline Iterator<TableItem<K, T>*>* SequenceTable<K, T>::getBeginIterator() const	
	{
		return list_->getBeginIterator();	//vyuzivame uz implementovane metody v liste
	}

	/**
		Vrati skutocny iterator na koniec struktury 
		returns - Iterator na koniec struktury. 
		remarks - Zabezpecuje polymorfizmus. 
	*/
	template<typename K, typename T>
	inline Iterator<TableItem<K, T>*>* SequenceTable<K, T>::getEndIterator() const
	{
		return list_->getEndIterator();	//vyuzivame uz implementovane metody v liste
	}

	/**
		Konstruktor. 
		list - Zoznam, do ktoreho sa budu ukladat prvky tabulky. 
		remarks - 
		Potomkovia ako list vlozia instanciu implicitne alebo explicitne implementovaneho zoznamu. 
		List dealokuje tento objekt.
	*/
	template<typename K, typename T>
	inline SequenceTable<K, T>::SequenceTable(List<TableItem<K, T>*>* list) :
		Table<K, T>(),
		list_(list)
	{
	}

	/**
		Najde prvok tabulky s danym klucom. 
		key - Hladany kluc.
		return - Prvok tabulky s danym klucom, ak neexistuje, vrati nullptr.
	*/
	template<typename K, typename T>
	inline TableItem<K, T>* SequenceTable<K, T>::findTableItem(const K & key) const
	{
		for each (TableItem<K,T>* item in *list_)
		{
			if (item->getKey() == key)	//objekt potrebuje mat implementovany operator porovnania
			{
				return item;
			} 
		}
		return nullptr;	//nenasli sme taky prvok 
	}

}