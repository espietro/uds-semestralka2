#pragma once

#include "table.h"
#include "../list/list.h"
#include "../tree/binary_tree.h"
#include <stdexcept>
#include <iostream>

namespace structures
{

	/// <summary> Binarny vyhladavaci strom. </summary>
	/// <typeparam name = "K"> Kluc prvkov v tabulke. </typeparam>
	/// <typeparam name = "T"> Typ dat ukladanych v tabulke. </typeparam>
	template <typename K, typename T>
	class BinarySearchTree : public Table<K, T>
	{
	public:		
		typedef typename BinaryTreeNode<TableItem<K, T>*> BSTTreeNode;
	public:
		/// <summary> Konstruktor. </summary>
		BinarySearchTree();

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> BinarySearchTree, z ktoreho sa prevezmu vlastnosti. </param>
		BinarySearchTree(const BinarySearchTree<K, T>& other);

		/// <summary> Destruktor. </summary>
		~BinarySearchTree();

		/// <summary> Operacia klonovania. Vytvori a vrati duplikat udajovej struktury. </summary>
		/// <returns> Ukazovatel na klon struktury. </returns>
		virtual Structure* clone() const;

		/// <summary> Vrati pocet prvkov v tabulke. </summary>
		/// <returns> Pocet prvkov v tabulke. </returns>
		size_t size() const override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Tabulka, z ktorej ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tato tabulka nachadza po priradeni. </returns>
		Table<K, T>& operator=(const Table<K, T>& other) override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Binarny vyhladavaci strom, z ktoreho ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tato tabulka nachadza po priradeni. </returns>
		virtual BinarySearchTree<K, T>& operator=(const BinarySearchTree<K, T>& other);

		/// <summary> Vrati adresou data s danym klucom. </summary>
		/// <param name = "key"> Kluc dat. </param>
		/// <returns> Adresa dat s danym klucom. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak kluc nepatri do tabulky. </exception>  
		T& operator[](const K key) override;

		/// <summary> Vrati hodnotou data s daynm klucom. </summary>
		/// <param name = "key"> Kluc dat. </param>
		/// <returns> Hodnota dat s danym klucom. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak kluc nepatri do zoznamu. </exception>  
		const T operator[](const K key) const override;

		/// <summary> Vlozi data s danym klucom do tabulky. </summary>
		/// <param name = "key"> Kluc vkladanych dat. </param>
		/// <param name = "data"> Vkladane data. </param>
		/// <exception cref="std::logic_error"> Vyhodena, ak tabulka uz obsahuje data s takymto klucom. </exception>  
		void insert(const K& key, const T& data) override;

		/// <summary> Odstrani z tabulky prvok s danym klucom. </summary>
		/// <param name = "key"> Kluc prvku. </param>
		/// <returns> Odstranene data. </returns>
		/// <exception cref="std::logic_error"> Vyhodena, ak tabulka neobsahuje data s takymto klucom. </exception>  
		T remove(const K& key) override;

		/// <summary> Bezpecne ziska data s danym klucom. </summary>
		/// <param name = "key"> Kluc dat. </param>
		/// <param name = "data"> Najdene data (vystupny parameter). </param>
		/// <returns> true, ak sa podarilo najst a naplnit data s danym klucom, false inak. </returns>
		bool tryFind(const K& key, T& data) override;

		/// <summary> Zisti, ci tabulka obsahuje data s danym klucom. </summary>
		/// <param name = "key"> Kluc dat. </param>
		/// <returns> true, tabulka obsahuje dany kluc, false inak. </returns>
		bool containsKey(const K& key) override;

		/// <summary> Vymaze tabulku. </summary>
		void clear() override;

		/// <summary> Vrati skutocny iterator na zaciatok struktury </summary>
		/// <returns> Iterator na zaciatok struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<TableItem<K, T>*>* getBeginIterator() const override;

		/// <summary> Vrati skutocny iterator na koniec struktury </summary>
		/// <returns> Iterator na koniec struktury. </returns>
		/// <remarks> Zabezpecuje polymorfizmus. </remarks>
		Iterator<TableItem<K, T>*>* getEndIterator() const override;
	protected:
		/// <summary> Najde vrchol binarneho vyhladavacieho stromu s danym klucom. </summary>
		/// <param name = "key"> Hladany kluc. </param>
		/// <param name = "found"> Vystupny parameter, ktory indikuje, ci sa kluc nasiel. </param>
		/// <returns> Vrchol binarneho vyhladavacieho stromu s danym klucom. Ak sa kluc v tabulke nenachadza, vrati otca, ktoreho by mal mat vrchol s takym klucom. </returns>
		typename BSTTreeNode* findBSTNode(const K& key, bool & found) const;
	protected:
		/// <summary> Binarny strom s datami. </summary>
		BinaryTree<TableItem<K, T>*>* binaryTree_;
		/// <summary> Pocet prvkov v binarnom vyhladavacom strome. </summary>
		size_t size_;

		/// <summary> Vlozi vrchol do stromu tak, aby nedoslo k naruseniu usporiadania BST. </summary>
		/// <param name = "node"> Vrchol stromu, ktory ma byt vlozeny. </param>
		/// <returns> true, ak sa podarilo vrchol vlozit (teda v strome nie je vrchol s rovnakym klucom), false inak. </returns>
		bool tryToInsertNode(BSTTreeNode* node);

		/// <summary> Bezpecne vyjme zo stromu vrchol stromu tak, aby nedoslo k naruseniu usporiadania BST. </summary>
		/// <param name = "node"> Vrchol stromu, ktory ma byt vyjmuty. </param>
		/// <remarks> Vrchol nebude zruseny, iba odstraneny zo stromu a ziadne vrcholy nebudu ukazovat na neho a ani on nebude ukazovat na ziadne ine vrcholy. </remarks>
		void extractNode(BSTTreeNode* node);
	};

	/**
		Konstruktor. 
	*/
	template<typename K, typename T>
	inline BinarySearchTree<K, T>::BinarySearchTree():
		Table<K, T>(),
		binaryTree_(new BinaryTree<TableItem<K, T>*>()),
		size_(0)
	{
	}

	/**
		Kopirovaci konstruktor. 
		other - BinarySearchTree, z ktoreho sa prevezmu vlastnosti. 
	*/
	template<typename K, typename T>
	inline BinarySearchTree<K, T>::BinarySearchTree(const BinarySearchTree<K, T>& other):
		BinarySearchTree()
	{
		*this = other;
	}

	/**
		Destruktor. 
	*/
	template<typename K, typename T>
	inline BinarySearchTree<K, T>::~BinarySearchTree()	//6
	{
		clear();		
		delete binaryTree_;
		binaryTree_ = nullptr;
	}

	/**
		Operacia klonovania. Vytvori a vrati duplikat udajovej struktury. 
		returns - Ukazovatel na klon struktury. 
	*/
	template<typename K, typename T>
	inline Structure * BinarySearchTree<K, T>::clone() const
	{
		return new BinarySearchTree<K, T>(*this);
	}

	/**
		Vrati pocet prvkov v tabulke. 
		returns - Pocet prvkov v tabulke. 
	*/
	template<typename K, typename T>
	inline size_t BinarySearchTree<K, T>::size() const	//7
	{
		return size_;
	}

	/**
		Operator priradenia. 
		other - Tabulka, z ktorej ma prebrat vlastnosti. 
		returns - Adresa, na ktorej sa tato tabulka nachadza po priradeni. 
	*/
	template<typename K, typename T>
	inline Table<K, T>& BinarySearchTree<K, T>::operator=(const Table<K, T>& other)	
	{
		if (this != &other)
		{
			*this = dynamic_cast<const BinarySearchTree<K, T>&>(other);
		}
		return *this;
	}

	/**
		Operator priradenia. 
		other - Binarny vyhladavaci strom, z ktoreho ma prebrat vlastnosti. 
		returns - Adresa, na ktorej sa tato tabulka nachadza po priradeni. 
	*/
	template<typename K, typename T>
	inline BinarySearchTree<K, T>& BinarySearchTree<K, T>::operator=(const BinarySearchTree<K, T>& other)	//9	mozeme pouzit level/pre-orderIterator
	{
		if (this != &other)
		{
			clear();
			Tree<TableItem<K, T>*>::PreOrderTreeIterator cur(other.binaryTree_->getRoot());	//simulujeme preOrderIterator	//su alkovane na stacku
			Tree<TableItem<K, T>*>::PreOrderTreeIterator end(nullptr);
			
			while (cur != end)	//simulujeme foreach
			{
				insert((*cur)->getKey(), (*cur)->accessData());
				++cur;
			}
		}
		return *this;
	}

	/**
		Vrati adresou data s danym klucom. 
		key - Kluc dat. 
		returns - Adresa dat s danym klucom. 
		exception std::out_of_range - Vyhodena, ak kluc nepatri do tabulky. </exception>  
	*/
	template<typename K, typename T>
	inline T & BinarySearchTree<K, T>::operator[](const K key)
	{
		bool found = false;
		BSTTreeNode* node = findBSTNode(key, found);
		if (found)
		{
			return node->accessData()->accessData();
		}
		else
		{
			throw std::out_of_range("Kluc neexistuje.");
		}
	}

	/**
		Vrati hodnotou data s daynm klucom. 
		key - Kluc dat. 
		returns - Hodnota dat s danym klucom. 
		exception std::out_of_range - Vyhodena, ak kluc nepatri do zoznamu.
	*/
	template<typename K, typename T>
	inline const T BinarySearchTree<K, T>::operator[](const K key) const
	{
		bool found = false;
		BSTTreeNode* node = findBSTNode(key, found);
		if (found)
		{
			return node->accessData()->accessData();
		}
		else
		{
			throw std::out_of_range("Kluc neexistuje.");
		}
	}

	/**
		Vlozi data s danym klucom do tabulky. 
		key - Kluc vkladanych dat. 
		data - Vkladane data. 
		exception std::logic_error - Vyhodena, ak tabulka uz obsahuje data s takymto klucom. </exception>  
	*/
	template<typename K, typename T>
	inline void BinarySearchTree<K, T>::insert(const K & key, const T & data)	//4
	{
		TableItem<K, T>* item = new TableItem<K, T>(key, data);
		//BinaryTreeNode<TableItem<K, T>*> BSTTreeNode;		
		BSTTreeNode* node = new BSTTreeNode(item);
		
		if (tryToInsertNode(node))
		{
			++size_;
		}
		else
		{
			delete node;
			delete item;
			throw std::logic_error("Kluc uz existuje.");
		}
	}

	/**
		Odstrani z tabulky prvok s danym klucom. 
		key - Kluc prvku. 
		returns - Odstranene data. 
		exception std::logic_error - Vyhodena, ak tabulka neobsahuje data s takymto klucom. </exception>  
	*/
	template<typename K, typename T>
	inline T BinarySearchTree<K, T>::remove(const K & key)	//zavolam extract node a z toho node vytiahnem data
	{
		bool found = false;
		BSTTreeNode* node = findBSTNode(key, found);
		if (found)
		{
			extractNode(node);
			T data = node->accessData()->accessData();
			TableItem<K, T>* pom = node->accessData();
			delete node;
			delete pom;			
			--size_;
			return data;
		}
		else
		{
			throw std::logic_error("Kluc neexistuje");
		}		
	}

	/**
		Bezpecne ziska data s danym klucom. 
		key - Kluc dat. 
		data - Najdene data (vystupny parameter). 
		returns - true, ak sa podarilo najst a naplnit data s danym klucom, false inak. 
	*/
	template<typename K, typename T>
	inline bool BinarySearchTree<K, T>::tryFind(const K & key, T & data)	//2
	{
		bool found = false;
		BSTTreeNode* node = findBSTNode(key, found);
		if (found)
		{
			data = node->accessData()->accessData();			
		}
		return found;
	}

	/**
		Zisti, ci tabulka obsahuje data s danym klucom. 
		key - Kluc dat. 
		returns - true, tabulka obsahuje dany kluc, false inak. 
	*/
	template<typename K, typename T>
	inline bool BinarySearchTree<K, T>::containsKey(const K & key)	//2
	{
		bool found = false;
		findBSTNode(key, found);
		return found;
	}

	/**
		Vymaze tabulku. 
	*/
	template<typename K, typename T>
	inline void BinarySearchTree<K, T>::clear()	//5
	{
		for each (TableItem<K,T>* item in *binaryTree_)
		{
			delete item;						
		}
		binaryTree_->clear();
		size_ = 0;
	}

	/**
		Vrati skutocny iterator na zaciatok struktury 
		returns - Iterator na zaciatok struktury. 
		remarks - Zabezpecuje polymorfizmus. 
	*/
	template<typename K, typename T>
	inline Iterator<TableItem<K, T>*>* BinarySearchTree<K, T>::getBeginIterator() const		//8	pouzivame inOrderIterator
	{
		//return new BinaryTree<TableItem<K, T>*>::InOrderTreeIterator(binaryTree_->getRoot()); keby sme nemali tu druhu metodu 
		return binaryTree_->getBeginIterator();	//vrati inorderIterator
	}

	/**
		Vrati skutocny iterator na koniec struktury 
		returns - Iterator na koniec struktury. 
		remarks - Zabezpecuje polymorfizmus. 
	*/
	template<typename K, typename T>
	inline Iterator<TableItem<K, T>*>* BinarySearchTree<K, T>::getEndIterator() const
	{
		return binaryTree_->getEndIterator();
	}

	/**
		Najde vrchol binarneho vyhladavacieho stromu s danym klucom. 
		key - Hladany kluc. 
		found - Vystupny parameter, ktory indikuje, ci sa kluc nasiel. 
		returns - Vrchol binarneho vyhladavacieho stromu s danym klucom. Ak sa kluc v tabulke nenachadza, vrati otca, ktoreho by mal mat vrchol s takym klucom.
	*/
	template<typename K, typename T>
	inline typename BinarySearchTree<K,T>::BSTTreeNode* BinarySearchTree<K, T>::findBSTNode(const K & key, bool & found) const	//1
	{
		BSTTreeNode *cur = dynamic_cast<BSTTreeNode*>(binaryTree_->getRoot());
		BSTTreeNode *parent = nullptr;

		while (cur != nullptr)
		{
			parent = cur;
			if (key == cur->accessData()->getKey())
			{
				found = true;
				return cur;
			}
			else if(key < cur->accessData()->getKey())
			{
				cur = cur->getLeftSon();
			}
			else
			{
				cur = cur->getRightSon();
			}
		}		
		found = false;
		return parent;
	}

	/**
		Vlozi vrchol do stromu tak, aby nedoslo k naruseniu usporiadania BST. 
		node - Vrchol stromu, ktory ma byt vlozeny. 
		returns - true, ak sa podarilo vrchol vlozit (teda v strome nie je vrchol s rovnakym klucom), false inak. 
	*/
	template<typename K, typename T>
	inline bool BinarySearchTree<K, T>::tryToInsertNode(BSTTreeNode* node)	//3
	{
		bool found = false;
		K key = node->accessData()->getKey();
		BSTTreeNode* parent = findBSTNode(key, found);	//ak je strom prazdny tak mi toto vrati nullptr
		if (!found)
		{
			if (parent != nullptr)	//ak strom nie je prazdny tak node len pridam
			{
				if (key < parent->accessData()->getKey())
				{
					parent->setLeftSon(node);
					//ak to uz nieje v setLeft alebo right son tak to sem pridat
					//node->setParent(parent);
				}
				else
				{
					parent->setRightSon(node);
				}				
			}
			else	//ak strom je prazdny
			{
				binaryTree_->replaceRoot(node);
			}
			return true;
		}
		return false;
	}

	/**
		Bezpecne vyjme zo stromu vrchol stromu tak, aby nedoslo k naruseniu usporiadania BST. 
		node - Vrchol stromu, ktory ma byt vyjmuty. 
		remarks - Vrchol nebude zruseny, iba odstraneny zo stromu a ziadne vrcholy nebudu ukazovat na neho a ani on nebude ukazovat na ziadne ine vrcholy. 
	*/
	template<typename K, typename T>
	inline void BinarySearchTree<K, T>::extractNode(BSTTreeNode* node)	//odrezeme node zo stromu a potomkov len poprehadzujeme spravne
	{//ked ma vrchol 2 synov tak vymenime synov a vyvolavame funkciu extractNode rekurzivne
		if (node != nullptr)
		{
			int numOfSons = node->numberOfSons();
			if (numOfSons == 0)		//ak nema synov
			{	
				if (node->isLeftSon())
				{	//a je lavym synom svojho otca
					node->getParent()->setLeftSon(nullptr);
				}
				else if (node->isRightSon())
				{	//alebo pravym synom svojho otca
					node->getParent()->setRightSon(nullptr);
				}				
				else
				{	//alebo nema ziadneho otca a je korenom
					binaryTree_->replaceRoot(nullptr);
				}
			}
			else if (numOfSons == 1)	//ak ma 1 syna
			{
				BSTTreeNode* singleSon;	//tak zistime ktory z 2 je rozny od null
				singleSon = node->hasLeftSon() ? node->getLeftSon() : node->getRightSon();

				//odpojim syna od seba
				if (singleSon->isLeftSon())
				{
					node->setLeftSon(nullptr);
				}
				else
				{
					node->setRightSon(nullptr);
				}

				//napojime syna node s otcom vrcholu node
				if (node->isLeftSon())
				{	//ak je lavym synom svojho otca
					node->getParent()->setLeftSon(singleSon);					
				}
				else if (node->isRightSon())
				{	//alebo ak je pravym synom svojho otca
					node->getParent()->setRightSon(singleSon);					
				}	
				else
				{	//alebo ak nema ziadneho otca a je korenom					
					binaryTree_->replaceRoot(singleSon);					
				}				
			}
			else/* if (numOfSons == 2)*/	//Ru�en� vrchol sa nahrad� bu� 
			{
				BSTTreeNode* inOrderSon = node->getLeftSon();		//najpravej��m vrcholom v �avom podstrome  (InOrder predchodca)
				while (inOrderSon->hasRightSon())
				{
					inOrderSon = inOrderSon->getRightSon();
				}
				//alebo naj�avej��m vrcholom v pravom podstrome (InOrder nasledovn�k)
				/*BSTTreeNode* inOrderSon = node->getRightSon();	
				while (inOrderSon->hasLeftSon())
				{
					inOrderSon = inOrderSon->getLeftSon();
				}*/	  

				extractNode(inOrderSon);	//odoberieme vrchol ktorym nahradime povodny node
				
				//dame vediet aj otcovi povodneho node ze ma noveho syna 
				if (node->isLeftSon())
				{	//ak node ma otca a je jeho lavym synom 
					node->getParent()->setLeftSon(inOrderSon);
				}
				else if (node->isRightSon())
				{	//ak node ma otca a je jeho pravym synom 
					node->getParent()->setRightSon(inOrderSon);
				}
				else
				{	//alebo ak node je korenom stromu(nema otca)
					binaryTree_->replaceRoot(inOrderSon);
				}
				inOrderSon->setLeftSon(node->getLeftSon());
				inOrderSon->setRightSon(node->getRightSon());

				node->setLeftSon(nullptr);
				node->setRightSon(nullptr);
			}			
		}		
	}
}