#pragma once

#include "../list/array_list.h"
#include "sequence_table.h"

namespace structures
{

	/// <summary> Utriedena sekvencna tabulka. </summary>
	/// <typeparam name = "K"> Kluc prvkov v tabulke. </typeparam>
	/// <typeparam name = "T"> Typ dat ukladanych v tabulke. </typeparam>
	template <typename K, typename T>
	class SortedSequenceTable : public SequenceTable<K, T>
	{
	public:
		/// <summary> Konstruktor. </summary>
		SortedSequenceTable();

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> SortedSequenceTable, z ktorej sa prevezmu vlastnosti. </param>
		SortedSequenceTable(const SortedSequenceTable<K, T>& other);

		/// <summary> Operacia klonovania. Vytvori a vrati duplikat udajovej struktury. </summary>
		/// <returns> Ukazovatel na klon struktury. </returns>
		Structure* clone() const override;

		/// <summary> Vlozi data s danym klucom do tabulky. </summary>
		/// <param name = "key"> Kluc vkladanych dat. </param>
		/// <param name = "data"> Vkladane data. </param>
		/// <exception cref="std::logic_error"> Vyhodena, ak tabulka uz obsahuje data s takymto klucom. </exception>  
		void insert(const K& key, const T& data) override;
	protected:
		/// <summary> Najde prvok tabulky s danym klucom. </summary>
		/// <param name = "key"> Hladany kluc. </param>
		/// <returns> Prvok tabulky s danym klucom, ak neexistuje, vrati nullptr. </returns>
		TableItem<K, T>* findTableItem(const K& key) const override;
	private:
		/// <summary> Najde index prvku s danym klucom. Kontroluje rozsah [indexStart, indexEnd). Pracuje na principe bisekcie. </summary>
		/// <param name = "key"> Hladany kluc. </param>
		/// <param name = "indexStart"> Pociatocny index prehladavanej sekvencie. </param>
		/// <param name = "indexEnd"> Koncovy index prehladavanej sekvencie. </param>
		/// <param name = "found"> Vystupny parameter, ktory indikuje, ci sa kluc nasiel. </param>
		/// <returns> Index prvku s danym klucom. Ak sa kluc v tabulke nenachadza, vrati index, kde by sa mal prvok s takym klucom nachadzat. </returns>
		int indexOfKey(const K& key, int indexStart, int indexEnd, bool& found) const;
	};

	/**
		Konstruktor.
	*/
	template<typename K, typename T>
	inline SortedSequenceTable<K, T>::SortedSequenceTable() :
		SequenceTable<K, T>(new ArrayList<TableItem<K, T>*>())
	{
	}

	/**
		Kopirovaci konstruktor. 
		other - SortedSequenceTable, z ktorej sa prevezmu vlastnosti.
	*/
	template<typename K, typename T>
	inline SortedSequenceTable<K, T>::SortedSequenceTable(const SortedSequenceTable<K, T>& other) :
		SortedSequenceTable()
	{
		*this = other;
	}

	/**	
		Operacia klonovania. Vytvori a vrati duplikat udajovej struktury. 
		returns - Ukazovatel na klon struktury.
	*/
	template<typename K, typename T>
	inline Structure * SortedSequenceTable<K, T>::clone() const
	{
		return new SortedSequenceTable<K, T>(*this);
	}

	/**
		Vlozi data s danym klucom do tabulky. 
		key - Kluc vkladanych dat. 
		data - Vkladane data. 
		exception std::logic_error - Vyhodena, ak tabulka uz obsahuje data s takymto klucom.
	*/
	template<typename K, typename T>
	inline void SortedSequenceTable<K, T>::insert(const K & key, const T & data)
	{
		bool found = false;
		int index = indexOfKey(key, 0, list_->size(), found);
		if (!found)
		{
			list_->insert(new TableItem<K, T>(key, data), index);
		}
		else
		{
			throw std::logic_error("Kluc uz existuje.");
		}
	}

	/**
		Najde prvok tabulky s danym klucom. 
		key - Hladany kluc. 
		returns - Prvok tabulky s danym klucom, ak neexistuje, vrati nullptr.
	*/
	template<typename K, typename T>
	inline TableItem<K, T>* SortedSequenceTable<K, T>::findTableItem(const K & key) const	
	{
		bool found = false;
		int index = indexOfKey(key, 0, list_->size(), found);
		if (found)
		{
			return (*list_)[index];
		}
		else
		{
			return nullptr;
		}
	}

	/**
		Najde index prvku s danym klucom. Kontroluje rozsah [indexStart, indexEnd). Pracuje na principe bisekcie. 
		key - Hladany kluc. 
		indexStart - Pociatocny index prehladavanej sekvencie. 
		indexEnd - Koncovy index prehladavanej sekvencie. 
		found - Vystupny parameter, ktory indikuje, ci sa kluc nasiel. 
		returns - Index prvku s danym klucom. Ak sa kluc v tabulke nenachadza, vrati index, kde by sa mal prvok s takym klucom nachadzat.
	*/
	template<typename K, typename T>
	inline int SortedSequenceTable<K, T>::indexOfKey(const K & key, int indexStart, int indexEnd, bool & found) const	//implementuje bisekciu	//indexEnd neberieme do uvahy <indexStart,indexEnd)
	{
		//int indexMid = (indexStart + indexEnd) / 2;	//pri obrovskych cislach moze dost k preteceniu a dostaneme zle vysledky-
		int indexMid = indexStart + (indexEnd - indexStart) / 2;
		if (indexMid < indexEnd && key == (*list_)[indexMid]->getKey())	//nebude platit keby sa indexStart = indexEnd
		{
			found = true;
			return indexMid;
		}
		else if (indexStart == indexEnd)
		{
			found = false;
			return indexStart;
		}
		else if (key < (*list_)[indexMid]->getKey())
		{
			return indexOfKey(key, indexStart, indexMid, found);	
		}
		else
		{
			return indexOfKey(key, indexMid + 1, indexEnd, found);
		}
		
	}
}