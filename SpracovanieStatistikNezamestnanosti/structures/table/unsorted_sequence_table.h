#pragma once

#include "../list/array_list.h"
#include "sequence_table.h"

namespace structures
{

	/// <summary> Sekvencna neutriedena tabulka. </summary>
	/// <typeparam name = "K"> Kluc prvkov v tabulke. </typeparam>
	/// <typeparam name = "T"> Typ dat ukladanych v tabulke. </typeparam>
	template <typename K, typename T>
	class UnsortedSequenceTable : public SequenceTable<K, T>
	{
	public:
		/// <summary> Konstruktor. </summary>
		UnsortedSequenceTable();

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> NonortedSequenceTable, z ktorej sa prevezmu vlastnosti. </param>
		UnsortedSequenceTable(const UnsortedSequenceTable<K, T>& other);

		/// <summary> Operacia klonovania. Vytvori a vrati duplikat udajovej struktury. </summary>
		/// <returns> Ukazovatel na klon struktury. </returns>
		Structure* clone() const override;	

		/// <summary> Vrati prvok tabulky na danom indexe. </summary>
		/// <param name = "index"> Index prvku. </param>
		/// <returns> Prvok tabulky na danom indexe. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do presahuje rozsah sekvencnej tabulky. </exception> 
		TableItem<K, T>& getItemAtIndex(int index);

		/// <summary> Vymeni prvky na indexoch. </summary>
		/// <param name = "indexFirst"> Index prveho prvku. </param>
		/// <param name = "indexSecond"> Index druheho prvku. </param>
		void swap(int indexFirst, int indexSecond);

		/// <summary> Vymeni prvky tabulky. </summary>
		/// <param name = "first"> Prvy prvok. </param>
		/// <param name = "second"> Druhy prvok. </param>
		static void swap(TableItem<K, T>& first, TableItem<K, T>& second);
	};

	/**
		Konstruktor.
	*/
	template<typename K, typename T>
	inline UnsortedSequenceTable<K, T>::UnsortedSequenceTable() :
		SequenceTable<K, T>(new ArrayList<TableItem<K, T>*>())
	{
	}

	/**
		Kopirovaci konstruktor. 
		other - NonortedSequenceTable, z ktorej sa prevezmu vlastnosti.
	*/
	template<typename K, typename T>
	inline UnsortedSequenceTable<K, T>::UnsortedSequenceTable(const UnsortedSequenceTable<K, T>& other) :
		UnsortedSequenceTable()
	{
		*this = other;
	}

	/**
		Operacia klonovania. Vytvori a vrati duplikat udajovej struktury. 
		returns - Ukazovatel na klon struktury.
	*/
	template<typename K, typename T>
	inline Structure * UnsortedSequenceTable<K, T>::clone() const
	{
		return new UnsortedSequenceTable<K, T>(*this);
	}

	/**	
		Vrati prvok tabulky na danom indexe. 
		index - Index prvku. 
		returns - Prvok tabulky na danom indexe. </returns>
		exception std::out_of_range - Vyhodena, ak index nepatri do presahuje rozsah sekvencnej tabulky.
	*/
	template<typename K, typename T>
	inline TableItem<K, T>& UnsortedSequenceTable<K, T>::getItemAtIndex(int index)
	{		
		return *(*list_)[index];	//takto vratim objekt
		//return (*list_)[index]; // takto smernik
	}

	/**
		Vymeni prvky na indexoch. 
		indexFirst - Index prveho prvku. 
		indexSecond - Index druheho prvku.
	*/
	template<typename K, typename T>
	inline void UnsortedSequenceTable<K, T>::swap(int indexFirst, int indexSecond)
	{
		swap(getItemAtIndex(indexFirst), getItemAtIndex(indexSecond));
	}

	/**
		Vymeni prvky tabulky. 
		first - Prvy prvok. 
		second - Druhy prvok.
	*/
	template<typename K, typename T>
	inline void UnsortedSequenceTable<K, T>::swap(TableItem<K, T>& first, TableItem<K, T>& second)
	{
		//TableItem<K, T> bucket = first;	//vyvolava sa konstruktor ale bezparametricky, no taky trieda nema preto musime zavolat copy construktor
		TableItem<K, T> bucket(first);	//volame copy construktor
		first = second;
		second = bucket;
	}

}