#pragma once

#include "queue.h"
#include "../array/array.h"

#include <stdexcept>

namespace structures
{
	/// <summary> Implicitny front. </summary>
	/// <typeparam name = "T"> Typ dat ukladanych vo fronte. </typeparam>
	template<typename T>
	class ImplicitQueue : public Queue<T>
	{
	public:
		/// <summary> Konstruktor. </summary>
		ImplicitQueue();

		/// <summary> Parametricky konstruktor. </summary>
		/// <param name = "capacity"> Maximalna velkost frontu. </param>
		ImplicitQueue(const size_t capacity);

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> Implicitny front, z ktoreho sa prevezmu vlastnosti. </param>
		ImplicitQueue(const ImplicitQueue<T>& other);

		/// <summary> Destruktor. </summary>
		~ImplicitQueue();

		/// <summary> Operacia klonovania. Vytvori a vrati duplikat frontu. </summary>
		/// <returns> Ukazovatel na klon struktury. </returns>
		Structure* clone() const override;

		/// <summary> Vrati pocet prvkov vo fronte. </summary>
		/// <returns> Pocet prvkov vo fronte. </returns>
		size_t size() const override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Front, z ktoreho ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tento front nachadza po priradeni. </returns>
		Queue<T>& operator=(const Queue<T>& other) override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Front, z ktoreho ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa tento front nachadza po priradeni. </returns>
		ImplicitQueue<T>& operator=(const ImplicitQueue<T>& other);

		/// <summary> Vymaze front. </summary>
		void clear() override;

		/// <summary> Prida prvok do frontu. </summary>
		/// <param name = "data"> Pridavany prvok. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak je front plny. </exception>  
		void push(const T& data) override;

		/// <summary> Odstrani prvok z frontu. </summary>
		/// <returns> Prvok na zaciatku frontu. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak je front prazdny. </exception>  
		T pop() override;

		/// <summary> Vrati prvok na zaciatku frontu. </summary>
		/// <returns> Prvok na zaciatku frontu. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak je front prazdny. </exception>  
		T& peek() override;

		/// <summary> Vrati prvok na zaciatku frontu. </summary>
		/// <returns> Prvok na zaciatku frontu. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak je front prazdny. </exception>  
		const T peek() const override;

	protected:
		/// <summary> Pole, pomocou ktoreho je implementovany front. </summary>	
		Array<T>* array_;

		/// <summary> Index prveho prvku vo fronte. </summary>	
		int startIndex_;

		/// <summary> Pocet prvkov vo fronte. </summary>	
		size_t size_;
	};

	/**
		Konstruktor.
	*/
	template<typename T>
	ImplicitQueue<T>::ImplicitQueue() :	
		ImplicitQueue(10)	//vytvorime 10 prvkove pole
	{
	}

	/**
		Parametricky konstruktor.
		capacity - Maximalna velkost frontu.
	*/
	template<typename T>
	ImplicitQueue<T>::ImplicitQueue(const size_t capacity) :	//velkost pola ktore chceme vytvorit
		Queue(),
		array_(new Array<T>(capacity)),
		startIndex_(0),
		size_(0)
	{
	}

	/**
		Kopirovaci konstruktor. 
		other - Implicitny front, z ktoreho sa prevezmu vlastnosti.	//musim davat pozor na velkosti poli 
	*/
	template<typename T>
	ImplicitQueue<T>::ImplicitQueue(const ImplicitQueue<T>& other) :
		Queue(other),
		array_(new Array<T>(*other.array_)),
		startIndex_(other.startIndex_),
		size_(other.size_)
	{
	}

	/**
		Destruktor.
	*/
	template<typename T>
	ImplicitQueue<T>::~ImplicitQueue()
	{
		delete array_;	//nutny je len tento prvy riadok
		array_ = nullptr;
		startIndex_ = 0;
		size_ = 0;
	}

	/**
		Operator priradenia. 
		other - Front, z ktoreho ma prebrat vlastnosti. 
		return - Adresa, na ktorej sa tento front nachadza po priradeni.
	*/
	template<typename T>
	inline Queue<T>& ImplicitQueue<T>::operator=(const Queue<T>& other)
	{
		if (this != &other)
		{
			*this = dynamic_cast<const ImplicitQueue<T>&>(other);
		}
		return *this;
	}

	/**
		Operator priradenia. 
		other - Front, z ktoreho ma prebrat vlastnosti. 
		return - Adresa, na ktorej sa tento front nachadza po priradeni.
	*/
	template<typename T>
	inline ImplicitQueue<T>& ImplicitQueue<T>::operator=(const ImplicitQueue<T>& other)
	{
		if (this != &other)
		{
			if (other.size_ > array_->size())
			{
				throw std::logic_error("Queue sa neda priradit, je prilis velky");	//v pripade ze pocet aktivnych prvkov v other je vacsi ako kapacita mojho pola, tak nevieme priradit vacsie pole do mensieho
			}	//inak prekopirujeme prvky do nasho pola
			
			//*array_ = *other.array_; //zjednodusena verzia by polia priradila len ak by boli rovnakej velkosti, ak polia nie su rovnake vyhodi sa vynimka a uz sa nepokracuje			
			
			if (other.startIndex_ + other.size_ > other.array_->size())		//ak other ma prvky v poli napr.: [4,5,-,-,-,-,1,2,3] tak ich presunieme do noveho pola: [1,2,3,4,5,-]
			{																//tak ze najprv skoprujeme poslednu cast pola 1,2,3 a druhy krat prvu cast pola 4,5
				
				int pom = other.array_->size() - other.startIndex_;	//pocet prvkov od startIndex az po koniec pola (kapacitaPola - startIndex)

				Array<T>::copy(*other.array_, other.startIndex_, *array_, 0, pom);	//skopiruj prvky od startIndexu az po koniec pola 
				Array<T>::copy(*other.array_, 0, *array_, pom, other.size_ - pom);	//skopiruj prvky od zaciatku pola az po (pocetPrvkov - noUzSkpirovanychPrvkov)

				startIndex_ = 0;
			} 
			else
			{	//ak je front v jednom kuse len ho skopirujeme
				Array<T>::copy(*other.array_, other.startIndex_, *array_, 0, other.size_);
				startIndex_ = other.startIndex_;
			}
			size_ = other.size_;			
		}
		return *this;
	}

	/**
		Operacia klonovania. Vytvori a vrati duplikat frontu. 
		return - Ukazovatel na klon struktury.
	*/
	template<typename T>
	inline Structure * ImplicitQueue<T>::clone() const
	{
		return new ImplicitQueue<T>(*this);
	}

	/**
		Vrati pocet prvkov vo fronte.
		return - Pocet prvkov vo fronte.
	*/
	template<typename T>
	size_t ImplicitQueue<T>::size() const
	{
		return size_;
	}

	/**
		Vymaze front.
	*/
	template<typename T>
	inline void ImplicitQueue<T>::clear()
	{
		startIndex_ = 0;
		size_ = 0;
	}

	/**
		Prida prvok do frontu. 
		data - Pridavany prvok.
		exception std::out_of_range - Vyhodena, ak je front plny.
	*/
	template<typename T>
	inline void ImplicitQueue<T>::push(const T& data)
	{
		if (size_ >= array_->size())	//ked napisem array->size tak dostanem adresu funkcie size v pamati, ale vyhodi to warning, v C by to slo normalne
		{
			throw std::out_of_range("Queue je plny");
		}
		(*array_)[(startIndex_ + size_) % array_->size()] = data;
		size_++;
	}

	/**
		Odstrani prvok z frontu. 
		return - Prvok na zaciatku frontu. 
		exception std::out_of_range - Vyhodena, ak je front prazdny.
	*/
	template<typename T>
	inline T ImplicitQueue<T>::pop()	//vyberame zo zaciatku frontu
	{
		if(size_ <= 0)	//ak by bolo pole prazdne
		{
			throw std::out_of_range("Queue je prazdny");
		}
		T pom = (*array_)[startIndex_];
		startIndex_++;	//prvok nechame v poli, len posunieme index zaciatku a povazujeme ho za neplatne data, mozeme prepisat
		if (startIndex_ >= array_->size())	
		{
			startIndex_ = 0;	//ak by startIndex zacal ukazovat za pole, presunieme ho na zaciatok pola (cyklicky prechadzame pole)
		}
		size_--;	//pocet platnych prvkov v stacku znizime 
		return pom;
	}

	/**
		Vrati prvok na zaciatku frontu. 
		return - Prvok na zaciatku frontu. 
		exception std::out_of_range - Vyhodena, ak je front prazdny.
	*/
	template<typename T>
	inline T& ImplicitQueue<T>::peek()
	{
		if (size_ <= 0)	//ak by bolo pole prazdne
		{
			throw std::out_of_range("Queue je prazdny");
		}
		return (*array_)[startIndex_];
	}

	/**
		Vrati prvok na zaciatku frontu. 
		return - Prvok na zaciatku frontu. 
		exception std::out_of_range - Vyhodena, ak je front prazdny.
	*/
	template<typename T>
	inline const T ImplicitQueue<T>::peek() const
	{
		if (size_ <= 0)	//ak by bolo pole prazdne
		{
			throw std::out_of_range("Queue je prazdny");
		}
		return (*array_)[startIndex_];
	}
}