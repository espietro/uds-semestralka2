#pragma once

#include "../structure.h"
#include "../vector/vector.h"
#include "../ds_routines.h"

namespace structures 
{
	
	/// <summary> Pole. </summary>
	/// <typeparam name = "T"> Typ dat ukladanych v poli. </typeparam>
	template<typename T> 
	class Array	: public Structure
	{
	public:
		/// <summary> Konstruktor vytvori pole o velkosti size prvkov. </summary>
		/// <param name = "size"> Pocet prvkov vo vytvorenom poli. </param>
		Array(const size_t size);

		/// <summary> Kopirovaci konstruktor. </summary>
		/// <param name = "other"> Pole, z ktoreho sa prevezmu vlastnosti. </param>
		Array(const Array<T>& other);

		/// <summary> Destruktor. </summary>
		~Array();
		
		/// <summary> Operacia klonovania. Vytvori a vrati duplikat pola. </summary>
		/// <returns> Ukazovatel na vytvoreny klon pola. </returns>
		Structure* clone() const override;

		/// <summary> Vrati pocet prvkov v poli. </summary>
		/// <returns> Pocet prvkov v poli. </returns>
		size_t size() const override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Struktura (pole), z ktorej ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa toto pole nachadza po priradeni. </returns>
		/// <exception cref="std::logic_error"> Vyhodena, vstupny parameter nie je Array. </exception>  
		/// <remarks> Vyuziva typovy operator priradenia. </remarks>
		Structure& operator=(const Structure& other) override;

		/// <summary> Operator priradenia. </summary>
		/// <param name = "other"> Pole, z ktoreho ma prebrat vlastnosti. </param>
		/// <returns> Adresa, na ktorej sa toto pole nachadza po priradeni. </returns>
		Array<T>& operator=(const Array<T>& other);

		/// <summary> Operator porovnania. </summary>
		/// <param name = "other"> Pole, s ktorym sa ma porovnat. </param>
		/// <returns> true, ak su polia rovnake, false inak. </returns>
		bool operator==(const Array<T>& other) const;
		
		/// <summary> Vrati adresou prvok na indexe. </summary>
		/// <param name = "index"> Index prvku. </param>
		/// <returns> Adresa prvku na danom indexe. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do pola. </exception>  
		T& operator[](const int index);

		/// <summary> Vrati hodnotou prvok na indexe. </summary>
		/// <param name = "index"> Index prvku. </param>
		/// <returns> Hodnota prvku na danom indexe. </returns>
		/// <exception cref="std::out_of_range"> Vyhodena, ak index nepatri do pola. </exception>  
		const T operator[](const int index) const;

		/// <summary> Skopiruje length prvkov z pola src od pozicie srcStartIndex do pola dest od pozicie destStartIndex. </summary>
		/// <param name = "src"> Zdrojove pole. </param>
		/// <param name = "srcStartIndex"> Index prvku v poli src, od ktoreho sa zacne citat. </param>
		/// <param name = "dest"> Cielove pole. </param>
		/// <param name = "destStartIndex"> Index prvku v poli dest, od ktoreho sa zacne zapisovat. </param>
		/// <param name = "length"> Pocet prvkov, ktore budu skopirovane z pola src od pozicie srcStartIndex do pola dest od pozicie destStartIndex. </param>
		/// <exception cref="std::out_of_range"> Vyhodena, ak zaciatocne alebo koncove indexy nepatria do prislusnych poli. </exception> 
		static void copy(const Array<T>& src, const int srcStartIndex, Array<T>& dest, const int destStartIndex, const int length);

	private:
		/// <summary> Vektor s datami. </summary>
		Vector* vector_;
		/// <summary> Pocet prvkov v poli. </summary>
		size_t size_;	//Velkost pola, pocet prvkov ktore mozu byt ponate polom.

	private:
		/// <summary> Mapovacia funkcia, ktora prevedie index v poli na index vo vektore. </summary>
		/// <param name = "index"> Index v poli. </param>
		/// <returns> Index vo vektore. </returns>
		int mapFunction(const int index) const;
	};

	
	
	/**
		Konstruktor vytvori pole o velkosti size prvkov.
		size - Pocet prvkov vo vytvorenom poli.
	*/
	template<typename T>
	Array<T>::Array(const size_t size) :
		vector_(new Vector(size * sizeof(T))),	//array je postaveny nad Vectorom, vyalokuje pocetBytov x velkostTypu T
		size_(size)								//napriklad 20 intov je 20 * 4 = 80 bytovy Vector -> if(1 int == 4 byty)
	{											//sizeof() - vracia velkost pamatoveho typu v bytoch
	}

	/**
		Kopirovaci konstruktor. 
		other - Pole, z ktoreho sa prevezmu vlastnosti.
	*/
	template<typename T>
	Array<T>::Array(const Array<T>& other) :
		vector_(new Vector(*(other.vector_))), //volame copy construktor vectora
		size_(other.size_)
	{
	}

	/**
		 Destruktor.
	*/
	template<typename T>
	Array<T>::~Array()	
	{
		delete vector_;		//volame destruktor Vectora
		vector_ = nullptr;	//nepovinne ale dame to sem 
		size_ = 0;			//nepovinne ale dame to sem 
	}

	/**
		Operacia klonovania. Vytvori a vrati duplikat pola.
		return - Ukazovatel na vytvoreny klon pola.
	*/
	template<typename T>
	Structure* Array<T>::clone() const
	{
		return new Array<T>(*this);	//copy Constructor
	}

	/**
		Operator priradenia. 
		other - Struktura (pole), z ktorej ma prebrat vlastnosti. 
		return - Adresa, na ktorej sa toto pole nachadza po priradeni.
		exception std::logic_error - Vyhodena, vstupny parameter nie je Array.
		remarks - Vyuziva typovy operator priradenia.
	*/
	template<typename T>
	Structure& Array<T>::operator=(const Structure& other) 
	{
		if (this != &other) //aby som sam seba nepriradzoval do seba
		{
			*this = dynamic_cast<const Array<T>&>(other);
		}
		return *this;
	}

	/**
		Operator priradenia. 
		other - Pole, z ktoreho ma prebrat vlastnosti.
		return - Adresa, na ktorej sa toto pole nachadza po priradeni.
	*/
	template<typename T>
	Array<T>& Array<T>::operator=(const Array<T>& other)	//ak mame 2 smerniky tak ho mozme vyvolat aj p1->operator=(*p2);
	{
		if (this != &other)
		{
			if (size_ != other.size_)//ak mam rozne velkosti poli vyhodim vynimku
			{
				throw std::logic_error("Velkosti pol� su rozdielne.");
			}
			else	
			{
				//Vector::copy(*(other.vector_), 0, *vector_, 0, size_); 
				*vector_ = *(other.vector_); //volame operator priradenia definovaneho vo Vectore
			}
		}
		return *this;
		//throw std::exception("Array<T>::operator=: Not implemented yet.");
	}
	
	/**
		Vrati pocet prvkov v poli.
		return - Pocet prvkov v poli. //Velkost pola, pocet prvkov ktore mozu byt ponate polom.
	*/
	template<typename T>
	size_t Array<T>::size() const	//pocet prvkov v poli, vo vektore je to pocet bytov
	{
		return size_;
	}

	/**
		Vrati adresou prvok na indexe. 
		index - Index prvku.
		return - Adresa prvku na danom indexe. 
		exception std::out_of_range - Vyhodena, ak index nepatri do pola.
	*/
	template<typename T>
	T& Array<T>::operator[](const int index)	
	{
		//musime pristupit k vektoru
		//toto (*vector_)mapFunction(index) mi vrati len 1 byte preto to potrebujeme pretypovat na T
		//da sa to aj takto : return reinterpret_cast<T&>((*vector_)[mapFunction(index)]);
		//================
		int indexVoVectore = mapFunction(index);
		byte* pointerVoVectore = vector_->getBytePointer(indexVoVectore);
		T* typovyPointer = reinterpret_cast<T*>(pointerVoVectore);
		return *typovyPointer;
	}

	/**
		Vrati hodnotou prvok na indexe.
		index - Index prvku.
		return - Hodnota prvku na danom indexe. 
		exception std::out_of_range - Vyhodena, ak index nepatri do pola.
	*/
	template<typename T>	//ak je pri T& alebo ak tam nie je, tak telo funkcie je v oboch pripadoch rovnake 
	const T Array<T>::operator[](const int index) const	//const je tam preto aby ak mame pole smernikov tak tie smerniky nesmieme menit
	{
		//teraz musim pristupik ku kopii ale o to sa postara Kompilator 
		int indexVoVectore = mapFunction(index);
		byte* pointerVoVectore = vector_->getBytePointer(indexVoVectore);
		T* typovyPointer = reinterpret_cast<T*>(pointerVoVectore);
		return *typovyPointer;
	}

	/**
		Operator porovnania. 
		other - Pole, s ktorym sa ma porovnat.
		return - true, ak su polia rovnake, false inak.
	*/
	template<typename T>
	inline bool Array<T>::operator==(const Array<T>& other) const
	{
		return size_  == other.size_ && *vector_ == *(other.vector_);	//volame porovnavaci operator Vektora
	}

	/**
		Skopiruje length prvkov z pola src od pozicie srcStartIndex do pola dest od pozicie destStartIndex. //Staticka metoda
		src - Zdrojove pole.
		srcStartIndex - Index prvku v poli src, od ktoreho sa zacne citat. 
		dest - Cielove pole. 
		destStartIndex - Index prvku v poli dest, od ktoreho sa zacne zapisovat.
		length - Pocet prvkov, ktore budu skopirovane z pola src od pozicie srcStartIndex do pola dest od pozicie destStartIndex. 
		exception std::out_of_range - Vyhodena, ak zaciatocne alebo koncove indexy nepatria do prislusnych poli.
	*/
	template<typename T>	//TODO 02: Array	//mozem pouzit copy z Vectoru 
	void Array<T>::copy(const Array<T>& src, const int srcStartIndex, Array<T>& dest, const int destStartIndex, const int length)
	{
		if (length != 0)	//pocet bytov musi byt rozny od nuly
		{
			int srcPomIndex = src.mapFunction(srcStartIndex);
			int destPomIndex = dest.mapFunction(destStartIndex);
			int pomLength = length * sizeof(T);
			Vector::copy(*src.vector_, srcPomIndex, *dest.vector_, destPomIndex, pomLength);
			//vyuzit vektor triedy 
			//pocet prvkov treba prepocitat na pocet bytov vo vektore
			//a vynimku z vektoru treba prepisat -rethrow s inym popisom inak to moze byt zmatocne pre uzivatela
		}
	}

	/**
		Mapovacia funkcia, ktora prevedie index v poli na index vo vektore.
		index - Index v poli.
		return - Index vo vektore.
	*/
	template<typename T>
	inline int Array<T>::mapFunction(const int index) const	//TODO 02: Array
	{
		DSRoutines::rangeCheckExcept(index, 0, size_, "Index mimo pola."); //ta 0 tam nemusi byt pojde to rovnako
		return index * sizeof(T);	//mapujeme index pola do Vektoru
		
	}
}

