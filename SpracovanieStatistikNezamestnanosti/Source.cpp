#include <iostream>
#include <locale>
#include <Windows.h>
#include <limits>
#include <corecrt_wstring.h>
#include "structures/heap_monitor.h"
#include "Kraj.h"
#include "Stat.h"

bool can_continue();
void menu();
void hladaj_okres_no_nezam_form(Stat* stat);
void miera_nezam_form(Stat* stat);
void disp_no_uchadzacov_form(Stat * stat);
void min_max_avg_ekonom_aktiv_ob_form(Stat*);
void max_pokles_miery_zen_muz_form(Stat*);
void best_pomer_disp_and_abs_form(Stat*);
void get_sum_stat_za_kraj_form(Stat*);
void miera_nezam_kraj_form(Stat*);
void zorad_disp_pocet_uchadz_form(Stat*);
void get_max_min_avg_ekonom_aktiv_ob_kraj_form(Stat*);
void vypis_max_pokles_miery_evid_nezam_kraj_form(Stat*);
void vypis_best_pomer_disp_abs_kraj(Stat*);
void get_roky(int& rok_od, int& rok_do);
void get_rok(int& rok);


int main() {
	initHeapMonitor();
	//using namespace structures;
	using namespace std;
	
	//locale::global(std::locale(""));
	/*std::locale cz("Czech");
	std::locale::global(cz);
	std::cout << "������������";*/

	SetConsoleCP(1250);
	SetConsoleOutputCP(1250);

	ifstream inFileZam("Data/Uch�dza�i o zamestnanie.csv");
	ifstream inFileMiera("Data/Miera evidovanej nezamestnanosti1.csv");
	//ifstream inFileMiera("Data/Miera evidovanej nezamestnanosti.csv");
	Stat* s = new Stat();	

	s->loadNeazmest(inFileZam);
	inFileZam.close();

	s->loadMiera(inFileMiera);
	inFileMiera.close();
	
	
	bool koniec = false;
	int vyber;	
	while (!koniec)
	{
		menu();
		while (!(cin >> vyber)) {
			cout << "Bad value!" << endl;
			cin.clear();
			cin.ignore(10000, '\n');
		}
		cin.get();

		switch (vyber)
		{
		case 1: hladaj_okres_no_nezam_form(s); break;

		case 2: miera_nezam_form(s); break;
		case 3: disp_no_uchadzacov_form(s); break;

		case 4: min_max_avg_ekonom_aktiv_ob_form(s); break;
		case 5: max_pokles_miery_zen_muz_form(s); break;
		case 6: best_pomer_disp_and_abs_form(s); break;

		case 7: get_sum_stat_za_kraj_form(s); break;

		case 8: miera_nezam_kraj_form(s); break;
		case 9: zorad_disp_pocet_uchadz_form(s); break;

		case 10: get_max_min_avg_ekonom_aktiv_ob_kraj_form(s); break;
		case 11: vypis_max_pokles_miery_evid_nezam_kraj_form(s); break;
		case 12: vypis_best_pomer_disp_abs_kraj(s); break;
		default: koniec = true;
			break;
		}

	}
	delete s;
	return 0;
}

void menu() {
	using namespace std;
	char *format = "\t%-60s\n";
	cout << endl;
	printf("\t\t%35s\n\n", "-----=====menu======-----");
	printf(format, "0\tkoniec");
	printf(format, "1\tI.Vypis statistik nezamestnanosti v okrese\n");
	printf(format, "2\tII.a)Vypis okresov podla miery evid. nezam. v %");
	printf(format, "3\tII.b)Vypis okresov podla disp. poctu uchadzac. o zamestnanie\n");
	printf(format, "4\tIII.a)Vypis okresov s najvac. a najman. avg poctom ekonom.aktiv. ob.");
	printf(format, "5\tIII.b)Vypis najvacsi pokles miery evid. nezam. muzi zeny");
	printf(format, "6\tIII.c)Vypis najlepsieho pomeru medzi disp.poct.uchadz. o zam. a nezam.absol.\n");
	printf(format, "7\tIV.Vypis sumarnych statistik kraju podla jeho nazvu\n");
	printf(format, "8\tV.a)Vypis podla miery evidovane nezamestnanosti v kraji");
	printf(format, "9\tV.b)Vypis podla disp. poctu uchadzacov o zamestnanie v kraji\n");
	printf(format, "10\tVI.a)Vypis kraju s najvacsim a najmensim poctom ekonom.aktiv.obiv.");
	printf(format, "11\tVI.b)Vypis kraju/ov s najvacsim poklesom miery evid.nezam. muzi zeny");
	printf(format, "12\tVI.c)Vypis kraju s najlepsim pomerom disp.poctu nezam. a absolventov");	
}

/**
Vyhlada okres a ak existuje vypise pocty nezamestnanych v 4 kategoriach podla zadaneho intervalu rokov
*/
void hladaj_okres_no_nezam_form(Stat* stat) {
	string okres = "";
	
	int rok_od = 0;
	int rok_do = 0;

	cout << "Zadajte nazov okresu >>  ";
	getline(cin, okres);

	get_roky(rok_od, rok_do);

	stat->vypis_pocty_nezam(okres, rok_od, rok_do);
}

void miera_nezam_form(Stat* stat)
{
	int rok = 0;

	get_rok(rok);

	cout << "Chcete subor zoradit vzostupne(y/n)?";	
	if (can_continue()) {
		cout << endl;
		stat->vypis_miera_nezam_vzost(rok);			
	}
	else
	{
		cout << endl;
		stat->vypis_miera_nezam_zost(rok);
	}
}

void disp_no_uchadzacov_form(Stat * stat)
{
	int rok = 0;
	get_rok(rok);

	cout << "Chcete subor zoradit vzostupne(y/n)?";
	if (can_continue()) {
		cout << endl;
		stat->vypis_no_disp_uchadz_vzost(rok);			
	}
	else
	{
		cout << endl;
		stat->vypis_no_disp_uchadz_zost(rok);
	}
}

void min_max_avg_ekonom_aktiv_ob_form(Stat* s)
{	
	int rok_od = 0;
	int rok_do = 0;
	
	get_roky(rok_od, rok_do);
	
	s->get_min_max_avg_ekonom_aktiv_ob(rok_od, rok_do);

}

void max_pokles_miery_zen_muz_form(Stat* s) 
{

	int rok_od = 0;
	int rok_do = 0;

	get_roky(rok_od, rok_do);
	if (rok_od != rok_do)
	{
		s->get_max_pokles_miery_zeny(rok_od, rok_do);
		s->get_max_pokles_miery_muzi(rok_od, rok_do);
	}
	else 
	{
		cout << endl << "Nie je mozne porovnat." << endl;
	}
}

void best_pomer_disp_and_abs_form(Stat* s) 
{
	int rok_od = 0;
	int rok_do = 0;

	get_roky(rok_od, rok_do);
	s->get_best_pomer_disp_and_abs(rok_od, rok_do);
}

void get_sum_stat_za_kraj_form(Stat* s)
{
	string kraj = "";
	int rok_od = 0;
	int rok_do = 0;

	cout << "Zadajte nazov kraju >>  ";
	getline(cin, kraj);

	get_roky(rok_od, rok_do);

	s->get_sum_stat_za_kraj(kraj, rok_od, rok_do);
}

void miera_nezam_kraj_form(Stat* s)
{
	int rok = 0;
	get_rok(rok);
	
	cout << "Chcete subor zoradit vzostupne(y/n)?";
	if (can_continue()) {
		cout << endl;
		s->vypis_miera_nezam_kraje_vzost(rok);
	}
	else
	{
		cout << endl;
		s->vypis_miera_nezam_kraje_zost(rok);
	}
}

void zorad_disp_pocet_uchadz_form(Stat* s) {
	int rok = 0;
	get_rok(rok);

	cout << "Chcete subor zoradit vzostupne(y/n)?";
	if (can_continue()) {
		cout << endl;
		s->vypis_no_disp_zam_kraje_vzost(rok);
	}
	else
	{
		cout << endl;
		s->vypis_no_disp_zam_kraje_zost(rok);
	}
}

void get_rok(int& rok) 
{
	cout << "Zadajte rok v intervale <2001;2016> >> ";
	while (!(cin >> rok)) {
		cout << "Bad value!" << endl;
		cin.clear();
		cin.ignore(10000, '\n');
	}
	if (rok < 2001)
	{
		rok = 2001;
	}
	else if (rok > 2016)
	{
		rok = 2016;
	}
}

void get_max_min_avg_ekonom_aktiv_ob_kraj_form(Stat* s)
{
	int rok_od = 0;
	int rok_do = 0;

	get_roky(rok_od, rok_do);
	
	s->get_max_min_avg_ekonom_aktiv_ob_kraj(rok_od, rok_do);
}

void vypis_max_pokles_miery_evid_nezam_kraj_form(Stat* s)
{
	int rok_od = 0;
	int rok_do = 0;

	get_roky(rok_od, rok_do);
	if (rok_od != rok_do)
	{
		s->get_max_pokles_zeny_kraj(rok_od, rok_do);
		s->get_max_pokles_muzi_kraj(rok_od, rok_do);
	}
	else
	{
		cout << endl << "Nie je mozne porovnat." << endl;
	}
}

void vypis_best_pomer_disp_abs_kraj(Stat* s) {
	int rok_od = 0;
	int rok_do = 0;

	get_roky(rok_od, rok_do);

	s->get_best_pomer_disp_abs_kraj(rok_od, rok_do);
}

void get_roky(int& rok_od, int& rok_do) 
{
	string interval = "";
	cout << "Zadajte roky - zaciatok_intervalu (medzera) koniec_inrevalu >> ";
	getline(cin, interval, ' ');
	rok_od = stoi(interval);
	getline(cin, interval);
	rok_do = stoi(interval);

	if (rok_do < rok_od)
	{
		int pom = rok_od;
		rok_od = rok_do;
		rok_do = pom;
	}

	if (rok_do > 2016 || rok_do < 2001)
	{
		rok_do = 2016;
	}
	if (rok_od < 2001 || rok_od > 2016)
	{
		rok_od = 2001;
	}
}

bool can_continue()
{
	char ch;

	while (!(cin >> ch)) {
		cout << "Zla hodnota!" << endl;
		cin.clear();
		cin.ignore(10000, '\n');
	}

	switch (ch)
	{
	case 'n':
	case 'N':
	case '0':
		return false;
	default:
	case 'y':
	case 'Y':
	case 'A':
	case 'a':
		return true;
	}
}