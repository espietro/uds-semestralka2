#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "structures/heap_monitor.h"
#include "structures\table\sorted_sequence_table.h"
#include "structures\list\array_list.h"
#include "Okres.h"
#include "Kraj.h"

class Stat
{
public:
	Stat();
	~Stat();
	bool loadNeazmest(ifstream&);	
	bool loadMiera(ifstream&);
	const static int NO_OKRES = 79;
	void add_to_kraj(Okres*, int);

	//1
	void vypis_okresy();	//vypisanie vsetkych okresov zoradene podla nazvu
	void vypis_pocty_nezam(string okres_nazov, int rok_od = 2001, int rok_do = 2016);
	//2a,b
	void usporiadaj_miera_nezam(int rok, UnsortedSequenceTable<float, Okres*>& pom_struct); 
	void usporiadaj_no_disp_uchadz(int rok, UnsortedSequenceTable<int, Okres*>& pom_struct);
	
	void vypis_miera_nezam_zost(int rok);
	void vypis_miera_nezam_vzost(int rok);
	void vypis_no_disp_uchadz_zost(int rok);
	void vypis_no_disp_uchadz_vzost(int rok);
	//3
	void get_min_max_avg_ekonom_aktiv_ob(int rok_od, int rok_do);

	void get_max_pokles_miery_muzi(int rok_od, int rok_do);
	void get_max_pokles_miery_zeny(int rok_od, int rok_do);
	
	void get_best_pomer_disp_and_abs(int rok_od, int rok_do);
	//4
	void get_sum_stat_za_kraj(string, int, int);
	//5a
	void zorad_miera_nezam_kraje(int rok, UnsortedSequenceTable<float, Kraj*>*);
	void vypis_miera_nezam_kraje_vzost(int rok);
	void vypis_miera_nezam_kraje_zost(int rok);
	//5b
	void zorad_no_disp_zam_kraje(int rok, UnsortedSequenceTable<int, Kraj*>*);
	void vypis_no_disp_zam_kraje_vzost(int rok);
	void vypis_no_disp_zam_kraje_zost(int rok);
	//6a
	void get_max_min_avg_ekonom_aktiv_ob_kraj(int rok_od, int rok_do);
	//6b
	void get_max_pokles_muzi_kraj(int, int);
	void get_max_pokles_zeny_kraj(int, int);
	//6c
	void get_best_pomer_disp_abs_kraj(int, int);

	enum Kraje	//cisla znamenaju pocet okresov v danom kraji
	{
		Bratislavsky = 8,
		Trnavsky = 15,//7,
		Trenciansky = 24,//9,
		Nitriansky = 31,//7,
		Zilinsky = 42,//11,
		Banskobystricky = 55,//13,
		Presovsky = 68,//13,
		Kosicky = 79//11		
	};
private:	
	SortedSequenceTable<string, Kraj*> kraje_;	//zoznam krajov	
	SortedSequenceTable<string, Okres*> okresy_;	//sekvencna tabulka smernikov na TableItem<nazovOkresu,Okres*>	
};

